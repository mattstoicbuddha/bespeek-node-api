const journal = {
	"create": "create:journals:{user_id}",
	"read": "read:journals:{user_id}",
	"edit": "edit:journals:{user_id}",
	"delete": "delete:journals:{user_id}"
};

const media = {
	"create": "create:media:{user_id}",
	"read": "read:media:{user_id}",
	"edit": "edit:media:{user_id}",
	"delete": "delete:media:{user_id}"
};

const settings = {
	"read": "read:settings:{user_id}",
	"edit": "edit:settings:{user_id}"
};

const subscription = {
	"read": "read:subscription:{user_id}",
	"edit": "edit:subscription:{user_id}",
	"delete": "delete:subscription:{user_id}"
};

const connections = {
	"create": "create:connections:{user_id}",
	"read": "read:connections:{user_id}",
	"edit": "edit:connections:{user_id}",
	"delete": "delete:connections:{user_id}"
};

const scopes = {
	journal,
	media,
	settings,
	subscription,
	connections
}

// Set up scopes for the API token
const getScopes = (user_id, local_scopes) => {
	if (!user_id || !Array.isArray(local_scopes)) return false;
	const scopesArray = [];
	local_scopes.forEach((scope) => {
		if (scopes[scope]) {
			const mainScope = scopes[scope];
			if (typeof mainScope === "string") {
				scopesArray.push(mainScope.replace("{user_id}", user_id));
			} else {
				for (const subScope of Object.keys(mainScope)) {
					scopesArray.push(mainScope[subScope].replace("{user_id}", user_id));
				}
			}
		}
	});
	return scopesArray;
};

module.exports = {
	getScopes
};