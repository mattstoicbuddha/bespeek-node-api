const apiUrl = "https://api.maildump.app",
	  fetch = require("node-fetch"),
	  config = require(process.cwd() + "/config") ;

let tokenHolder = "";

const getAuthToken = async () => {
	if (tokenHolder.length) return tokenHolder;
	const getToken = new Promise((resolve, reject) => {
		fetch(`${apiUrl}/authenticate`, {
	        method: 'post',
	        headers: {
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({api_key: config.maildump.api_key})
	      })
	      .then(res => res.json())
	      .then(json => {
	      	if (json.response && json.response.token) {
	      		tokenHolder = json.response.token;
	      		return waitFor(500).then(token => resolve(json.response.token))	      			      		
	      	}
	      	return reject("No token present.");
	      })
	      .catch(error => reject(error));
	});
	return await getToken.catch(e => {throw e});
}

const createInbox = async () => {
	const token = await getAuthToken();
	const create = new Promise((resolve, reject) => {
		fetch(`${apiUrl}/inbox/create`, {
	        method: 'post',
	        headers: {
	        	"Authorization": "Bearer " + token
	        }
	      })
	      .then(res => res.json())
	      .then(json => {
	      	return resolve(json.response);
	      })
	      .catch(error => reject(error));
	});
	return await create.catch(e => {throw e});
}

const waitFor = ms => {
	return new Promise(r => {
		setTimeout(() => {
			r(true);
		}, ms)
	});
}

const waitForLatest = async (inboxId) => {
	const token = await getAuthToken();
	console.time('wait_for_maildump_email_' + inboxId);
	let inboxObj = null;
	const url = `${apiUrl}/inbox/${inboxId}/latest`;
	while(!inboxObj) {
		const wait = new Promise((resolve, reject) => {
			fetch(url, {
		        method: 'get',
		        headers: {
		        	"Authorization": "Bearer " + token
		        }
		      })
		      .then(res => res.json())
		      .then(json => {
		      	return resolve(json.response);
		      })
		      .catch(error => reject(error));
		});
		const waitCheck = await wait.catch(e => {throw e});
		if (!waitCheck.email || (!waitCheck.email.text && !waitCheck.email.html)) {
			await waitFor(500);
			continue;
		}
		inboxObj = waitCheck.email;
	}
	console.timeEnd('wait_for_maildump_email_' + inboxId);
	return inboxObj;
}

const deleteLatest = async (inboxId) => {
	const token = await getAuthToken();
	const url = `${apiUrl}/inbox/${inboxId}/latest`;
	return await new Promise((resolve, reject) => {
		fetch(url, {
	        method: 'delete',
	        headers: {
	        	"Authorization": "Bearer " + token
	        }
	      })
	      .then(res => res.json())
	      .then(json => {
	      	return resolve(json.response);
	      })
	      .catch(error => reject(error));
	});
}

module.exports = {
	createInbox,
	waitForLatest,
	deleteLatest
}