const rootDir = process.cwd(),
		config = require('../config'),
		fs = require('fs'),
		sgMail = require('@sendgrid/mail');
		sgMail.setApiKey(config.mail_settings.sendgrid_api_key);

class MailHelper {
	constructor() {
		const account = config.mail_settings;
		this.mailOptions = {};
		this.template = null;
		this.templateKeys = {};
	}

	setOptions(opts) {
		this.mailOptions = {
			from: opts.from || config.mail_settings.from,
			to: opts.to,
			subject: opts.subject,
			html: opts.html || null
		}
		if (opts.template) {
			if (opts.template === "registration_welcome") {
				this.template = rootDir + '/templates/mail/registration_welcome.html'
			} else if (opts.template === "password_reset") {
				this.template = rootDir + '/templates/mail/password_reset.html'
			} else if (opts.template === "sponsor_registration_welcome") {
				this.template = rootDir + '/templates/mail/sponsor_registration_welcome.html'
			} else if (opts.template === "remote_signup") {
				this.template = rootDir + '/templates/mail/remote_signup.html'
			}
			if (opts.templateKeys) {
				this.templateKeys = opts.templateKeys;
			}
		}
	}

	async send() {
		const opts = Object.assign({}, this.mailOptions);
		console.log(!opts.from,  !opts.to,  !opts.subject,  (!opts.html && !this.template))
		if (!opts.from || !opts.to || !opts.subject || (!opts.html && !this.template) ) {
			throw "You are missing at least one necessary field.";
		}
		if (!opts.html && this.template) {
			try {
				let templateHTML = fs.readFileSync(this.template,'utf8');
				if (this.templateKeys) {
					Object.keys(this.templateKeys).forEach(k => {
						const replacement = new RegExp(k,"g");
						templateHTML = templateHTML.replace(replacement, this.templateKeys[k]);
					});
				}
				opts.html = templateHTML;
			} catch(e) {
				console.log({e});
			}
		}
		// send mail with defined transport object
		let info = await sgMail.send(opts).catch(e => e);
		console.log({info})
		const response = info.shift();
		if (response.statusCode < 400) return true;
		throw "Mail was not sent.";
	}
}

module.exports = MailHelper;