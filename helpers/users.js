const rootDir = process.cwd(),
			models = require(rootDir + "/models"),
			Users = models.mariadb.users,
			Questions = models.mariadb.questions,
			Answers = models.mariadb.answers,
			Redis = models.redis;
// generic error function
const catchErr = e => {throw e};

/**
 * Publish question category info related to a user to a db cache
 * @since 1.0.0
 *
 * @param {integer} userId - The user id we need to publish info for
 * @returns {object}
 */
const publishUserInfo = async (userId) => {
	const usersModel = new Users;
	const questionsModel = new Questions;
	const answersModel = new Answers;
	const redisModel = new Redis;

	if (!userId) throw "No user given";
	// TODO: set up a config for all redis keys somehow
	const redisKey = "publishUserInfo:user_id:" + userId;
	const answered = await answersModel.getAnsweredQuestionsByUserId(userId).catch(catchErr);
	const qids = answered.map(answer => answer.question_id);
	const questions = await questionsModel.getQuestionsByIds(qids).catch(catchErr);
	const unlockedCats = [];
	for (const question of questions) {
		const getAnswer = answered.find(ans => {
			return ans.question_id === question.id;
		});
		if (!getAnswer) continue;
		if (question.possible_answers && question.type == "multiple_choice") {
			const answer = question.possible_answers.find(poss => {
				return poss.id === getAnswer.possible_answer_id;
			});
			if (!answer) continue;
			if (unlockedCats.indexOf(answer.cat_unlock) < 0) {
				unlockedCats.push(answer.cat_unlock);
			}
		}
		if (question.type == "boolean") {
			let catUnlock = 0;
			if (getAnswer.possible_answer_id === 1) {
				catUnlock = question.true_cat_unlock;
			} else if (getAnswer.possible_answer_id === 0) {
				catUnlock = question.false_cat_unlock;
			} else {
				continue;
			}
			if (catUnlock && unlockedCats.indexOf(catUnlock) < 0) {
				unlockedCats.push(catUnlock);
			}
		}
	}
	const userInfo = {
		unlocked_cats: unlockedCats
	}
	await redisModel.save(redisKey, userInfo, 3600 * 3).catch(catchErr);
	return "User info published";
}

/**
 * Publish question category info related to a user to a db cache
 * @since 1.0.0
 *
 * @param {integer} userId - The user id we need published info for
 * @returns {object}
 */
const getPublishedUserInfo = async (userId) => {
	const redisModel = new Redis;
	if (!userId) throw "No user given";
	const redisKey = "publishUserInfo:user_id:" + userId;
	const cache = await redisModel.get(redisKey).catch(e => console.log(e));
	// if (cache) return cache;
	await publishUserInfo(userId).catch(catchErr);
	return await redisModel.get(redisKey).catch(e => console.log(e));
}

module.exports = {
	getPublishedUserInfo,
	publishUserInfo
}