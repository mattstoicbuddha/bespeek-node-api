const rootDir = process.cwd(),
			config = require(rootDir + "/config"),
			fs = require("fs"),
    		aws = require('aws-sdk'),
			s3 = new aws.S3({
			    // Your SECRET ACCESS KEY from AWS should go here,
			    // Never share it!
			    // Setup Env Variable, e.g: process.env.SECRET_ACCESS_KEY
			    secretAccessKey: config.aws.secret_access_key,
			    // Not working key, Your ACCESS KEY ID from AWS should go here,
			    // Never share it!
			    // Setup Env Variable, e.g: process.env.ACCESS_KEY_ID
			    accessKeyId: config.aws.access_key_id,
			    region: config.aws.region // region of your bucket
			}),
			uuid = require('uuid/v1');

/**
 * Format the field names for error returns from our JOI validation
 * @since 1.0.0
 *
 * @param {string} field - The error field we are formatting.
 * @returns {string}
 */
const formatFieldNames = (field) => {
	// Break out the field name we need by splitting it from the nearby quotes
	const fieldSplit = field.split("\"");
	console.log({field, fieldSplit})
	const fieldName = fieldSplit[1];
	fieldSplit[1] = fieldName.replace(/(^|_)./g, s => " " + s.slice(-1).toUpperCase());
	// If we have a space at the beginning of the word, which we probably will,
	// we need to remove it
	if (fieldSplit[1].charAt(0) === " ") {
		// This will only be replaced once since we aren't passing any regex
		fieldSplit[1] = fieldSplit[1].replace(" ", "");
	}
	return fieldSplit.join("\"");
}

/**
 * Validate submitted fields against our JOI validation functionality
 * @since 1.0.0
 *
 * @param {object} validateObj - The JOI validation object.
 * @param {array} fields - The fields we need to validate.
 * @returns {null || object}
 */
const checkValidation = (validateObj, fields) => {
	const isValid = validateObj.validate(fields);
	if (isValid.error) {
		// Format our error and error object for return
		return {
			error: true,
			error_object: createError(400, isValid.error)
		}
	}
	return {
		error: false
	};
};

/**
 * Create our error object and allow us to manipulate said object
 * @since 1.0.0
 *
 * @param {integer} code - The error code we need to send as a response.
 * @param {string} msg - The error message.
 * @returns {object}
 */
const createError = (code, msg) => {
	let message = msg;
	// Check if our message is actually a Javascript Error object, in
	// order to ensure that we know where to get the error message from
	// if `msg` isn't a string
	if (msg instanceof Error) {
		message = msg.message;
	}
	return {
		isAPIError: true,
		code,
		msg: message
	}
}

/**
 * Format our error object to avoid unnecessary repetition and enforce normalized returns
 * @since 1.0.0
 *
 * @param {object} errorObj - The error message.
 * @returns {object}
 */
const formatErrors = (errObj) => {
	return {
		success: false,
		error: errObj.msg || "An unknown error occurred."
	}
}

/**
 * Determine if an object is one of our errors
 * @since 1.0.0
 *
 * @param {object} obj - The object to check
 * @returns {boolean}
 */
const isError = (obj) => {
	return !obj || obj.isAPIError && typeof obj.isAPIError === "boolean";
}

/**
 * Format our success object to avoid unnecessary repetition and enforce normalized returns
 * @since 1.0.0
 *
 * @param {mixed} response - The response to the call; this changes depending on the route and what is needed.
 * @returns {object}
 */
const formatSuccess = (response) => {
	return {
		success: true,
		response: response || ""
	}
}

/**
 * Polyfill for PHP empty, which makes a lot of things easier/quicker to check
 * @since 1.0.0
 *
 * @param {mixed} mixed_var - The variable we are checking for emptiness
 * @returns {boolean}
 */
const isEmpty = (mixed_var) => {
 var key;
 if (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || mixed_var === undefined ) {
  return true;
 }
 if (typeof mixed_var == 'object') {
  for (key in mixed_var) {
   return false;
  }
  return true;
 }
 return false;
}

/**
 * Pluck/format our user info for a journal
 * @since 1.0.0
 *
 * @param {object} container - The container for the journal
 * @param {object} row - The variable we are checking for emptiness
 * @returns {null}
 */
const getUserInfo = (container, row) => {
	for(const key in row ) {
		// If we have the userinfo_ prefix, we know it is
		// user data that we can assign appropriately
		if (key.indexOf("userinfo_") > -1) {
			// Remove this key from the containered row since it will
			// be inside of its own object
			delete container[key];
			// Create the user_info object in the containered row if we don't
			// already have it
			if (!container.owner_info) {
				container.owner_info = {};
			}
			// Remove the prefix from the key
			const newKey = key.split("userinfo_")[1];
			// Set the data up appropriately
			container.owner_info[newKey] = row[key];
		}
	}
}

/**
 * Format our user metadata and user info
 * @since 1.0.0
 *
 * @param {array} rows - The rows of data from the database we need to manipulate
 * @returns {array}
 */
const formatUserList = (rows) => {
	// Set up our container array
	const container = [];
	// Is this an object? If so, we need to do some slightly different
	// things before processing
	const isObject = !Array.isArray(rows) && rows === Object(rows);
	// If it is indeed an object, we only had the one result and need
	// to put it in an array to process. We will take it out later though
	const rowsToProcess = isObject ? [rows] : rows;
	rowsToProcess.forEach((row) => {
		if (!container[row.id]) {
			// If we don't have an index for this row ID yet,
			// then we haven't created the object and need to
			// do so
			container[row.id] = Object.assign({}, row);
			// Remove the current metadata, since we will be
			// storing it in an array in the object itself
			delete container[row.id].meta_key;
			delete container[row.id].meta_value;
			delete container[row.id].meta_user_id;
			// See if we have metadata, and set an empty array if not, for
			// the sake of data normalization
			if (!row.meta_key && !container[row.id].meta_data) {
				container[row.id].meta_data = [];
				return;
			}
			// Set up our metadata array
			container[row.id].meta_data = [
				{
					meta_key: row.meta_key,
					meta_value: row.meta_value
				}
			];
		} else {
			// See if we have metadata, and set an empty array if not, for
			// the sake of data normalization
			if (!row.meta_key && !container[row.id].meta_data) {
				container[row.id].meta_data =[];
				return;
			}
			// Create our array if we don't have one already
			container[row.id].meta_data = container[row.id].meta_data || [];
			// Add to the metadata
			container[row.id].meta_data.push(
				{
					meta_key: row.meta_key,
					meta_value: row.meta_value
				}
			);
		}
	});
	// The .filter here ensures that our indexes aren't screwy;
	// since the first will always be `1`, the first value in our
	// array will always be null (for index 0) unless we do this
	const processedContainer = container.filter(function(){return true;});
	// If the original `rows` value was an object, we need to return the one value, instead of
	// an array; if we send back an array, we defeat the purpose of grabbing the first value in the first place.
	return isObject ? processedContainer[0] : processedContainer;
}

/**
 * Make sure that our journal data we've received looks approximately correct
 * @since 1.0.0
 *
 * @param {object} data - The posted data for our journal
 * @returns {object}
 */
const checkJournalData = (data) => {
	const result = {
		valid: true
	}
	if (isEmpty(data.title)) {
		result.valid = false;
		result.message = "No title received.";
		return result;
	}
	if (isEmpty(data.content)) {
		result.valid = false;
		result.message = "No content received.";
		return result;
	}
	if (isEmpty(data.type) && isEmpty(data.journal_id)) {
		result.valid = false;
		result.message = "No type received.";
		return result;
	}
	return result;
}

/**
 * Make sure that our question data we've received looks approximately correct
 * @since 1.0.0
 *
 * @param {object} data - The posted data for our question
 * @returns {object}
 */
const checkQuestionData = (data) => {
	const result = {
		valid: true
	}
	if (isEmpty(data.text)) {
		result.valid = false;
		result.message = "No question text received.";
		return result;
	}
	if (isEmpty(data.id) && isEmpty(data.type)) {
		result.valid = false;
		result.message = "No question type received.";
		return result;
	}
	if (isEmpty(data.category_id)) {
		result.valid = false;
		result.message = "No question category received.";
		return result;
	}
	if (data.type === "boolean") {
		if (isEmpty(data.true_cat_id) && isEmpty(data.false_cat_id)) {
			result.valid = false;
			result.message = "No category unlock for answers received.";
			return result;
		}
	} else if (data.type === "multiple_choice") {
		if (isEmpty(data.possible_answers)) {
			result.valid = false;
			result.message = "No possible answers received.";
			return result;
		}
		const possibleAnswersCheck = data.possible_answers.filter(answer => {
			return !(
						isEmpty(answer.answer_text) ||
						(isEmpty(answer.cat_unlock) && answer.cat_unlock !== 0) ||
						isEmpty(answer.answer_weight) ||
						isNaN(answer.cat_unlock) ||
						isNaN(answer.answer_weight)
					)
		});
		if (possibleAnswersCheck.length < data.possible_answers.length) {
			result.valid = false;
			result.message = "One or more possible answers received were invalid.";
			return result;
		}
	} else if (isEmpty(data.id) && data.type !== "text") {
		result.valid = false;
		result.message = "Invalid type '" + data.type + "' received.";
		return result;
	}
	if (!isEmpty(data.id) && isNaN(data.id)) {
		result.valid = false;
		result.message = "Invalid id '" + data.id + "' received.";
		return result;
	}
	return result;
}

/**
 * Format a message object for normalizing message returns
 * @since 1.0.0
 *
 * @param {string} message - The message to send back
 * @returns {array}
 */
const message = (msg) => {
	return {"message": msg}
}

/**
 * Ensure proper formatting when trying to parse meta arrays.
 * @since 1.0.0
 *
 * @param {array} meta - The meta array to check
 * @returns {array}
 */
const checkMetaArray = (meta) => {
	// Set up our check variable we can use shortly to
	// verify success
	let check = true;
	// No need to manipulate the check variable here,
	// as that would just be unnecessary
	if (!Array.isArray(meta)) return false;
	// Iterate through our array and verify we are looking
	// at a proper key->value structure; with this check, it
	// could technically contain various other values, but we
	// are ignoring them anyway
	meta.forEach((obj) => {
		if (isEmpty(obj.meta_key)) check = false;
	});
	return check;
}

/**
 * Format the question list data to send back the necessary data per question
 * @since 1.0.0
 *
 * @param {array} meta - The meta array to check
 * @returns {array}
 */
const formatQuestionListRows = (rows) => {
	const rowsObj = rows.map(row => Object.assign({}, row));
	const formattedRows = [];
	rowsObj.map(row => {
		const exists = formattedRows.findIndex(elem => {
		  return parseInt(row.id) === parseInt(elem.id);
		});
		// Make sure we are only messing with objects we have to
		if (exists > -1 && row.type !== "multiple_choice") return;
		// Create a base object to ensure we have the same
		// formatting in objects except where it actually
		// needs to differ
		const baseObj = {
			id: row.id,
			text: row.text,
			type: row.type,
			category_id: row.category_id
		}
		// Start manipulating objects by type
		if (row.type === "multiple_choice") {
			let ind = exists;
			if (exists < 0) {
				ind = formattedRows.length;
				formattedRows[ind] = Object.assign({}, baseObj);
			}
			if (!formattedRows[ind].possible_answers) {
				formattedRows[ind].possible_answers = [];
			}
			formattedRows[ind].possible_answers.push({
				cat_unlock: row.pam_cat_unlock,
				answer_text: row.pam_answer_text,
				answer_weight: row.pam_answer_weight
			});
			// Set up our answers to return by weight so we don't need
			// to do any work on the front end if we don't want to
			formattedRows[ind].possible_answers.sort((a, b) => {
				return a.answer_weight - b.answer_weight
			});
		} else if (row.type === "boolean") {
			const boolObj = Object.assign(baseObj, {
				true_cat_unlock: row.pab_true_cat_unlock,
				false_cat_unlock: row.pab_false_cat_unlock
			});
			formattedRows.push(boolObj);
		} else if (row.type === "text") {
			formattedRows.push(Object.assign({}, baseObj));
		}
	});
	return formattedRows;
}

/**
 * Generate a unique password reset code
 * @since 1.0.0
 *
 * @returns {array}
 */
const generatePasswordReset = () => {
	const token = require('crypto').randomBytes(32).toString('hex');
	return token;
};

/**
 * Parses the validation errors into human-readable formats
 * @since 1.0.0
 *
 * @param {object} errorObj - The error object
 * @returns {object}
 */
const makeJoiErrorReadable = (errorObj) => {

	const errs = 'The following errors have occurred: ' + errorObj.map((err) => {
		if (err.code === "any.allowOnly") {
			// Check to see if we have an error involving matching fields
			const validValues = err.context.valids;
			const valids = validValues.map((val, i) => {
				if (val.toString().indexOf("ref:") > -1) return {val: val.key, i};
			});
			// If we have matching ref values, we need to figure out what they are
			// to display the error message, and stuff them into the appropriate
			// bits of the validValues array
			if (valids.length) {
				valids.forEach(valid => {
					if (!valid) return;
					validValues[valid.i] = "The value must match the value for the following field: " + valid.val;
				});
				return `\`${err.value}\` is an invalid value for the parameter \`${err.local.key}\`. ${
					validValues.map(valid => "`" + valid + "`").join(", ")
				}`;
			}
			// If we don't have a matching field error, then we can assume we have actual valid values to send back
			return `\`${err.value}\` is an invalid value for the parameter \`${err.local.key}\`. Valid values are: ${
				validValues.map(valid => "`" + valid + "`").join(", ")
			}`;
		}
		if (err.code === "any.required") {
			return `\`${err.local.key}\` is a required parameter.`;
		}
		if (err.code === "any.empty") {
			return `\`${err.local.key}\` requires a value.`;
		}
		if (err.code === "string.email") {
			return `\`${err.value}\` is an invalid value for \`${err.local.key}\`. Please check the email address you've submitted.`;
		}
		if (err.code === "string.min") {
			return `\`${err.local.key}\` must be at least ${err.local.limit} characters. Length: ${err.value.length}`;
		}
		if (err.code === "string.max") {
			return `\`${err.local.key}\` cannot be longer than ${err.local.limit} characters. Length: ${err.value.length}`;
		}
		if (err.code === "number.base") {
			if (isNaN(err.value)) {
				return `\`${err.local.key}\` must be a number; \`${err.value}\` is an invalid value`;
			} else {
				return `\`${err.value}\` is an invalid value for the parameter \`${err.local.key}\`.`;
			}
		}
		if (err.code === "string.base64") {
			return `\`${err.local.key}\` must be a valid base64 image; \`${err.value}\` is an invalid value`;
		}
		console.log({JOIERR: err})
		return err;
	}).join(' and ');
	return new Error(errs);
}

/**
 * Parses the validation errors into human-readable formats
 * @since 1.0.0
 *
 * @param {object} errorObj - The error object
 * @returns {object}
 */
const uploadFile = (fileData, bucket, dir) => {
	return new Promise((resolve, reject) => {
		if (!fileData || !bucket) return reject("Missing necessary parameters for file upload.");

		const 	type = fileData.type,
				ext = "." + fileData.name.split(".").pop(),
				folder = dir ? dir + "/" : "",
				name = uuid() + ext;
		let data = null;
		if (fileData.path) {
			data = fs.readFileSync(fileData.path);
		} else if (fileData.stream) {
			data = fileData.stream;
		}

		const params = {
			Bucket: bucket || config.aws.assets_bucket, // pass your bucket name
			Key: folder + name, // file will be saved as testBucket/contacts.csv
			Body: data,
			ContentType: type,
			ACL: "public-read"
		};
		s3.upload(params, function(s3Err, data) {
		 if (s3Err) return reject(s3Err);
		 return resolve({
		 	location: data.Location,
		 	key: data.key,
		 	type: fileData.type,
		 	name,
		 	folder
		 })
		});
	});
}

const uploadBase64Image = async (imgStr, bucket, dir) => {
	const img = imgStr.split(';base64,').pop();
	const stream = Buffer.from(img, 'base64');
	const contentType = imgStr.split(';base64,').shift().replace("data:", "");
	// Base64 image types by character
	const types = {
		'/': 'jpg',
		'i': 'png',
		'R': 'gif',
		'U': 'webp'
	}
	const ext = types[img.charAt(0)];
	const fileName = uuid() + '.' + ext;
	const fileData = {
		type: contentType,
		name: fileName,
		stream
	}
	return await uploadFile(fileData, bucket, dir);
}

/**
 * Get a random string of a specified length
 * @since 1.0.0
 *
 * @param {integer} length - The length of the string
 * @returns {string}
 */
const getRandomString = length => {
	let result = '';
	const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

module.exports = {
	formatErrors,
	createError,
	isError,
	isEmpty,
	checkValidation,
	formatSuccess,
	formatUserList,
	checkJournalData,
	message,
	checkMetaArray,
	checkQuestionData,
	formatQuestionListRows,
	generatePasswordReset,
	makeJoiErrorReadable,
	uploadFile,
	getRandomString,
	uploadBase64Image
}