// This file exists so we don't have to call the full knex connection
// package for every single model, as that is repetitious and unnecessary.
const config = require('../config'),
    knex = require('knex')({
        client: 'mysql',
        connection: {
            host: config.mariadb.host,
            user: config.mariadb.user,
            password: config.mariadb.password,
            database: 'bespeek'
        },
        pool: {
            min: 0,
            max: 10
        }
    });
module.exports = knex;