const Joi = require('@hapi/joi').extend(require('joi-phone-number')),
        isBase64 = require('is-base64'),
		h = require(process.cwd() + "/helpers");

const zipCode = (value, helpers) => {
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value) ? value : helpers.error('any.invalid');
}

const logoImage = (value, helpers) => {
    const imgValid = isBase64(value, {mimeRequired: true})
    return imgValid ? value : helpers.error('string.dataUri');
}

const hexColor = (value, helpers) => {
    const isHex = /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(value);
    console.log({isHex})
    if (!isHex) return helpers.error('any.invalid');
    return value;
}

// Validate our registration form
const registration = Joi.object().keys({
    email: Joi.string().email({ minDomainSegments: 2 }).required().error(h.makeJoiErrorReadable),
    password: Joi.string().min(6).required().error(h.makeJoiErrorReadable),
    contact_name: Joi.string().min(3).max(50).required().error(h.makeJoiErrorReadable),
    contact_number: Joi.string().phoneNumber().required().error(h.makeJoiErrorReadable),
    company_name: Joi.string().min(3).max(50).required().error(h.makeJoiErrorReadable),
    address: Joi.string().min(3).max(50).required().error(h.makeJoiErrorReadable),
    city: Joi.string().min(3).max(30).required().error(h.makeJoiErrorReadable),
    zip_code: Joi.string().custom(zipCode, 'Zip Code Validator').required().error(h.makeJoiErrorReadable),
    logo: Joi.string().custom(logoImage, 'Logo Image Base64 Validator').error(h.makeJoiErrorReadable),
    isSponsor: Joi.boolean().valid(true).error(h.makeJoiErrorReadable),
});

// Validate our updated data
const update = Joi.object().keys({
    password: Joi.string().min(6).error(h.makeJoiErrorReadable),
    contact_name: Joi.string().min(3).max(50).error(h.makeJoiErrorReadable),
    contact_number: Joi.string().phoneNumber().error(h.makeJoiErrorReadable),
    company_name: Joi.string().min(3).max(50).error(h.makeJoiErrorReadable),
    address: Joi.string().min(3).max(50).error(h.makeJoiErrorReadable),
    city: Joi.string().min(3).max(30).error(h.makeJoiErrorReadable),
    zip_code: Joi.string().custom(zipCode, 'Zip Code Validator').error(h.makeJoiErrorReadable),
    logo: Joi.string().custom(logoImage, 'Logo Image Base64 Validator').error(h.makeJoiErrorReadable)
});

// Validate profile updates
const embedCodeSettings = Joi.object().keys({
    backgroundColor: Joi.string().custom(hexColor,'Hex Color Validator').error(h.makeJoiErrorReadable),
    quadrantClass: Joi.string().valid('top-left', 'top-right', 'bottom-left', 'bottom-right').error(h.makeJoiErrorReadable)
});

module.exports = {
	registration,
    update,
    embedCodeSettings
}