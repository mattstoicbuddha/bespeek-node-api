const Joi = require('@hapi/joi'),
        isBase64 = require('is-base64'),
		h = require(process.cwd() + "/helpers");

const zipCode = (value, helpers) => {
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value) ? value : helpers.error('any.invalid');
}

const profileImage = (value, helpers) => {
    const imgValid = isBase64(value, {mimeRequired: true})
    return imgValid ? value : helpers.error('string.dataUri');
}

// Validate our registration form
const registration = Joi.object().keys({
    email: Joi.string().email({ minDomainSegments: 2 }).required().error(h.makeJoiErrorReadable),
    password: Joi.string().min(6).required().error(h.makeJoiErrorReadable),
    first_name: Joi.string().min(3).max(30).error(h.makeJoiErrorReadable),
    last_name: Joi.string().min(3).max(30).error(h.makeJoiErrorReadable),
    zip_code: Joi.string().custom(zipCode, 'Zip Code Validator').error(h.makeJoiErrorReadable),
    sponsor_id: Joi.number().min(1).error(h.makeJoiErrorReadable)
});

// Validate profile updates
const profileUpdate = Joi.object().keys({
    id: Joi.number().min(1).error(h.makeJoiErrorReadable),
    first_name: Joi.string().min(3).max(30).error(h.makeJoiErrorReadable),
    last_name: Joi.string().min(3).max(30).error(h.makeJoiErrorReadable),
    zip_code: Joi.string().custom(zipCode, 'Zip Code Validator').error(h.makeJoiErrorReadable),
    profile_image: Joi.string().custom(profileImage, 'Profile Image Base64 Validator').error(h.makeJoiErrorReadable),
});

// Validate our remote reg
const remoteRegistration = Joi.object().keys({
    email: Joi.string().email({ minDomainSegments: 2 }).required().error(h.makeJoiErrorReadable),
    sponsor_id: Joi.number().min(1).required().error(h.makeJoiErrorReadable),
    remote: Joi.boolean().required().allow(true).error(h.makeJoiErrorReadable),
});


// Validate our password reset values
const passwordReset = Joi.object().keys({
    reset_code: Joi.string().min(3).required().error(h.makeJoiErrorReadable),
    password_first: Joi.string().min(6).max(50).required().error(h.makeJoiErrorReadable),
    password_second: Joi.string().min(6).max(50).required().valid(Joi.ref('password_first')).error(h.makeJoiErrorReadable)
});

// Validate our email value
const email = Joi.object().keys({
    email: Joi.string().email({ minDomainSegments: 2 }).required().error(h.makeJoiErrorReadable)
});

// Validate our connection ids array
const connectionIds = Joi.object().keys({
    connection_ids: Joi.array().items(
        Joi.number().min(1).required().error(h.makeJoiErrorReadable)
    ).required().error(h.makeJoiErrorReadable)
});

module.exports = {
	registration,
    passwordReset,
    remoteRegistration,
    email,
    connectionIds,
    profileUpdate
}