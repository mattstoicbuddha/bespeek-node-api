const Joi = require('@hapi/joi'),
			h = require(process.cwd() + "/helpers");

// Validate our question categories data
const categories = Joi.object().keys({
    category_name: Joi.string().min(2).max(25).required(),
    category_description: Joi.string().min(2).max(50),
    id: Joi.number()
});

module.exports = {
	categories
}