const Joi = require('@hapi/joi'),
		h = require(process.cwd() + "/helpers");

// Validate our analytics object
const save = Joi.object().keys({
    event: Joi.string().required().error(h.makeJoiErrorReadable),
    timestamp: Joi.number().min(Date.now() - 60000).required().error(h.makeJoiErrorReadable),
    url: Joi.string().error(h.makeJoiErrorReadable),
    ip_address: Joi.string().error(h.makeJoiErrorReadable),
    category: Joi.string().required().error(h.makeJoiErrorReadable),
    browser: Joi.string().error(h.makeJoiErrorReadable),
    session: Joi.string().min(36).max(46).required().error(h.makeJoiErrorReadable),
});

module.exports = {
	save
}