const rootDir = process.cwd(),
		model = require(rootDir + "/models"),
		Questions = model.mariadb.questions,
		h = require(rootDir + "/helpers");


/**
 * List current questions
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const listQuestions = (req, callback) => {
	const {user, params} = req;
	// Get a list of currently available questions
	const questionsModel = new Questions;
	questionsModel
	.listQuestions(params)
	.then(rows => {
		return callback(null, h.formatQuestionListRows(rows));
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Create a new question
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const createQuestion = (req, callback) => {
	const {user, params} = req;
	if (params.id) {
		return callback(h.createError(400, "An id was passed to the creation route, indicating this is not a new question. Please use the update route instead."));
	}
	const check = h.checkQuestionData(params);
	if (!check.valid) {
	  return callback(h.createError(400, check.message));
	}
	const questionsModel = new Questions;
	questionsModel
	.saveQuestion(params)
	.then(obj => {
		return callback(null, {id: obj.question_id});
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Update a question
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const updateQuestion = (req, callback) => {
	const {user, params} = req;
	if (!params.id) {
		return callback(h.createError(400, "No id given to update a question. Please pass an id or use the appropriate route to create a new question."));
	}
	if (params.type) {
		return callback(h.createError(400, "Question types cannot be updated after creation."));
	}
	const check = h.checkQuestionData(params);
	if (!check.valid) {
	  return callback(h.createError(400, check.message));
	}
	const questionsModel = new Questions;
	questionsModel
	.saveQuestion(params)
	.then(obj => {
		return callback(null, {result: obj});
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Delete a question
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const deleteQuestion = (req, callback) => {
	const {user, params} = req;
	if (!params.id) {
		return callback(h.createError(400, "No id given to update a question. Please pass an id."));
	}
	const questionsModel = new Questions;
	questionsModel
	.deleteQuestion(params.id)
	.then(obj => {
		return callback(null, {rows_updated: obj});
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}


module.exports = {
	listQuestions,
	updateQuestion,
	createQuestion,
	deleteQuestion
}