const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(rootDir + "/models"),
		Questions = model.mariadb.questions,
		QuestionsCategories = model.mariadb.questions_categories
		h = require(rootDir + "/helpers"),
		valid = require(rootDir + "/validation/questions_categories");


/**
 * List current categories
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const listCategories = (req, callback) => {
	const {user, params} = req;
	const questionsCategoriesModel = new QuestionsCategories;
	questionsCategoriesModel
	.getAllCategories()
	.then(rows => {
		const rowClones = rows.map(row => {
			// Clone our object so we can actually manipulate it
			const clone = Object.assign({}, row);
			const aws = config.aws;
			// If we have a category icon, stub in the appropriate URL so the front-end doesn't have to do
			// anything to get the full image url
			if (clone.category_icon && clone.category_icon.length) {
				clone.category_icon = aws.assets_cdn + "/" + aws.category_icons_dir + "/" + clone.category_icon;
			}
			return clone;
		})
		return callback(null, rowClones);
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Create a new category
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const createCategory = (req, callback) => {
	const {user, params} = req;
	if (params.id) {
		return callback(h.createError(400, "An id was given; this route is to create categories. Please remove the id to create a new category."));
	}
	const check = h.checkValidation(valid.categories, params);
	if (check.error) {
	  return callback(h.createError(400, check.error_object.msg));
	}
	const questionsCategoriesModel = new QuestionsCategories;
	// Set up a promise to allow us to upload a file without needing to repeat
	// business logic if we *don't* have a file
	const uploadCheck = new Promise((resolve, reject) => {
		// If we don't have a file, resolve now and save our category info
		if (!req.files) return resolve(null);
		// If we do have a file, upload before resolution
		const img = Object.keys(req.files)[0];
		if (img !== "icon") return reject({code: 400, err: img + " is an invalid parameter for file upload. Valid parameter: `icon`"});
		const upload = h.uploadFile(req.files[img], config.aws.assets_bucket, config.aws.category_icons_dir)
		upload
			.then(fileInfo => {
				return resolve(fileInfo);
			})
			.catch(err => {
				return reject({code: 500, err});
			})
	});
	uploadCheck
		.then(fileInfo => {
			if (fileInfo) {
				params.category_icon = fileInfo.name;
			}
			questionsCategoriesModel
			.saveCategory(params)
				.then(obj => {
					return callback(null, {id: obj[0]});
				}).catch(err => {
					return callback(h.createError(500, err));
				});
		})
		.catch(err => {
			return callback(h.createError(err.code, err.err));
		});
}

/**
 * Update a category
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const updateCategory = (req, callback) => {
	const {user, params} = req;
	if (!params.id) {
		return callback(h.createError(400, "No id given to update a category. Please pass an id or use the appropriate route to create a new category."));
	}
	const check = h.checkValidation(valid.categories, params);
	if (check.error) {
	  return callback(h.createError(400, check.error_object.msg));
	}
	const questionsCategoriesModel = new QuestionsCategories;
	questionsCategoriesModel
	.saveCategory(params)
	.then(obj => {
		return callback(null, {rows_updated: obj});
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Delete a category
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const deleteCategory = (req, callback) => {
	const {user, params} = req;
	if (!params.id) {
		return callback(h.createError(400, "No id given to update a category. Please pass an id."));
	}
	const questionsCategoriesModel = new QuestionsCategories;
	questionsCategoriesModel
	.deleteCategory(params.id)
	.then(obj => {
		return callback(null, {rows_updated: obj});
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}


module.exports = {
	listCategories,
	updateCategory,
	createCategory,
	deleteCategory
}