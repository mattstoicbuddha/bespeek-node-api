const rootDir = process.cwd(),
			model = require(rootDir + "/models"),
			Users = model.mariadb.users,
			UsersMeta = model.mariadb.users_meta,
			Answers = model.mariadb.answers,
			Connections = model.mariadb.connections,
			bcrypt = require('bcrypt'),
			h = require(rootDir + "/helpers"),
			saltRounds = 10,
			valid = require(rootDir + "/validation/users");

/**
 * Get a list of current users
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const listUsers = (req, callback) => {
	const {user} = req;
	const limit = req.params.limit || 25;
	const skip = req.params.skip || 0;
	// Find the user's connections,if they have any
	const usersModel = new Users;
	usersModel
		.getUserList(limit, skip)
		.then(usersFromDb => {
			// So we can massage the objects
			const users = usersFromDb.map(u => {
				const user = Object.assign({}, u);
				user.type = "member";
				if (user.sponsor_id) {
					user.type = "partner";
				}
				delete user.sponsor_id;
				delete user.sponsor_company;
				delete user.user_sponsor;
				return user;
			});
			const userIds = users.map(user => user.id);
			// Now we need to get user meta
			const metadata = new UsersMeta;
			metadata
				.getMetaByUserIds(userIds)
				.then(meta => {
					users.map(user => {
						if (typeof user.meta_data === "undefined") user.meta_data = [];
						meta.map(m => {
							if (user.id === m.user_id) {
								delete m.user_id;
								user.meta_data.push(Object.assign({}, m));
							}
						});
					});
					// We need to get how many questions each user answered, so let's do that too
					const answersModel = new Answers;
					answersModel
						.getAnswerCountByUserIds(userIds)
						.then(answers => {
							users.map(user => {
								if (typeof user.answer_count === "undefined") user.answer_count = 0;
								answers.map(answer => {
									if (user.id === answer.user_id) user.answer_count++;
								});
							});
							return callback(null, {users});
						})
						.catch(err => {
							console.log(err);
							return callback(h.createError(500, err), null);
						});
				})
				.catch(err => {
					console.log(err);
					return callback(h.createError(500, err), null);
				})
		}).catch( err => {
			console.log(err);
			return callback(h.createError(500, err), null);
		});
}

/**
 * Deactivate a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const deactivateUser = async (req, callback) => {
	const {user_id} = req.params;
	if (!user_id) return callback(h.createError(400, "No user id submitted."));
	const usersModel = new Users;
	const worked = await usersModel.deactivateUserById(user_id).catch(err => {
		console.log({err})
	});
	if (!worked) return callback(h.createError(500, "Unable to deactivate user."));
	return callback(null, "Success!");
}

/**
 * Re-activate a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const reactivateUser = async (req, callback) => {
	const {user_id} = req.params;
	if (!user_id) return callback(h.createError(400, "No user id submitted."));
	const usersModel = new Users;
	const worked = await usersModel.reactivateUserById(user_id).catch(err => {
		console.log({err})
	});
	if (!worked) return callback(h.createError(500, "Unable to reactivate user."));
	return callback(null, "Success!");
}


module.exports = {
	listUsers,
	deactivateUser,
	reactivateUser
}