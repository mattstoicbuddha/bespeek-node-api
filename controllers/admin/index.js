const questions = require('./questions'),
			questions_categories = require('./questions_categories'),
			users = require('./users');

module.exports = {
	questions,
	questions_categories,
	users
}