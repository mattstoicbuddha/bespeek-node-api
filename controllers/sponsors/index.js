const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(process.cwd() + "/models"),
		Redis = model.redis,
		Sponsors = model.mariadb.sponsors,
		SponsorVerifications = model.mariadb.sponsor_verifications,
		h = require(process.cwd() + "/helpers"),
		MailHelper = require(process.cwd() + "/helpers/mail"),
		Users = require(rootDir + "/controllers/users"),
		valid = require(process.cwd() + "/validation/sponsors");

const sendSignupEmail = async id => {
	const sponsorVerificationModel = new SponsorVerifications;
	const verification = await sponsorVerificationModel.getVerification(id).catch(err => { throw new Error(err) });
	const mail = new MailHelper;
	mail.setOptions({
		to: verification.email,
		subject: "Welcome to Bespeek!",
		template: "sponsor_registration_welcome",
		templateKeys: {
			"BESPEEK_SPONSOR_REGISTRATION_LINK": config.api_url + "/partners/confirm-registration?code=" + verification.registration_code
		}
	});
	return await mail.send().then(res => res).catch(err => { throw new Error(err) });
}

/**
 * Get a sponsor's profile
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getProfile = async (req, callback) => {
	const {params} = req;
	const sponsorsModel = new Sponsors;
	const profile = await sponsorsModel.findSponsorById(params.sponsor_id)
		.then(s => s) // Hack to ensure we hit the sponsor MariaDB middleware
		.catch( err => {
			console.log("error", err);
			return false;
		});
	if (!profile) {
		// The query was technically fine, but we don't have a sponsor
		// for that ID; therefore, it is a 404
		return callback(h.createError(404, "No sponsor found with that ID."), null);
	}
	const clone = Object.assign({}, profile);
	const sponsees = await sponsorsModel.getSponsees(clone.id).catch( err => {
		console.log("error", err);
		return false;
	});
	const sponseesArray = sponsees.map(sponsee => {
		return Users.getProfileById(sponsee.id);
	});
	const sponseeData = await Promise.all(sponseesArray).catch( err => {
		console.log("error", err);
		return false;
	});
	if (sponseeData) clone.sponsees = sponseeData;
	return callback(null, clone);
}

const getSponsorProfile = async (req, callback) => {
	const {params, user} = req;
	const sponsorsModel = new Sponsors;
	const profile = await sponsorsModel.findSponsorByUserId(user.id)
		.then(s => s) // Hack to ensure we hit the sponsor MariaDB middleware
		.catch( err => {
			console.log("error", err);
			return false;
		});
	if (!profile) {
		// The query was technically fine, but we don't have a sponsor
		// for that ID; therefore, it is a 404
		return callback(h.createError(404, "No sponsor data found."), null);
	}
	const clone = Object.assign({}, profile);
	const sponsees = await sponsorsModel.getSponsees(clone.id).catch( err => {
		console.log("error", err);
		return false;
	});
	const sponseesArray = sponsees.map(sponsee => {
		return Users.getProfileById(sponsee.id);
	});
	const sponseeData = await Promise.all(sponseesArray).catch( err => {
		console.log("error", err);
		return false;
	});
	if (sponseeData) clone.sponsees = sponseeData;
	return callback(null, clone);
}


/**
 * Create the back-end management task to verify new Sponsor
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const createVerification = (req, callback) => {
	const {company_name, address, city, state, zip_code, email} = req.params;
	if (!company_name || !address || !city || !state || !zip_code || !email) return callback(h.createError(400, "Missing one or more parameters for verification"), null);
	const sponsorVerificationModel = new SponsorVerifications;
	sponsorVerificationModel
		.startVerification({company_name, address, city, state, zip_code, email})
			.then(verificationTask => {
				if (verificationTask) {
					return callback(null, "Verification task created");
				} else {
					return callback(h.createError(500, "Verification not created for unknown reasons."), null);
				}
			}).catch( err => {
				return callback(h.createError(500, err), null);
			});
}

/**
 * Create the back-end management task to verify new Sponsor
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getVerificationByCode = (req, callback) => {
	const {registration_code} = req.params;
	if (!registration_code) return callback(h.createError(400, "Missing one or more parameters for verification"), null);
	const sponsorVerificationModel = new SponsorVerifications;
	sponsorVerificationModel
		.getVerificationByCode(registration_code)
			.then(verificationTask => {
				if (verificationTask) {
					return callback(null, verificationTask);
				} else {
					return callback(h.createError(500, "Verification unknown."), null);
				}
			}).catch( err => {
				return callback(h.createError(500, err), null);
			});
}

/**
 * Finish the back-end management task to verify new Sponsor
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const finishVerification = (req, callback) => {
	const {id} = req.params;
	if (!id) return callback(h.createError(400, "Missing one or more parameters to finish verification"), null);
	const sponsorVerificationModel = new SponsorVerifications;
	sponsorVerificationModel
		.finishVerification(id, h.getRandomString(10))
			.then(async verificationTask => {
				if (verificationTask) {
					const sent = await sendSignupEmail(id);
					console.log({sent})
					return callback(null, "Verified");
				} else {
					return callback(h.createError(500, "Verification not updated for unknown reasons."), null);
				}
			}).catch( err => {
				return callback(h.createError(500, err), null);
			});
}

/**
 * Register a sponsor
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
 const register = async (req, callback) => {
	const invalid = h.checkValidation(valid.registration, req.params);
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	const {email, password, contact_name, contact_number, company_name, address, city, zip_code} = req.params;
	const userObj = {
		email: email,
		password,
		first_name: contact_name || "",
		last_name: "",
		isSponsor: true
	}
	if (contact_name && contact_name.indexOf(" ") > -1) {
		const contact = contact_name.split(" ");
		userObj.first_name = contact.shift();
		userObj.last_name = contact.join(" ");
	}
	const user = await Users.createUser(userObj, true).catch(err => err);
	if (h.isError(user)) {
		return callback(user);
	}
	console.log({user})
	const params = {
		user_id: user.id,
		company_name,
		contact_name,
		contact_number,
		address,
		city,
		zip_code
	}
	if (params.profile_image) {
		const pi = params.profile_image;
		const fileInfo = await h.uploadBase64Image(pi, config.aws.assets_bucket, config.aws.sponsor_logos_dir).catch(reject);
		const fileName = fileInfo.name;
		params.logo = fileName;
	}
	const sponsorsModel = new Sponsors;
	sponsorsModel
		.createSponsor(params)
		.then(async sponsor => {
			return callback(null, "Account created!");
		})
		.catch( err => {
			return callback(h.createError(500, err), null);
		});
 }

/**
 * Update a sponsor's profile
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
 const update = async (req, callback) => {
	const invalid = h.checkValidation(valid.update, req.params);
	if (invalid && invalid.error) {
		console.log({invalid: invalid.error_object})
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	const {params, user} = req;
	const sponsorUpdate = {
		user_id: user.id
	}
	const userUpdate = {}
	const paramKeys = Object.keys(params);
	paramKeys.forEach(p => {
		// Set up our sponsor update
		if (['company_name', 'contact_name', 'contact_number', 'address', 'city', 'zip_code'].indexOf(p) > -1) sponsorUpdate[p] = params[p];
		// Set up our user update
		if (['contact_name', 'zip_code', 'password', 'first_name', 'last_name'].indexOf(p) > -1) {
			// We need to make sure that we don't override a legit first_name update with a contact name
			if (p === 'contact_name' && paramKeys.indexOf('first_name') < 0) {
				if (params[p].indexOf(' ') > -1) {
					const name = params[p].split(" ");
					userUpdate.first_name = name.shift();
					userUpdate.last_name = name.join(" ");
				}
			} else {
				userUpdate[p] = params[p];
			}
		}
	});
	if (Object.keys(userUpdate).length) {
		const updateUser = await Users.updateUser(user.id, userUpdate, req.files).catch(err => h.createError(500, err));
		if (!updateUser) console.log("Couldn't update user for some reason...");
	}	
	if (params.logo) {
		const fileInfo = await h.uploadBase64Image(params.logo, config.aws.assets_bucket, config.aws.sponsor_logos_dir).catch(err => h.createError(500, err));
		if (fileInfo && fileInfo.name) {
			sponsorUpdate.logo = fileInfo.name;
		}
	}
	const sponsorsModel = new Sponsors;
	sponsorsModel
		.updateSponsor(sponsorUpdate)
		.then(async sponsor => {
			return callback(null, "Account updated!");
		})
		.catch( err => {
			return callback(h.createError(500, err), null);
		});
}

/**
 * Get a sponsor's embed code settings
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getEmbedCodeSettings = async (req, callback) => {
	const {params, user} = req;
	console.log({params, user})
	const sponsorUpdate = {
		user_id: user.id
	}
	const sponsorsModel = new Sponsors;
	const profile = await sponsorsModel.findSponsorByUserId(user.id)
		.then(s => s) // Hack to ensure we hit the sponsor MariaDB middleware
		.catch( err => {
			console.log("error", err);
			return false;
		});
	if (!profile) {
		// The query was technically fine, but we don't have a sponsor
		// for that ID; therefore, it is a 404
		return callback(h.createError(404, "No sponsor found."), null);
	}
	const returnObj = {
		id: profile.id,
		backgroundColor: profile.embed_code_background_color,
		quadrantClass: profile.embed_code_quadrant_class
	}
	return callback(null, returnObj);
}

/**
 * Get a sponsor's embed code settings
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const updateEmbedCodeSettings = async (req, callback) => {
	const invalid = h.checkValidation(valid.embedCodeSettings, req.params);
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	const {params, user} = req;
	
	const sponsorUpdate = {
		user_id: user.id
	}
	const sponsorsModel = new Sponsors;
	const profile = await sponsorsModel.findSponsorByUserId(user.id)
		.then(s => s) // Hack to ensure we hit the sponsor MariaDB middleware
		.catch( err => {
			console.log("error", err);
			return false;
		});
	if (!profile) {
		// The query was technically fine, but we don't have a sponsor
		// for that ID; therefore, it is a 404
		return callback(h.createError(404, "No sponsor found."), null);
	}
	const redisModel = new Redis;
	const cacheKeys = ["bespeek_sponsor_embed_code_style_id:" + profile.id, "bespeek_sponsor_embed_code_script_id:" + profile.id];
	redisModel.delMany(cacheKeys).then(deleted => console.log({deleted})).catch(error => console.error(error));
	if (params.backgroundColor) {
		sponsorUpdate.embed_code_background_color = params.backgroundColor;
	}
	if (params.quadrantClass) {
		sponsorUpdate.embed_code_quadrant_class = params.quadrantClass;
	}
	const returnObj = {
		id: profile.id,
		backgroundColor: sponsorUpdate.embed_code_background_color || profile.embed_code_background,
		quadrantClass: sponsorUpdate.embed_code_quadrant_class || profile.embed_code_quadrant_class
	}
	sponsorsModel
		.updateSponsor(sponsorUpdate)
		.then(async sponsor => {
			return callback(null, returnObj);
		})
		.catch( err => {
			return callback(h.createError(500, err), null);
		});
}


module.exports = {
	register,
	getProfile,
	getSponsorProfile,
	finishVerification,
	createVerification,
	getVerificationByCode,
	update,
	getEmbedCodeSettings,
	updateEmbedCodeSettings
}