class BespeekEmbedAnalytics {
	
	constructor() {
		this.start = this._startAnalytics.bind(this);
		this.emit = this._emitEvent.bind(this);
	}

	_startSession() {
		const cls = this;
		return new Promise((resolve, reject) => {
			let code = 200;
			fetch("https://api.bespeek.com/analytics/start", {
				method: 'get',
				headers: {
				  'Content-Type': 'application/json'
				}
			})
			.then(res => { code = res.status; return res; })
			.then(res => res.json())
			.then(json => {
				if (!json.success) {
					return reject(json);
				}
				return resolve(json.response.session);
			})
			.catch(err => {
				return reject(err);
			});
		});
	}

	async _startAnalytics() {
		// No need to set up another session if we have one
		if (this.session) return;
		const session = await this._startSession().catch(err => {return new Error(err)});
		if (session instanceof Error) {
			// Couldn't start the session for some reason...
			// what do we do here?
			return;
		}
		this.session = session;
	}

	_sendEvent(event, category) {
		if (!this.session) return;
		let code = 200;
		const analyticsObj = {
			category,
			event,
			session: this.session,
			url: window.location.href,
			browser: window.navigator.userAgent
		}
		fetch("https://api.bespeek.com/analytics/save", {
			method: 'post',
			headers: {
			  'Content-Type': 'application/json'
			},
			body: JSON.stringify(analyticsObj)
		})
		.then(res => { code = res.status; return res; })
		.then(res => res.json())
		.then(json => {
			if (!json.success) {
				console.log({json});
				return;
			}
			console.log({response: json.response});
		})
		.catch(err => {
			console.error(err);
		});
	}

	_emitEvent(event) {
		const category = "embed_code_modal";
		this._sendEvent(event, category);
	}
}