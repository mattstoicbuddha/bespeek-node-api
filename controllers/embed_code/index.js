const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(process.cwd() + "/models"),
		Redis = model.redis,
		redisConn = new Redis,
		SponsorsModel = model.mariadb.sponsors,
		cleanCSS = require("clean-css");
		sass = require("node-sass");
		UglifyJS = require("uglify-es"),
		fs = require("fs");

/**
 * Get a the embed code to display on a sponsor's page
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 */
const getCode = async (req, callback) => {
	const {sponsorId, nocache} = req.params;
	const {host} = req.headers;
	const apiURL = (host.indexOf('localhost:') < 0 ? "https://" : "http://") + host;
	if (!sponsorId) return callback("No sponsor id present.");
	const cacheKey = "bespeek_sponsor_embed_code_script_id:" + sponsorId;
	console.log({nocache})
	if (nocache && nocache === 'true') {
		console.log("Skipping caching...");
	} else {
		const cached = await redisConn.get(cacheKey).catch(err => console.log({err}));
		if (cached) return callback(null, cached.replace(/API_URL_GOES_HERE/g, apiURL));	
	}
	// No cache, gotta pull fresh
	const Sponsors = new SponsorsModel;
	const sponsor = await Sponsors.findSponsorById(sponsorId).catch(callback);
	if (!sponsor) return callback("Invalid sponsor id.");
	const feAnalytics = fs.readFileSync(rootDir + '/controllers/embed_code/front-end-analytics.js', 'utf-8');
	let feScript = feAnalytics + fs.readFileSync(rootDir + '/controllers/embed_code/front-end-script.js', 'utf-8');
	// replace necessary sponsor ids
	feScript = feScript.replace(/SPONSOR_ID_GOES_HERE/g, sponsorId);
	// replace necessary logos
	feScript = feScript.replace(/SPONSOR_LOGO_GOES_HERE/g, sponsor.logo);
	// replace necessary company names
	feScript = feScript.replace(/SPONSOR_NAME_GOES_HERE/g, sponsor.company_name);
	// replace necessary company names
	let quadrantClass = 'bottom-left';
	if (sponsor.embed_code_quadrant_class) quadrantClass = sponsor.embed_code_quadrant_class;
	feScript = feScript.replace(/SPONSOR_QUADRANT_CLASS_GOES_HERE/g, quadrantClass);
	// set cache bits
	let cache = "";
	if (nocache && nocache === 'true') cache = "?nocache=true"
	feScript = feScript.replace(/NOCACHE/g, cache);
	// minify on the fly bc why not?
	const minified = UglifyJS.minify(feScript);
	// Cache for 12 hours
	await redisConn.save(cacheKey, minified.code, 3600 * 12);
	return callback(null, minified.code.replace(/API_URL_GOES_HERE/g, apiURL));
}

/**
 * Get a the stylesheet to display the embed on a sponsor's page
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 */
const getStyle = async (req, callback) => {
	const host = req.headers.host;
	const {sponsorId, nocache} = req.params;
	if (!sponsorId) return callback("No sponsor id present.");
	const cacheKey = "bespeek_sponsor_embed_code_style_id:" + sponsorId;
	if (nocache && nocache === 'true') {
		console.log("Skipping caching...");
	} else {
		const cached = await redisConn.get(cacheKey).catch(err => console.log({err}));
		if (cached) return callback(null, cached);	
	}
	// No cache, gotta pull fresh
	const Sponsors = new SponsorsModel;
	const sponsor = await Sponsors.findSponsorById(sponsorId).catch(callback);
	if (!sponsor) return callback("Invalid sponsor id.");
	// Compile our CSS on the fly
	const sassStyles = sass.renderSync({
	  file: rootDir + '/controllers/embed_code/front-end-style.scss'
	});
	// Get our CSS string
	let feStyle = sassStyles.css.toString();
	// dump the sourcemap
	feStyle = feStyle.split("/*# sourceMappingURL")[0];
	// Get our background color in
	let bgColor = "#eb6864";
	if (sponsor.embed_code_background_color) bgColor = sponsor.embed_code_background_color;
	feStyle = feStyle.replace(/SPONSOR_BACKGROUND_COLOR_GOES_HERE/g, bgColor);
	// Finally, minify the css on the fly bc why not
	const minified = new cleanCSS().minify(feStyle);
	// Cache for 12 hours
	await redisConn.save(cacheKey, minified.styles, 3600 * 12);
	return callback(null, minified.styles);
}

const getPage = async (req, callback) => {
	const html = fs.readFileSync(process.cwd() + '/templates/tests/embed_code_test.html', 'utf-8');
	const pageHtml = html.replace(/PARTNER_ID_GOES_HERE/i, req.params.sponsorId);
	return callback(null, pageHtml); 
}

module.exports = {
	getCode,
	getStyle,
	getPage
}