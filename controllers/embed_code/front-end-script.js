const bespeekReady = fn => {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

class bespeekModal {
	constructor() {
		this.questionElements = [];
		this.answerObjects = [];
		this.searchTimer = null;
		this.getSongSearchResults = this.displaySongSearchResults.bind(this);
		this.Analytics = new BespeekEmbedAnalytics;
		this.sendAnalytics = this.Analytics.emit;
	}

	validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	setStyleLoaded() {
		this.styleLoaded = true;
	}

	waitForStyle() {
		const cls = this;
		return new Promise(r => {
			const wfs = setInterval(() => {
				if (this.styleLoaded) {
					clearInterval(wfs);
					return r(true);
				}
			}, 100);
		})
	}

	toggleModal() {
		const style = window.getComputedStyle(this.mainModal);
		if (style.display === "none") {
			// Modal is hidden, opening...
			this.mainModal.classList.add('show');
			this.openButton.classList.add('hidden');
			this.modalOverlay.classList.add('show');
		} else {
			this.mainModal.classList.remove('show');
			this.openButton.classList.remove('hidden');
			this.modalOverlay.classList.remove('show');
		}
	}

	displaySongSearchResults(results, input) {
		const minTop = input.offsetHeight + input.offsetTop;
		const currentSearch = document.querySelector('.bespeek-song-results-holder');
		if (currentSearch) currentSearch.remove();
		const searchResults = document.createElement('div');
		searchResults.classList.add('bespeek-song-results-holder');
		searchResults.style.top = minTop + 'px';
		const resultContainer = document.createElement('ul');
		resultContainer.classList.add('bespeek-song-results-list');
		searchResults.appendChild(resultContainer);
		results.forEach(result => {
			// we only want songs in this search
			if (result.kind !== "song") return;
			const row = document.createElement('li');
			row.classList.add('bespeek-song-result');
			// Artwork
			const art = document.createElement('div');
			art.classList.add('art');
			const artWork = document.createElement('img');
			artWork.src = result.artworkUrl100;
			art.appendChild(artWork);
			row.appendChild(art);
			// Title
			const title = document.createElement('div');
			title.classList.add('title');
			title.innerHTML = result.trackName;
			row.appendChild(title);
			// Artist
			const artist = document.createElement('div');
			artist.classList.add('artist');
			artist.innerHTML = result.artistName;
			row.appendChild(artist);
			row.addEventListener('click', () => {
				input.value = result.trackName + ' by ' + result.artistName;
				searchResults.remove();
			});
			row.addEventListener('mouseenter', () => {
				row.classList.add('bespeek-song-hover');
			});
			row.addEventListener('mouseleave', () => {
				row.classList.remove('bespeek-song-hover');
			});
			resultContainer.appendChild(row);
		});
		input.parentElement.appendChild(searchResults);
	}

	createQuestion(i, q) {
		const question = document.createElement('div');
		question.classList.add('bespeek-modal-question', 'question-' + i);
		const questionText = document.createElement('p');
		// We don't want to show the numbered index for the
		// email address
		const ind = i > -1 ? (i+1) + ". " : "";
		questionText.innerHTML = ind + q.text;
		question.appendChild(questionText);
		question.dataset.id = q.id;
		question.dataset.question = JSON.stringify(q);
		question.dataset.type = q.type;
		if (i === 0) {
			question.classList.add('visible');
		}
		if (q.type === "text" || q.type === "song") {
			const answerContainer = document.createElement('div');
			answerContainer.classList.add('bespeek-answer-text-container');
			const answerHolder = document.createElement('input');
			answerHolder.classList.add('bespeek-modal-answer-text');
			answerContainer.appendChild(answerHolder);
			question.appendChild(answerContainer);
			// Create our submit button
			const submitAnswerButton = document.createElement('button');
			submitAnswerButton.innerHTML = "Submit";
			submitAnswerButton.classList.add('bespeek-submit', 'bespeek-submit-text');
			submitAnswerButton.addEventListener('click', this.saveAnswer.bind(this, submitAnswerButton));
			answerContainer.appendChild(submitAnswerButton);
			const saveAnswer = this.saveAnswer.bind(this, submitAnswerButton);
			if (q.type === "song") {
				const classObj = this;
				// add an itunes search bc why not		
				answerHolder.addEventListener('keypress', function (e) {
					if (e.keyCode === 13) return;
					if (classObj.searchTimer) clearTimeout(classObj.searchTimer);
					let code = 200;
					classObj.searchTimer = setTimeout(() => {
						answerHolder.disabled = true;
						answerHolder.classList.add('searching');
						const url = "API_URL_GOES_HERE/proxy/songs?term=" + answerHolder.value.replace(/ /g, '+');
						fetch(url, {
						    method: 'get',
						    headers: {
						      'Content-Type': 'application/json'
						    }
						  })
						  .then(res => { code = res.status; return res; })
						  .then(res => res.json())
						  .then(json => {
							answerHolder.classList.remove('searching');
						  	answerHolder.disabled = false;
						    if (!json.success) {
						      console.log("boo", {json})
						    } else {
						      classObj.getSongSearchResults(json.response, answerHolder);
						    }
						    return json;
						  })
						  .catch(error => {
							answerHolder.classList.remove('searching');
						  	answerHolder.disabled = false;
							console.log({error})
						  });
					}, 500);
				}, false);
			} else {

				answerHolder.addEventListener('keypress', function (e) {
					// If they press enter, submit
				    if (e.keyCode === 13) {
				        saveAnswer();
				    }
				}, false);
			}
		} else if (q.type === "multiple_choice") {
			const paName = "bspk_" + Math.floor(Math.random() * Date.now());
			const answerHolder = document.createElement('div');
			answerHolder.classList.add('bespeek-modal-possible-answers');
			question.appendChild(answerHolder);
			q.possible_answers.forEach(pa => {
				const paId = "bspk_id_" + Math.floor(Math.random() * Date.now());
				const possibleContainer = document.createElement('div');
				possibleContainer.classList.add('bespeek-possible-answers-container');
				const possibleText = document.createElement('label');
				possibleText.innerHTML = pa.text;
				possibleText.htmlFor = paId;
				const possibleRadio = document.createElement('input');
				possibleRadio.type = 'radio';
				possibleRadio.value = pa.id;
				possibleRadio.name = paName;
				possibleRadio.id = paId;
				possibleContainer.appendChild(possibleRadio);
				if (q.useIcons) {
					possibleRadio.style.height = 0;
					const possibleIcon = document.createElement('div');
					possibleIcon.classList.add('bspk-mc-icon', 'answer-' + pa.id);
					possibleIcon.dataset.answerId = pa.id;
					possibleIcon.addEventListener('click', (e) => {
						possibleRadio.click();
						document.querySelectorAll('.bspk-mc-icon').forEach(icon => {
							icon.classList.remove('selected');
						});
						possibleIcon.classList.add('selected');
					})
					possibleContainer.appendChild(possibleIcon);
				}
				possibleContainer.appendChild(possibleText);
				answerHolder.appendChild(possibleContainer);
			});
			question.appendChild(answerHolder);
			// Create our submit button
			const submitAnswerButton = document.createElement('button');
			submitAnswerButton.innerHTML = "Submit";
			submitAnswerButton.classList.add('bespeek-submit', 'bespeek-submit-mc');
			submitAnswerButton.addEventListener('click', this.saveAnswer.bind(this, submitAnswerButton));
			question.appendChild(submitAnswerButton);
		} else if (q.type === "email") {
			const answerContainer = document.createElement('div');
			answerContainer.classList.add('bespeek-answer-text-container');
			const answerHolder = document.createElement('input');
			answerHolder.classList.add('bespeek-modal-answer-email');
			answerContainer.appendChild(answerHolder);
			question.appendChild(answerContainer);
			// Create our submit button
			const submitAnswerButton = document.createElement('button');
			submitAnswerButton.innerHTML = "Submit";
			submitAnswerButton.classList.add('bespeek-submit', 'bespeek-submit-email');
			submitAnswerButton.addEventListener('click', this.saveAnswer.bind(this, submitAnswerButton));
			answerContainer.appendChild(submitAnswerButton);
			const saveAnswer = this.saveAnswer.bind(this, submitAnswerButton);
			answerHolder.addEventListener('keypress', function (e) {
				// If they press enter, submit
			    if (e.keyCode === 13) {
			        saveAnswer();
			    }
			}, false);
		} else if (q.type === "finished") {
			const answerContainer = document.createElement('div');
			answerContainer.classList.add('bespeek-answer-finished-container');
			question.appendChild(answerContainer);
		}
		this.questionElements.push(question);
	}

	async createModal() {
		await this.Analytics.start();
		// SPONSOR_ID_GOES_HERE - put this in place of sponsor ids
		// SPONSOR_LOGO_GOES_HERE - put this in place of sponsor logo images
		// SPONSOR_NAME_GOES_HERE - put this in place of sponsor names
		// SPONSOR_QUADRANT_CLASS_GOES_HERE - put this in place of the classname for the sponsor button quadrant

		// Create our style element
		const stylesheet = document.createElement('link');
		stylesheet.rel = "stylesheet";
		stylesheet.type = "text/css";
		stylesheet.href = "API_URL_GOES_HERE/embedcode/css/SPONSOR_ID_GOES_HERENOCACHE";
		stylesheet.onload = this.setStyleLoaded.bind(this);

		const preloadImages = ["https://bespeek-react-assets.s3-us-west-2.amazonaws.com/widget-button-transparent2.png", "https://bespeek-react-assets.s3-us-west-2.amazonaws.com/cancel2.png", "https://bespeek-react-assets.s3-us-west-2.amazonaws.com/service.png", "https://bespeek-react-assets.s3-us-west-2.amazonaws.com/church.png", "https://bespeek-react-assets.s3-us-west-2.amazonaws.com/outdoor.png"];

		preloadImages.forEach(pl => {
			const load = document.createElement('link');
			load.rel = 'preload';
			load.as = 'image';
			load.href = pl;
			document.querySelector('head').appendChild(load);
		})

		// Get our snpm run dev-tyles on the page
		document.querySelector('head').appendChild(stylesheet);

		// Wait for our stylesheet to load before we put anything in the DOM
		await this.waitForStyle();

		// Create our main modal div
		const mainModalHolder = document.createElement('div');
		mainModalHolder.classList.add('bespeek-mid-modal');
		this.mainModal = mainModalHolder;

		// Create the button to use for opening the modal
		const modalOpen = document.createElement('div');
		modalOpen.id = 'bespeek-open-modal-button';
		modalOpen.classList.add('SPONSOR_QUADRANT_CLASS_GOES_HERE');
		modalOpen.addEventListener('click', this.toggleModal.bind(this));
		modalOpen.addEventListener('click', () => this.sendAnalytics('modal-open'));
		document.querySelector('body').appendChild(modalOpen);
		this.openButton = modalOpen;

		// Create our modal close button
		const modalClose = document.createElement('button');
		modalClose.classList.add('bespeek-modal-close');
		modalClose.addEventListener('click', this.toggleModal.bind(this));
		modalClose.addEventListener('click', () => this.sendAnalytics('modal-close'));
		mainModalHolder.appendChild(modalClose);
		this.closeButton = modalClose;

		// Create our inner modal container
		const innerModalContainer = document.createElement('div');
		innerModalContainer.classList.add('bespeek-modal-inner');
		mainModalHolder.appendChild(innerModalContainer);

		// Create our modal tooltip
		const modalTooltip = document.createElement('div');
		modalTooltip.classList.add('bespeek-modal-tooltip');
		modalTooltip.innerHTML = "<span class='tooltiptext'>Bespeek allows you to plan a little at a time. Continuously answering questions until your final wishes plan is complete.<br>Your free membership to Bespeek is compliments of this Funeral Home & gives you access to your planning profile for life!</span>";
		modalTooltip.addEventListener('mouseenter', () => modalTooltip.classList.add('show'));
		modalTooltip.addEventListener('mouseenter', () => this.sendAnalytics('tooltip-open'), {once: true});
		modalTooltip.addEventListener('mouseleave', () => modalTooltip.classList.remove('show'));
		innerModalContainer.appendChild(modalTooltip);


		// Create our modal title
		const modalTitle = document.createElement('h1');
		modalTitle.classList.add('bespeek-modal-title');
		modalTitle.innerHTML = "Free Bespeek Membership!";
		innerModalContainer.appendChild(modalTitle);
		// Create our modal description
		const modalDescription = document.createElement('p');
		modalDescription.classList.add('bespeek-modal-description');
		modalDescription.innerHTML = "Bespeek allows you to plan a little at a time. <br><b>Just answer the first 2 questions & start for free!</b>";
		innerModalContainer.appendChild(modalDescription);

		// Create our question container
		const questionHolder = document.createElement('div');
		questionHolder.classList.add('bespeek-modal-questions');
		innerModalContainer.appendChild(questionHolder);
		this.questionHolder = questionHolder;

		// Create questions
		const questions = [
			{id: 54, text: "Do you prefer your service at:", type: "multiple_choice", possible_answers: [
				{id: 36, text: "Funeral Home"},
				{id: 37, text: "A Church"},
				{id: 38, text: "An Outdoor Location"}
			], useIcons: true},
			{id: 55, text: "What song would you want to play at your service?", type: "song"}
		];
		for(let i = 0; i < questions.length; i++) {
			const q = questions[i];
			this.createQuestion(i, q);
		}

		// Create the Input/Submit container
		const userInterfaceContainer = document.createElement('div');
		userInterfaceContainer.classList.add('bespeek-user-interface');
		innerModalContainer.appendChild(userInterfaceContainer);
		
		// Create our undo button
		const undoAnswerSubmit = document.createElement('button');
		undoAnswerSubmit.innerHTML = "Undo";
		undoAnswerSubmit.classList.add('bespeek-undo-submit');
		undoAnswerSubmit.addEventListener('click', () => this.sendAnalytics('undo-answer'));
		undoAnswerSubmit.addEventListener('click', this.undoAnswer.bind(this));
		innerModalContainer.appendChild(undoAnswerSubmit);

		// Stick our modal on the page
		document.querySelector('body').appendChild(mainModalHolder);

		// Create our modal overlay
		const modalOverlay = document.createElement('div');
		modalOverlay.classList.add('bespeek-modal-overlay');
		document.querySelector('body').appendChild(modalOverlay);
		this.modalOverlay = modalOverlay;

		// Create the "question" that gathers the email address
		this.createQuestion(-1, {id: -1, text: "Complete your planning guide online with a free lifetime membership to Bespeek provided by <b>SPONSOR_NAME_GOES_HERE</b> ( $149 value). At Bespeek you can assemble all of your plans and save them securely for your survivors to access. Just enter your email below to secure your FREE membership.", type: "email"});

		// Create the "question" that shows the signup result
		this.createQuestion(-2, {id: -2, text: "", type: "finished"});

		// Show our questions
		this.questionElements.forEach(qe => questionHolder.appendChild(qe));
		this.sendAnalytics('loaded');
	}

	undoAnswer() {
		const currentQuestion = document.querySelector('div.bespeek-mid-modal').querySelector('div.bespeek-modal-question.visible');
		if (!currentQuestion) return;
		const q = JSON.parse(currentQuestion.dataset.question);
		const qInd = this.questionElements.findIndex(qe => Number(qe.dataset.id) === q.id);
		const qObj = this.questionElements[qInd];
		const oldQuestion = this.questionElements[qInd - 1];
		if (!oldQuestion) return;
		const oldQid = Number(oldQuestion.dataset.id);
		const answer = this.answerObjects.find(ao => Number(ao.questionId) === oldQid);
		qObj.classList.remove('visible');
		oldQuestion.classList.add('visible');
		if (oldQuestion.dataset.type === "text") {
			oldQuestion.querySelector('.bespeek-modal-answer-text').value = answer.answer;
		} else if (oldQuestion.dataset.type === "multiple_choice" || oldQuestion.dataset.type === "boolean") {
			oldQuestion.querySelector('.bespeek-modal-possible-answers').querySelectorAll('input[type=radio]').forEach(a => {
				if (a.value === answer.answer) {
					a.checked = true;
				}
			});
		} else if(oldQuestion.dataset.type === "email") {

		}
	}

	async saveAnswer(button) {
		const question = button.closest('div.bespeek-modal-question');
		const qType = question.dataset.type;
		let answerText = null;
		let answerId = null;
		if (qType === "text" || qType === "song") {
			const answerContainer = question.querySelector('.bespeek-modal-answer-text');
			if (!answerContainer.value || !answerContainer.value.length) {
				alert("Please provide an answer.");
				return;
			}		
			answerText = answerContainer.value;
		} else if (qType === "multiple_choice") {
			const answerContainer = question.querySelector('.bespeek-modal-possible-answers');
			const currentAnswer = answerContainer.querySelector('input[type=radio]:checked');
			if (!currentAnswer) {
				alert("Please choose an answer.");
				return;
			}
			answerText = Number(currentAnswer.value);
			answerId = answerText;
		} else if (qType === "email") {
			const answerContainer = question.querySelector('.bespeek-modal-answer-email');
			if (!answerContainer.value || !answerContainer.value.length) {
				alert("Please provide your email.");
				return;
			} else if (!this.validateEmail(answerContainer.value)) {
				alert("Please provide a valid email address.");
				return;
			}
			answerText = answerContainer.value;
		} else {
			console.log({qType, question})
		}
		this.sendAnalytics('save-answer');
		const q = JSON.parse(question.dataset.question);
		if (qType !== "email") {
			// Answers that aren't an email address
			// are actually answers
			const answerObj = {
				questionId: q.id,
				answer: answerText,
				type: qType === "song" ? "text" : qType
			};
			if (answerId) answerObj.possible_answer_id = answerId;
			this.answerObjects.push(answerObj);
		}
		const qInd = this.questionElements.findIndex(qe => Number(qe.dataset.id) === q.id);
		if (q.type === "email") {
			// If we have the email, we are now done unless there's an error
			document.querySelector('.bespeek-undo-submit').style.display = "none";
			const email = answerText;
			const finished = document.querySelector('.bespeek-answer-finished-container');
			finished.innerHTML = "Signing you up for your new account...";
			const currentQuestion = this.questionElements[qInd];
			const newQuestion = this.questionElements[qInd + 1];
			if (newQuestion) {
				currentQuestion.classList.remove('visible');
				newQuestion.classList.add('visible');
			} else {
				console.log("No new question?");
			}
			const obj = this;
			const result = await this.sendFinalAnswers(email).catch(err => {
				let errText = "";
				if (err.code === 409) {
					errText = "You already have an account with Bespeek. You can <a target='_blank' href='#'>Sign In</a> to view your profile and continue planning.";
				} else {
					errText = err.error;
				}
				// finished.classList.add('bespeek-modal-error');
				obj.undoAnswer();
				alert("There was an error creating your account: " + errText);				

			});
			if (result && result.success) {
				finished.innerHTML = "You've officially signed up for Bespeek! We've sent you an email with a link to create a password and confirm your registration. If you don't receive this email, please reach out to us at <a href='mailto:support@bespeek.com.'>support@bespeek.com.</a>";
				finished.classList.add('bespeek-modal-success');
			} else {
				console.log({result})
			}
		} else {
			if (qInd > -1) {
				const currentQuestion = this.questionElements[qInd];
				const newQuestion = this.questionElements[qInd + 1];
				if (newQuestion) {
					currentQuestion.classList.remove('visible');
					newQuestion.classList.add('visible');
				} else {
					console.log("No new question?");
				}
			} else {
				console.log("ERRRR");
			}
		}
	}

	sendFinalAnswers(email) {
		const cls = this;
		const url = "API_URL_GOES_HERE/remote/signup";
		const answers = cls.answerObjects;
		let code = 200;
		cls.sendAnalytics('send-final-answers');
		return new Promise((resolve, reject) => {
			fetch(url, {
			    method: 'post',
			    headers: {
			      'Content-Type': 'application/json'
			    },
			    body: JSON.stringify({email, answers, sponsorId: SPONSOR_ID_GOES_HERE})
			  })
			  .then(res => { code = res.status; return res; })
			  .then(res => res.json())
			  .then(json => {
			    if (!json.success) {
			      cls.sendAnalytics('send-final-answers-error');
			      reject({code, error: json.error});
			    } else {
			      cls.sendAnalytics('send-final-answers-complete');
			      resolve(json);
			    }
			    return json;
			  })
			  .catch(error => reject(error));
		});
	}
}
const bspkModal = new bespeekModal();
bespeekReady(bspkModal.createModal.bind(bspkModal));