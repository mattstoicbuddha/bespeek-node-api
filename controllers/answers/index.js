const rootDir = process.cwd(),
		model = require(rootDir + "/models"),
		QuestionsCategories = model.mariadb.questions_categories,
		Questions = model.mariadb.questions,
		Answers = model.mariadb.answers,
		h = require(rootDir + "/helpers"),
		answerHelpers = require("./helpers"),
		Redis = model.redis;


/**
 * Save an answer
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const saveAnswer = async (req, callback) => {
	console.log({"SAVE ANSWER PARAMS": req.params})
	const {user} = req;
	const {answer, possible_answer_id, questionId} = req.params;
	if ((!answer && !possible_answer_id) || (answer && answer.length < 1 && !possible_answer_id)) {
		// We are missing an actual answer here, so we need to let the user
		// know that they didn't give us what we need
		return callback(h.createError(400, "No answer present"));
	} else if (possible_answer_id && (isNaN(possible_answer_id) || possible_answer_id < 0)) {
		return callback(h.createError(400, "Invalid answer present"));
	}
	// Create a new instance of our journal model; if we don't, we start building some pretty massive
	// queries, which is confusing and awful
	const answersModel = new Answers;
	const questionsModel = new Questions;
	const questionsCategoriesModel = new QuestionsCategories;
	const question = await questionsModel.getQuestionById(questionId).catch(err => {
		return callback(h.createError(500, err));
	});
	if (question.type === 'text' && (answer && answer.length < 1 || !answer)) {
		return callback(h.createError(400, "No answer present."));
	} else if (question.type === 'boolean') {
		if (!possible_answer_id) return callback(h.createError(400, "No answer present."));
	} else if (question.type === 'multiple_choice') {
		if (!possible_answer_id) return callback(h.createError(400, "No answer present."));
	}
	const rows = await answersModel.saveAnswer(user.id, questionId, answer, possible_answer_id).catch(err => {
		return callback(h.createError(500, err));
	});
	if (rows) {
		const redisKey = "publishUserInfo:user_id:" + user.id;
		const redisModel = new Redis;
		await redisModel.del(redisKey).catch(e => console.log(e));
		const successMsg = await answerHelpers.parseAnswerUpdate(question, possible_answer_id, user.id);
		if (h.isError(successMsg)) return callback(successMsg);
		let returnObj = {
			message: successMsg
		}
		if (question.type === "boolean") {
			if (possible_answer_id === 1 && question.true_cat_unlock) {
				returnObj.category_unlocked = true;
				const cat = await questionsCategoriesModel.getCategory(question.true_cat_unlock);
				if (Array.isArray(cat)) returnObj.category = cat[0];
			} else if (possible_answer_id === 0 && question.false_cat_unlock) {
				returnObj.category_unlocked = true;
				const cat = returnObj.category = await questionsCategoriesModel.getCategory(question.false_cat_unlock);
				if (Array.isArray(cat)) returnObj.category = cat[0];
			}
		}
		return callback(null, returnObj);
		
	} else {
		return callback(h.createError(500, "Could not save, unknown error occurred"));
	}
}

/**
 * Update an answer
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const updateAnswer = async (req, callback) => {
	const {user, params} = req;
	console.log({params})
	const {answer, possible_answer_id, questionId} = params;
	if (!possible_answer_id && (!answer || answer.length < 1)) {
		// We are missing an actual answer here, so we need to let the user
		// know that they didn't give us what we need
		return callback(h.createError(400, "No answer present"));
	} else if (possible_answer_id && (isNaN(possible_answer_id) || possible_answer_id < 0)) {
		return callback(h.createError(400, "Invalid answer present"));
	}
	// Create a new instance of our journal model; if we don't, we start building some pretty massive
	// queries, which is confusing and awful
	const answersModel = new Answers;
	const questionsModel = new Questions;
	const question = await questionsModel.getQuestionById(questionId).catch(err => err);
	if (h.isError(question)) return callback(question);
	if (question.type === 'text' && (!answer || answer && answer.length < 1)) {
		return callback(h.createError(400, "No answer present."));
	} else if (question.type === 'boolean') {
		if (!possible_answer_id) return callback(h.createError(400, "No answer present."));
	} else if (question.type === 'multiple_choice') {
		if (!possible_answer_id) return callback(h.createError(400, "No answer present."));
	}
	const rows = await answersModel.updateAnswer(user.id, questionId, answer, possible_answer_id).catch(err => err);
	if (h.isError(rows)) return callback(rows);
	if (rows) {
		const successMsg = await answerHelpers.parseAnswerUpdate(question, possible_answer_id, user.id).catch(err => err);
		return callback(null, successMsg);
	} else {
		return callback(h.createError(500, "Could not save, unknown error occurred"));
	}
}

/**
 * Get the answers a user has submitted, along with the questions for them
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getAnswers = (req, callback) => {
	const {user} = req;
	// Create a new instance of our journal model; if we don't, we start building some pretty massive
	// queries, which is confusing and awful
	const answersModel = new Answers;
	answersModel
	.getAnswersByUserId(user.id)
	.then(answers => {
			if (answers && answers.length) {
				return callback(null, {answers});
			}
			return callback(h.createError(404, "No answers found."));
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}


module.exports = {
	saveAnswer,
	updateAnswer,
	getAnswers
}