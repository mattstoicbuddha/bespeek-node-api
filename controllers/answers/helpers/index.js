const rootDir = process.cwd(),
			userHelper = require(rootDir + "/helpers/users");

const parseAnswerUpdate = (question, possible_answer_id, userId) => {
	return new Promise((resolve, reject) => {
		if (question.type === "boolean") {
			if (
					possible_answer_id === 0 && !question.false_cat_unlock ||
					possible_answer_id === 1 && !question.true_cat_unlock
				) {
				return resolve("Saved");
			}
		}
		if (question.type === "multiple_choice") {
			if (
					!question.possible_answers ||
					!Array.isArray(question.possible_answers) ||
					!question.possible_answers.length
				) {
					// If we don't have any possible answers to iterate through,
					// we don't need to refresh anything because we won't be
					// updating accessable categories
					return resolve("Saved");
				}
				
				return resolve("Saved");
		}
		// Make sure to publish our new user info to our cache
		userHelper.publishUserInfo(userId)
			.then(success => {
				return resolve("Saved");
			})
			.catch(err => {
				return reject(err);
			});
		});
}

module.exports = {
	parseAnswerUpdate
}