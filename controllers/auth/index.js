const model = require(process.cwd() + "/models"),
		Users = model.mariadb.users,
		UsersMeta = model.mariadb.users_meta,
		Sponsors = model.mariadb.sponsors,
  		config = require(process.cwd() + "/config"),
		h = require(process.cwd() + "/helpers"),
		bcrypt = require('bcrypt'),
		Redis = model.redis,
		jwt = require('jsonwebtoken');

/**
 * Request an API token; essentially is user login
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const requestToken = async (req, callback) => {
	const {params} = req;
	if (!params.email || !params.password) {
		return callback(h.createError(400, "Missing one or more necessary authentication parameters."));
	}
	// See if a user with that email already exists
	const usersModel = new Users;
	const user = await usersModel.findUserByEmail(params.email).catch(err => {
		console.log("Error:", err);
		return callback(h.createError(500, err));
	});
	if (!user) {
		return callback(h.createError(404, "That user does not exist."));
	}
	if (!user.registration_confirmed) {
		console.log({user})
		// Should we resend an email here? Maybe
		return callback(h.createError(401, "You have not confirmed your account."));
	}
	const returnObj = {
		role: user.role
	};
	const sponsorsModel = new Sponsors;
	const sponsor = await sponsorsModel.findSponsorByUserId(user.id)
		.then(s => s) // Hack to ensure we hit the sponsor MariaDB middleware
		.catch( err => {
			console.log("error", err);
			return false;
		});
	if (sponsor) {
		returnObj.sponsor_info = sponsor;
	}
	// Check the password to make sure it matches
	bcrypt.compare(params.password, user.password, async function(err, match) {
		if (err || !match) {
			return callback(h.createError(401, err || "That password is incorrect."));
		}
		// Create our token to let us know which user we have
		const tokenInfo = {
			exp: Math.floor(Date.now() / 1000) + (60 * 60 * 12),
			id: user.id,
			role: user.role,
			type: "user"
		};
		if (returnObj.sponsor_info) {
			tokenInfo.type = "sponsor";
			tokenInfo.sponsor_id = returnObj.sponsor_info.id;
		}
		const token = jwt.sign(tokenInfo, config.jwt.secret);
		if (!token) return callback("Unable to create authentication token.");
		returnObj.token = token;
		// We need to add the last login user meta, but it doesn't need to
		// block our execution so we don't require the promise to fulfill
		// before returning our token
		const metaModel = new UsersMeta;
		await metaModel.upsertMetaByUserId(user.id, "last_login_timestamp", Date.now())
			.catch(err => {
				console.log(err);
			});

	  const redisModel = new Redis;
	  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	 	const redisKey = config.brute_force.redis_key_base + ip;
	 	redisModel.del(redisKey)
	 		.then(result => {
				// We have a token, so we can send it back
				return callback(null, returnObj);
			})
			.catch(err => {
				// We have a token, so we can send it back
				return callback(null, returnObj);
			});
	});
}

module.exports = {
	requestToken
}