const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(process.cwd() + "/models"),
		AnalyticsModel = model.mariadb.analytics,
		h = require(process.cwd() + "/helpers"),
		uuidv4 = require('uuid').v4,
		valid = require(process.cwd() + "/validation/analytics");

const saveAnalytics = async (req, callback) => {
	const {params} = req;
	const timestamp = Date.now();
	const cloneParams = Object.assign({timestamp}, params);
	// Ensure the analytics object is properly formatted and has the proper data
	const invalid = h.checkValidation(valid.save, cloneParams);
	if (invalid && invalid.error) {
		console.log(invalid.error_object)
		// If the validation fails, we can't create the user
		return callback(invalid.error_object);
	}
	const {event, url, category, browser, session} = params;
	const analyticsObj = {
		event,
		category,
		session,
		timestamp
	}
	if (url) {
		const ip_address = req.connection.remoteAddress;
		analyticsObj.url = url;
		analyticsObj.ip_address = ip_address;
		analyticsObj.browser = browser;
	}
	const Analytics = new AnalyticsModel;
	const saved = await Analytics.save(analyticsObj).catch(e => e);
	if (h.isError(saved)) return callback(saved);
	return callback(null, "Saved");
}

const startAnalyticsSession = async (req, callback) => {
	const timestamp = Date.now();
	const session = uuidv4();
	const analyticsObj = {
		event: "analytics-start",
		category: "core-events",
		session,
		timestamp
	}
	const Analytics = new AnalyticsModel;
	const saved = await Analytics.save(analyticsObj).catch(e => e);
	if (h.isError(saved)) return callback(saved);
	return callback(null, {session});
}

module.exports = {
	saveAnalytics,
	startAnalyticsSession
}