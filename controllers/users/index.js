const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(process.cwd() + "/models"),
		fs = require('fs'),
		UsersModel = model.mariadb.users,
		AnswersModel = model.mariadb.answers,
		bcrypt = require('bcrypt'),
		h = require(process.cwd() + "/helpers"),
		MailHelper = require(process.cwd() + "/helpers/mail"),
		saltRounds = 10,
		valid = require(process.cwd() + "/validation/users"),
		validSponsor = require(process.cwd() + "/validation/sponsors");

const sendConfirmation = async (email, opts) => {
	const usersModel = new UsersModel;
	const user = await usersModel.findUserByEmail(email).catch(err => { throw new Error(err) });
	if (!user) {
		return Promise.reject(h.createError(404, new Error('User does not exist.')));
	}
	if (user.registration_confirmed) {
		return Promise.reject(h.createError(403, new Error('User is already confirmed.')));
	}
	let template = "registration_welcome";
	const templateKeys = {
		"BESPEEK_REGISTRATION_LINK": config.api_url + "/users/confirmregistration/" + user.registration_code,
	}
	if (opts.sponsor) {
		template = "sponsor_registration_welcome";
		templateKeys.BESPEEK_SPONSOR_REGISTRATION_LINK = templateKeys.BESPEEK_REGISTRATION_LINK;
		delete templateKeys.BESPEEK_REGISTRATION_LINK;
	}
	if (opts.connection) {
		template = "connection_registration_welcome";
		templateKeys.BESPEEK_CONNECTION_REGISTRATION_LINK = templateKeys.BESPEEK_REGISTRATION_LINK;
		templateKeys.BESPEEK_USER = opts.bespeek_user;
		delete templateKeys.BESPEEK_REGISTRATION_LINK;
	}
	const mail = new MailHelper;
	mail.setOptions({
		to: email,
		subject: "Welcome to Bespeek!",
		template,
		templateKeys
	});
	return await mail.send().then(res => res).catch(err => { throw new Error(err) });
}

const getPasswordHash = p => {
	return new Promise((resolve, reject) => {
		bcrypt.hash(p, saltRounds, function(err, hash) {
			if (err) return reject(err);
			return resolve(hash);
		});
	});
}

const createUser = async (params, sendEmail = true, requireConfirmation = true) => {
		let validator = valid.registration;
		let isSponsor = false;
		let isConnection = false;
		const emailOpts = {sponsor: false, connection: false};
		if (params.remote) {
			validator = valid.remoteRegistration;
		} else if (params.isSponsor) {
			delete params.isSponsor;
			emailOpts.sponsor = true;
		} else if (params.isConnection) {
			delete params.isConnection;
			emailOpts.connection = true;
		}
		// Ensure the email is valid
		const invalid = h.checkValidation(validator, params);
		if (invalid && invalid.error) {
			console.log(invalid.error_object)
			// If the validation fails, we can't create the user
			throw invalid.error_object;
			return;
		}
		// See if a user with that email already exists
		const usersModel = new UsersModel;
		const user = await usersModel.findUserByEmail(params.email).catch(err => {console.log(err); throw h.createError(400, err)});
		if (user) {
			console.log({WE_HAVE_A_USER: user});
			throw h.createError(409, "That email already exists in our system.");
			return;
		}
		// If we are remote, we need to have them set their password when they confirm their acct,
		// so we pass a dummy value that basically locks their account until they have a password
		let hash = "remote_pass";
		if (!params.remote) {
			hash = await getPasswordHash(params.password).catch(err => {console.log(err); throw h.createError(400, err)});
		}
		const userObj = {
			email: params.email,
			password: hash,
			zip_code: params.zip_code || 0, // We don't actually *need* zip code when we register
			role: 100
		}
		if (requireConfirmation) {
			userObj.registration_code = h.generatePasswordReset(); //Password reset tokens work fine here
			userObj.registration_code_expires = (Date.now() / 1000) + (3600 * 12); // Expires in 12 hours
		} else {
			userObj.registration_confirmed = 1;
		}
		// We don't force first and last name to be required in the validation because we will
		// be registering users outside of the context of a regular registration form, so we
		// can't enforce certain fields we will not have in all contexts
		if (params.first_name && params.last_name) {
			userObj.first_name = params.first_name;
			userObj.last_name = params.last_name;
		}
		if (emailOpts.sponsor) {
			userObj.role = 200;
		} else if (params.sponsor_id) {
			userObj.sponsor_id = params.sponsor_id;
		}
		if(emailOpts.connection) {
			let bespeekName = "";
			let bespeekEmail = userObj.email;
			if (params.first_name) bespeekName = params.first_name + " ";
			if (params.last_name) bespeekName += params.last_name + " ";
			if (bespeekName.length) bespeekEmail = "<" + bespeekEmail + ">";
			emailOpts.bespeek_user = bespeekName + bespeekEmail;
		}
		// Create the user since they have passed validation and don't exist
		const result = await usersModel.registerUser(userObj).catch(err => {console.log(err); return h.createError(400, err)});
		if (h.isError(result)) {
			throw result;
		}
		userObj.id = result.shift();
		if (sendEmail) {
			await sendConfirmation(userObj.email, {sponsor: isSponsor, connection: isConnection});
		}
		return userObj;
}

/**
 * Register a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const registerUser = (req, callback) => {
	const {params} = req;
	const invalid = h.checkValidation(valid.registration, params);
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	createUser(params, true)
		.then(result => {
			return callback(null, {message: "User Created", id: result.id});
		}).catch( err => {
			console.log("error", err);
			return callback(err, null);
		});
}

/**
 * Confirm a user's registration confirmation code
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const confirmRegistration = (req, callback) => {
	const {params} = req;
	const usersModel = new UsersModel;
	usersModel
		.getUserByConfirmationCode(params.confirmation_code)
		.then(user => {
			if (user) {
				usersModel
					.confirmUser(user.id)
					.then(() => {
						return callback(null, {confirmed: true})
					}).catch( err => {
						console.log("error", err);
						return callback(h.createError(500, err), null);
					});
			} else {
				return callback(h.createError(404, "A user with that confirmation code could not be found."), null);
			}
		}).catch( err => {
			console.log("error", err);
			return callback(h.createError(500, err), null);
		});
}

/**
 * Get a user's profile
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getProfile = (req, callback) => {
	const {user} = req;
	getProfileById(user.id)
			.then(profile => {
				return callback(null, {profile});
			}).catch( err => {
				console.log("error", err);
				return callback(h.createError(500, err), null);
			});
}
/**
 * Get a user's profile by their id
 * @since 1.0.0
 *
 * @param {integer} id - The user id
 * @returns {function}
 */
const getProfileById = (id) => {
	const usersModel = new UsersModel;
	return usersModel.getUserProfileById(id);
}

/**
 * Get a user's Story (death) page
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getStory = (req, callback) => {
	const {user} = req;
	const usersModel = new UsersModel;
	usersModel
		.getUserStoryById(user.id)
			.then(story => {
				return callback(null, {story});
			}).catch( err => {
				console.log("error", err);
				return callback(h.createError(500, err), null);
			});
}

/**
 * Set a password reset code for a user and email them a reset link
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const sendPasswordResetCode = async (req, callback) => {
	const {email} = req.query;
	if (!email) {
		return callback(h.createError(400, "No email received for password reset."));
	}
	const invalid = h.checkValidation(valid.email, {email});
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null);
	}

	const passwordReset = await setPasswordResetCode(email).catch(err => { throw new Error(err) });
	if (!passwordReset) return callback(h.createError(404, "A password reset code could not be set for that user. Is the email address correct?"));
	
	const mail = new MailHelper;
	mail.setOptions({
		to: email,
		subject: "Your Bespeek Password Reset",
		template: "password_reset",
		templateKeys: {
			"BESPEEK_PASSWORD_RESET_LINK": config.api_url + "/users/resetpassword/" + passwordReset.password_reset_code
		}
	});
	await mail.send().catch(err => { throw new Error(err) });
	return callback(null, "Sent");
};

/**
 * Set a password reset code for a user signed up via embed code, so they can set their pass initially
 * @since 1.0.0
 *
 * @param {string} email - The email for the user
 * @returns {function}
 */
const sendRemotePasswordInit = async (email) => {
	if (!email) {
		throw new Error(h.createError(400, "No email received for password reset."));
	}
	const passwordReset = await setPasswordResetCode(email);
	
	const mail = new MailHelper;
	mail.setOptions({
		to: email,
		subject: "Your Bespeek Signup",
		template: "remote_signup",
		templateKeys: {
			"BESPEEK_PASSWORD_RESET_LINK": config.api_url + "/users/createpassword/" + passwordReset.password_reset_code
		}
	});
	await mail.send().catch(err => { throw new Error(err) });
	return true;
};

/**
 * Set a password reset code for a user
 * @since 1.0.0
 *
 * @param {string} email - The email address of the user
 * @returns {string || null}
 */
const setPasswordResetCode = async (email) => {
	const usersModel = new UsersModel;
	const updated = await usersModel.setPasswordResetCode(email).catch(err => { throw new Error(err) });
	if (updated) {
		return await usersModel.getUserPasswordResetCode(email).catch(err => { throw new Error(err) });
	}
	return null;
}

/**
 * Validate a user's password reset code
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const validatePasswordResetCode = (req, callback) => {
	const resetCode = req.params.reset_code;
	if (!resetCode || !resetCode.length) {
		return callback(h.createError(400, "No password reset code received."));
	}
	const usersModel = new UsersModel;
	usersModel.getUserByPasswordResetCode(resetCode)
		.then(user => {
			if (!user) {
				return callback(h.createError(404, "No user found with that password reset code."));
			}
			return callback(null, {valid: true});
		})
		.catch(err => {
			return callback(h.createError(500, err));
		});
}

/**
 * Reset a user's password
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const resetPassword = (req, callback) => {
	const {params} = req;
	const invalid = h.checkValidation(valid.passwordReset, params);
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	const resetCode = req.params.reset_code;
	const password = req.params.password_first;

	bcrypt.hash(password, saltRounds, function(err, hash) {

		const usersModel = new UsersModel;
		usersModel.updateUserPassword(resetCode, hash)
			.then(user => {
				if (!user) {
					return callback(h.createError(404, "No user found with that password reset code."));
				}
				return callback(null, {message: "Password successfully reset."});
			})
			.catch(err => {
				return callback(h.createError(500, err));
			});
	});
}

/**
 * Update a user's profile; this function is only exposed via the direct user update route
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const updateProfile = async (req, callback) => {
	console.log("Should start the update")
	const {user} = req;
	const updateableParams = {};
	const allowed = ["first_name", "last_name", "zip_code", "profile_image"];
	for(const p of Object.keys(req.params)) {
		if(allowed.find(a => a === p)) {
			updateableParams[p] = req.params[p];
		}
	}
	const update = await updateUser(user.id, updateableParams).catch(err => err);
	console.log({update})
	if (update && !h.isError(update)) return callback(null, "Successfully saved profile info!");
	return callback(update);
}

/**
 * Update a user's info; this function is exported so it can be used elsewhere to update user info
 * @since 1.0.0
 *
 * @param {integer} id - The user id for the profile we are updating
 * @param {array} params - The parameters to update within the profile
 * @returns {Promise}
 */
const updateUser = (id, params) => {
	return new Promise(async (resolve, reject) => {
		if (!id || !params || !Object.keys(params).length) return reject(h.createError(400, "Missing values to update user."));
		let profileImage = null;
		const cloneParams = {
			id
		};
		if (params.profile_image) {
			profileImage = params.profile_image;
			profileImage = params.profile_image;
		}
		if (params.first_name) {
			cloneParams.first_name = params.first_name;
		}
		if (params.last_name) {
			cloneParams.last_name = params.last_name;
		}
		if (params.zip_code) {
			cloneParams.zip_code = params.zip_code;
		}
		const invalid = h.checkValidation(valid.profileUpdate, Object.assign({profile_image: params.profile_image}, cloneParams));
		if (invalid && invalid.error) {
			console.log(invalid.error_object)
			// If the validation fails, we can't update the user
			return reject(invalid.error_object);
		}
		const usersModel = new UsersModel;		
		if (Object.keys(cloneParams).length > 1) {
			// If we have more than one key in the params, we have something to
			// update; otherwise, we only have an id
			const updateUser = await usersModel.updateUser(cloneParams).catch(err => h.createError(500, err));
			if (!updateUser || h.isError(updateUser)) return reject(updateUser);
			if (profileImage) {
				const fileInfo = await h.uploadBase64Image(profileImage, config.aws.assets_bucket, config.aws.profile_images_dir).catch(reject);
				const fileName = fileInfo.name;
				const finalUpdate = await usersModel.updateUser({id, profile_image: fileName}).catch(err => h.createError(500, err));
				if (!finalUpdate || h.isError(finalUpdate)) return reject(finalUpdate);
				return resolve("Successfully saved profile info!");
			} else {
				return resolve("Successfully saved profile info!");
			}
		} else if (profileImage) {
			const fileInfo = await h.uploadBase64Image(profileImage, config.aws.assets_bucket, config.aws.profile_images_dir).catch(reject);
			const fileName = fileInfo.name;
			const finalUpdate = await usersModel.updateUser({id, profile_image: fileName}).catch(err => h.createError(500, err));
			if (!finalUpdate || h.isError(finalUpdate)) return reject(finalUpdate);
			return resolve("Successfully saved profile image!");
		}
	});
}

/**
 * Find a user by their email
 * @since 1.0.0
 *
 * @param {string} email - The email to look for
 * @returns {Object}
 */
const findUserByEmail = async (email) => {
	const usersModel = new UsersModel;
	return await usersModel.findUserByEmail(email)
}

/**
 * Find users by their emails
 * @since 1.0.0
 *
 * @param {Array.string} emails - The emails to look for
 * @returns {Array.Object}
 */
const findUsersByEmail = async (emails) => {
	const usersModel = new UsersModel;
	return await usersModel.findUsersByEmail(emails)
}


/**
 * Sign new users up from other sites
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const remoteSignup = async (req, callback) => {
	const {params} = req;
	const {email, answers, sponsorId} = params;
	const userParams = {
		email: email,
		sponsor_id: sponsorId,
		remote: true
	}
	if (!answers || !answers.length) {
		return callback(h.createError(400, "No answers given during remote signup process."));
	}
	const userResult = await createUser(userParams, false, false).catch(err => err);
	if (userResult && !h.isError(userResult)) {
		const result = await sendRemotePasswordInit(userParams.email).catch(err => err);
		if (result && !h.isError(result)) {
			// Figure out which questions were answered and set up
			// the answer values as part of their profile
			const answersModel = new AnswersModel;
			for(let i = 0; i < answers.length; i++) {
				const answer = answers[i];
				const answerVal = answer.answer;
				const possAnswerId = answer.type === "multiple_choice" || answer.type === "boolean" ? answer.possible_answer_id : null;
				await answersModel.saveAnswer(userResult.id, answer.questionId, answerVal, possAnswerId).catch(err => err);
			}			
			return callback(null, "Success");
		}
		return callback(result);
	}
	return callback(userResult);

}

module.exports = {
	confirmRegistration,
	createUser,
	findUserByEmail,
	findUsersByEmail,
	registerUser,
	getProfile,
	getStory,
	resetPassword,
	sendPasswordResetCode,
	validatePasswordResetCode,
	updateProfile,
	sendConfirmation,
	getProfileById,
	updateUser,
	remoteSignup
}