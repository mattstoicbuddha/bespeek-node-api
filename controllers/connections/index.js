const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		model = require(process.cwd() + "/models"),
		fs = require('fs'),
		Users = require("../users"),
		UsersModel = model.mariadb.users,
		SponsorsModel = model.mariadb.sponsors,
		ConnectionsModel = model.mariadb.connections,
		bcrypt = require('bcrypt'),
		h = require(process.cwd() + "/helpers"),
		MailHelper = require(process.cwd() + "/helpers/mail"),
		saltRounds = 10,
		valid = require(process.cwd() + "/validation/users");

/**
 * Get a user's connections
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getConnections = (req, callback) => {
	const {user} = req;
	// Find the user's connections,if they have any
	const connectionsModel = new ConnectionsModel;
		connectionsModel.getConnectionsByUserId(user.id)
			.then(connections => {
				return callback(null, {connections});
			}).catch( err => {
				console.log("error", err);
				return callback(h.createError(500, err), null);
			});
}

/**
 * Add connections for a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const addConnections = async (req, callback) => {
	const {user} = req;
	const {connection_email} = req.params;
	const invalid = h.checkValidation(valid.email, {email: connection_email});
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	const currentUser = await Users.getProfileById(user.id).catch( err => {
		console.log("error", err, user);
		return callback(h.createError(500, err), null);
	});
	console.log("Setting sponsor id...", {currentUser});
	const sponsor_id = currentUser.user.sponsor_id;
	// Find the user's connections,if they have any
	const connectionsModel = new ConnectionsModel;
	console.log("Getting the conns...");
	const currentConns = await connectionsModel.getConnectionsByUserId(user.id).catch( err => {
		console.log("error", err);
		return callback(h.createError(500, err), null);
	});
	console.log("Checking to see if user exists...");
	const emailsQuery = await Users.findUserByEmail(connection_email).catch( err => {
		console.log("error", err);
		return callback(h.createError(500, err), null);
	});
	const userRows = [];
	// If we have no users for that email, it is
	// time to start inviting people to Bespeek!
	if (!emailsQuery) {
		console.log("User doesn't exist...");
		const newUserObj = {email: connection_email, password: h.generatePasswordReset(), isConnection: true};
		// If the user doesn't have a sponsor, this will cause an error if we try to pass a 0 value
		// so we do a check first
		if (sponsor_id) newUserObj.sponsor_id = sponsor_id;
		const newUser = await Users.createUser(newUserObj, true, true).catch(err => { callback(err) });
		userRows.push(newUser);
	} else {
		console.log("User exists, seeing if connection already exists...");
		const hasConn = currentConns.find(cc => cc.email === emailsQuery.email && cc.added_by_user);
		if (hasConn) return callback(h.createError(400, "That connection already exists."));
		userRows.push(emailsQuery);
	}
	getConnections(req, (err, conns) => {
		if (err) return callback(err);
		const {connections} = conns;
		console.log({connections})
		const filteredUsers = userRows.filter(ur => {
			const exists = connections.find(cn => ur.id === cn.user_id && ur.added_by_user);
			return !exists
		}).map(ur => ur.id);
		console.log({filteredUsers})
		connectionsModel.addConnectionsByUserIds(user.id, filteredUsers)
			.then(results => {
				return callback(null, {results});
			})
			.catch( err => {
				console.log("error", err);
				return callback(h.createError(500, err), null);
			});
	});
}

/**
 * Delete connections for a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const deleteConnections = async (req, callback) => {
	const {user} = req;
	if (!req.params.connection_ids.length) {
		return callback(h.createError(400, "No connection ids received."), null);
	}
	const {connection_ids} = req.params;

	const invalid = h.checkValidation(valid.connectionIds, {connection_ids});
	if (invalid && invalid.error) {
		// If the validation fails, we can't create the user
		return callback(invalid.error_object, null)
	}
	if (!connection_ids.length) {
		return callback(h.createError(400, err), null);
	}

	// Find the user's connections,if they have any
	const connectionsModel = new ConnectionsModel;
	connectionsModel.deleteConnectionsByUserIds(user.id, connection_ids)
		.then(results => {
			return callback(null, {results});
		})
		.catch( err => {
			console.log("error", err);
			return callback(h.createError(500, err), null);
		});
}

/**
 * Get a connection's profile
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getProfile = async (req, callback) => {
	const currentUser = req.user;
	const userId = Number(req.params.userId);
	if (!userId) {
		return callback(h.createError(400, "No user id present in request."), null);
	}
	const usersModel = new UsersModel;
	const profileUser = await usersModel.findUserById(userId).catch(e => h.createError(500, e));
	if (profileUser && profileUser.isApiError) {
		return callback(profileUser);
	}
	let hasConn = false;
	if (req.user.type === 'sponsor' && profileUser.sponsor_id) {
		const sponsorsModel = new SponsorsModel;
		// If we are a sponsor, determine which one the correct one
		const sponsor = await sponsorsModel.findSponsorByUserId(currentUser.id).catch(e => h.createError(500, e));
		if (sponsor && sponsor.isApiError) {
			return callback(sponsor);
		}
		// If we couldn't find the sponsor or they aren't the same as the sponsor for the user,
		// we deny access.
		if (!sponsor || sponsor.id !== profileUser.sponsor_id) {
			return callback(h.createError(403, "You are not sponsoring this user and cannot view their profile."));
		}
		hasConn = true;
	} else {
		const connectionsModel = new ConnectionsModel;
		const connections = await connectionsModel.getConnectionsByUserId(currentUser.id).catch(e => h.createError(500, e));
		if (!Array.isArray(connections) || connections.isApiError) {
			return callback(connections);
		}
		hasConn = connections.find(c => c.user_id === userId);
		console.log({connections, hasConn})
	}
	if (!hasConn) {
		return callback(h.createError(403, "You do not have access to this user's profile."));
	}
	const userProfile = await usersModel.getUserProfileById(userId, true).catch(e => h.createError(500, e));
	if (userProfile && userProfile.isApiError) {
		return callback(userProfile);
	}
	// We don't need connections
	delete userProfile.connections;
	return callback(null, userProfile);
}

module.exports = {
	addConnections,
	getConnections,
	deleteConnections,
	getProfile
}