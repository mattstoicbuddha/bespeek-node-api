const rootDir = process.cwd(),
			model = require(rootDir + "/models"),
			Questions = model.mariadb.questions,
			h = require(rootDir + "/helpers"),
	    usersHelper = require(rootDir + "/helpers/users");

/**
 * Get a new question for a user
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getNewQuestion = async (req, callback) => {
	const {user} = req;
	// Create a new instance of our journal model; if we don't, we start building some pretty massive
	// queries, which is confusing and awful
	const questionsModel = new Questions;

	const userInfo = await usersHelper.getPublishedUserInfo(user.id).catch(e => h.createError(500, e));
	console.log({userInfo})
	if (h.isError(userInfo)) {
		return callback(userInfo);
	}
	const question = await questionsModel.getNewQuestion(user.id, userInfo).catch(e => h.createError(500, e));
	if (h.isError(question)) {
		return callback(question);
	}
	return callback(null, question);
}

/**
 * Get question by its id
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getQuestionById = (req, callback) => {
	const {qid} = req.params;
	if (!qid) return callback(h.createError(400, "No question ID passed."));
	const questionsModel = new Questions;
	questionsModel
	.getQuestionById(qid)
	.then(row => {
		const q = row && typeof row[0] !== "undefined"? row[0] : row;
		return callback(null, q);
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

/**
 * Get question by its id
 * @since 1.0.0
 *
 * @param {object} req - The request object from Restify
 * @param {function} callback - The callback function to send the result to the router
 * @returns {function}
 */
const getQuestionsByIds = (req, callback) => {
	const {qids} = req.params;
	if (!qids) return callback(h.createError(400, "No question IDs passed."));
	// Create a new instance of our journal model; if we don't, we start building some pretty massive
	// queries, which is confusing and awful
	const questionsModel = new Questions;
	questionsModel
	.getQuestionsByIds(qids)
	.then(rows => {
		return callback(null, rows);
	}).catch(err => {
		return callback(h.createError(500, err));
	});
}

module.exports = {
	getNewQuestion,
	getQuestionById,
	getQuestionsByIds
}