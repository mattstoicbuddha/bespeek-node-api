const rootDir = process.cwd(),
		config = require(rootDir + "/config"),
		h = require(process.cwd() + "/helpers"),
		fetch = require("node-fetch");


const iTunes = (req, callback) => {
	const {params} = req;
	fetch("https://itunes.apple.com/search?term=" + params.term, {
        method: 'get'
      })
      .then(res => res.json())
      .then(json => {
      	return callback(null, json.results);
      })
      .catch(error => callback(error));
}

module.exports = {
	iTunes
}