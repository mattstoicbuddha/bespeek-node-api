const UsersModel = require(process.cwd() + '/models/mariadb/users');

const tests = (conf, test, request, mailDump) => {

  test('Delete test users', async function(t) {
  	const emails = [
  		conf.inbox.address,
  		conf.remoteInbox.address,
  		conf.sponsorInbox.address,
  		conf.newConnection.address,
      conf.embedCodeInbox.address
  	];
  	console.log({emails})
    const usersModel = new UsersModel;
    const remove = await usersModel.deleteTestUsers(emails).catch(e => t.fail(e) && t.end());
	t.ok(true);
    t.end();
  });

}

module.exports = {tests};

