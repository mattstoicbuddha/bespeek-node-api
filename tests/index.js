const test = require('tape-catch'),
		request = require('supertest'),
		mainConf = require(process.cwd() + '/config'),
		testConf = require('./conf.js'),
		pass = require('./pass'),
		fail = require('./fail'),
		cleanup = require('./cleanup'),
	 	Redis = require(process.cwd() + "/models/redis"),
		server = require('./../index.js'),
		https = require('https'),
		MailDump = require(process.cwd() + '/helpers/maildump');

const getNewMb = async () => await MailDump.createInbox();

server.on('listening', async function() {
	const conf = Object.assign({}, testConf);
	
	const bf = mainConf.brute_force;
	const redisModel = new Redis;
	// Clear out any login attempts in Redis that might prevent us from testing properly
	const redisKey = bf.redis_key_base + '*127.0.0.1*';
	await redisModel.del(redisKey);

    conf.inbox = await getNewMb();
    conf.remoteInbox = await getNewMb();
    conf.sponsorInbox = await getNewMb();
    conf.newConnection = await getNewMb();
    conf.embedCodeInbox = await getNewMb();
    // Allow for global test settings
    const nTest = (name, cb) => test(name, {timeout: 30000}, cb);
	pass.tests(conf, nTest, request, MailDump);
	fail.tests(conf, nTest, request, MailDump);
	cleanup.tests(conf, nTest, request, MailDump);

	test.onFinish(function() {
		server.close();
		process.exit();
	});
});
