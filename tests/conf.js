const conf = require("../config.js");

// Pseudo-middleware for test config to add in necessary config options
const testConfig = {
	api_url: "localhost:" + conf.tests.port
}

module.exports = Object.assign(conf.tests, testConfig);