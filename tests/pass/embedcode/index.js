const puppeteer = require('puppeteer');

const requestCheck = async (page, dataCallback) => {
  await page.setRequestInterception(true);

  page.on('request', request => {
    request.continue();
    const url = request.url();
    const method = request.method();
    if (url.indexOf('https://api.bespeek.com/analytics/save') < 0 || method !== "POST") return;
    const data = request.postData();
    try {
      const parsed = JSON.parse(data);
      dataCallback(parsed);
    } catch(e) {
      console.log({e, method, url})
    }
  });
}

const tests = (conf, test, request, mailDump) => {

  // Puppeteer Settings
  const puppeteerArgs = [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-infobars',
      '--window-position=0,0',
      '--ignore-certifcate-errors',
      '--ignore-certifcate-errors-spki-list',
      '--user-agent="Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36"'
  ];

  const puppeteerOptions = {
      args: puppeteerArgs,
      headless: true,
      ignoreHTTPSErrors: true,
      userDataDir: './tmp'
  };


  test('Check Embed Code functionality and analytics events', async t => {
    try {
      const browser = await puppeteer.launch(puppeteerOptions);
      const page = await browser.newPage();
      const requiredEvents = ['loaded', 'modal-open', 'save-answer', 'undo-answer', 'tooltip-open', 'modal-close', 'send-final-answers', 'send-final-answers-complete'];
      const events = [];
      await requestCheck(page, data => {
        events.push(data);
      });
      const embedUrl = 'http://' + conf.api_url + '/embedcode/test/13';
      await page.goto(embedUrl).catch(errr => {console.log({errr})});
      await page.waitFor('#bespeek-open-modal-button');
      await page.click('#bespeek-open-modal-button');
      await page.waitFor(1000);
      await page.click('.bespeek-modal-close');
      await page.waitFor(500);
      await page.click('#bespeek-open-modal-button');
      await page.waitFor(1000);
      await page.waitFor('.bespeek-modal-question.question-0.visible > .bespeek-modal-possible-answers > .bespeek-possible-answers-container:nth-child(2)');
      await page.click('.bespeek-modal-question.question-0.visible > .bespeek-modal-possible-answers > .bespeek-possible-answers-container:nth-child(2) > .bspk-mc-icon');
      await page.waitFor(1000);
      await page.click('.bespeek-modal-question.question-0.visible > .bespeek-submit');
      await page.waitFor(1000);
      await page.click('.bespeek-undo-submit');
      await page.waitFor(1000);
      await page.click('.bespeek-modal-question.question-0.visible > .bespeek-modal-possible-answers > .bespeek-possible-answers-container:nth-child(1) > .bspk-mc-icon');
      await page.click('.bespeek-modal-question.question-0.visible > .bespeek-submit');
      await page.waitFor(1000);
      await page.type('.bespeek-modal-question.question-1.visible > .bespeek-answer-text-container > .bespeek-modal-answer-text', 'what is love');
      await page.waitFor('.bespeek-song-results-holder');
      await page.waitFor(1500);
      await page.click('.bespeek-song-results-holder > ul.bespeek-song-results-list > .bespeek-song-result:nth-child(1)');
      await page.waitFor(500);
      await page.click('.bespeek-modal-question.question-1.visible > .bespeek-answer-text-container > .bespeek-submit');
      await page.waitFor(2000);
      const tooltip = await page.$('.bespeek-modal-tooltip');
      const rect = await page.evaluate((tooltip) => {
        const {top, left, bottom, right} = tooltip.getBoundingClientRect();
        return {top, left, bottom, right};
      }, tooltip);
      await page.mouse.move(rect.left, rect.top, {steps: 10});
      await page.waitFor(1000);
      await page.type('.bespeek-modal-question.question--1.visible > .bespeek-answer-text-container > .bespeek-modal-answer-email', conf.embedCodeInbox.address);
      await page.waitFor(1000);
      await page.click('.bespeek-modal-question.question--1.visible > .bespeek-answer-text-container > .bespeek-submit');
      await page.waitFor(3000);
      browser.close();
      const latest = await mailDump.waitForLatest(conf.embedCodeInbox.boxId).catch(e => t.fail(e) && t.end());
      const {text} = latest;
      await mailDump.deleteLatest(conf.remoteInbox.boxId);
      if (text.indexOf('/users/createpassword/') < 0) {
        t.fail("Incorrect email received for remote user.");
        t.end();
        return;
      }
      const eventsCollected = events.map(ev => ev.event);
      let testPassed = true;
      requiredEvents.some(ev => {
        if (eventsCollected.indexOf(ev) < 0) {
          testPassed = false;
          return true;
        }
      })
      console.log({eventsCollected, testPassed})
      t.ok(testPassed);
    } catch (e) {
      t.fail(e);
    }
    t.end();
  });
}

module.exports = tests