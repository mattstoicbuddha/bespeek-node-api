const {randomName} = require('../../helpers');

const tests = (conf, test, request, mailDump) => {
  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(length) {
    const len = Math.pow(10, length - 1);
    return Math.floor(Math.random() * (9 * len)) + len;
  }

  const registrationData = {
    "email": conf.sponsorInbox.address,
    "password": "testingitall",
    "contact_name": randomName(),
    "contact_number": randomInt(10).toString(),
    "company_name": randomName() + " Funeral Services",
    "address": randomInt(5) + " " + randomName() + " Drive",
    "city": "Palm Springs",
    "zip_code": randomInt(5).toString()
  }

  conf.passSponsor = {registrationData};

  test('POST /sponsors/register create a sponsor', function(t) {
    request('http://' + conf.api_url)
      .post('/sponsors/register')
      .send(registrationData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        console.log({err, res})
        if (err) {
          t.fail(err, "Error with registration.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('Check for sponsor\'s confirmation email', async function(t) {
    const latest = await mailDump.waitForLatest(conf.sponsorInbox.boxId).catch(e => t.fail(e) && t.end());
    const {text} = latest;
    const code = text.split('/users/confirmregistration/').pop().split(']').shift();
    registrationData.confirmation_code = code;
    await mailDump.deleteLatest(conf.sponsorInbox.boxId);
    t.ok(true);
    t.end();
  });

  test('POST /users/register/confirm confirm our sponsor', async function(t) {
    const {confirmation_code, email} = registrationData;
    request('http://' + conf.api_url)
      .post('/users/register/confirm')
      .send({confirmation_code})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error with confirmation.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate sign the new sponsor in', function(t) {
    const loginData = {
      email: registrationData.email,
      password: registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          conf.sponsorInbox.token = {"Authorization": "Bearer " + res.body.response.token};
          t.ok(res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;