const tests = (conf, test, request, mailDump) => {
	let newInbox = null;

  let token = {},
  oldProfile = {};

  test('POST /users/authenticate sign the sponsor in', function(t) {
    const loginData = {
      email: conf.passSponsor.registrationData.email,
      password: conf.passSponsor.registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          token = {
            'Authorization': 'Bearer ' +  res.body.response.token
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('GET /sponsors/profile get our new sponsor\'s profile', function(t) {
    request('http://' + conf.api_url)
      .get('/sponsors/profile')
      .set(token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting sponsors's profile.");
        } else if (res.body.success) {
          const profile = res.body.response;
          oldProfile = profile;
          t.ok(profile.id);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('PUT /sponsors/update update the sponsor profile and check the result', function(t) {
    request('https://picsum.photos').get('/200').end((err, res1) => {

    
      request('https://i.picsum.photos').get(res1.headers.location.split('https://i.picsum.photos')[1]).end((err, res) => {
      	// Our initial registration data
      	const registrationData = Object.assign({}, conf.passSponsor.registrationData);
        // Can't change email, unnecessary
        delete registrationData.email;
        // Shouldn't be sending the confirmation codem as we are already confirmed
        delete registrationData.confirmation_code;
      	registrationData.company_name = registrationData.company_name + " A";
        if (registrationData.company_name.length > 50) registrationData.company_name = ("New " + registrationData.company_name).slice(0, 49);
      	registrationData.logo = "data:" + res.headers['content-type'] + ";base64," + res.body.toString('base64');

        request('http://' + conf.api_url)
          .put('/sponsors/update')
          .set(token)
          .send(registrationData)
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(err, res) {
            if (err) {
              t.fail(err, "Error updating the user profile.");
            } else if (res.body.success) {
              request('http://' + conf.api_url)
			      .get('/sponsors/profile')
			      .set(token)
			      .expect(200)
			      .expect('Content-Type', /json/)
			      .end(function(err, res) {
			        if (err) {
			          t.fail(err, "Error getting sponsors's profile.");
			        } else if (res.body.success) {
			          const profile = res.body.response;
			          if (!profile.logo || conf.passSponsor.company_name === profile.company_name) {
			          	// If we don't have a logo or the name is the same, we've failed to update
			          	t.fail("Update did not work."); 
			          }
			          t.ok(profile.id);
			        } else {
			          t.fail('Unsuccessful');
			        }
			        t.end();
			        return;
			      });
            } else {
	            t.fail('Unsuccessful');
	            t.end();
            }
          });
      });
    });
  });

  test('GET /sponsors/embedcode/settings get our new sponsor\'s embed code settings', function(t) {
    request('http://' + conf.api_url)
      .get('/sponsors/embedcode/settings')
      .set(token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting sponsors's embed code settings.");
        } else if (res.body.success) {
          const settings = res.body.response;
          // These should exist and be null currently
          t.ok(settings.backgroundColor === null && settings.quadrantClass === null);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('PUT /sponsors/embedcode/settings update our new sponsor\'s embed code settings', function(t) {
  	const newSettings = {
  		backgroundColor: "#000000",
  		quadrantClass: "top-left"
  	}
    request('http://' + conf.api_url)
      .put('/sponsors/embedcode/settings')
      .set(token)
      .send(newSettings)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error updating sponsors's embed code settings.");
        } else if (res.body.success) {
          request('http://' + conf.api_url)
		      .get('/sponsors/embedcode/settings')
		      .set(token)
		      .expect(200)
		      .expect('Content-Type', /json/)
		      .end(function(err, res) {
		        if (err) {
		          t.fail(err, "Error getting sponsors's embed code settings.");
		        } else if (res.body.success) {
		          const settings = res.body.response;
		          // These should exist and be null currently
		          t.ok(settings.backgroundColor === newSettings.backgroundColor && settings.quadrantClass === newSettings.quadrantClass);
		        } else {
		          t.fail('Unsuccessful');
		        }
		        t.end();
		      });          
        } else {
	        t.fail('Unsuccessful');
	        t.end();
        }
      });
  });

}

module.exports = tests;