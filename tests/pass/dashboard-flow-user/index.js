const UsersModel = require(process.cwd() + '/models/mariadb/users');

const tests = (conf, test, request, mailDump) => {
	let newInbox = null;

  let token = {};

  test('POST /users/authenticate sign the new user in', function(t) {
    const loginData = {
      email: conf.remoteRegistrationData.email,
      password: conf.remoteRegistrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          token = {
            'Authorization': 'Bearer ' +  res.body.response.token
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('GET /users/profile get our new user\'s profile', function(t) {
    request('http://' + conf.api_url)
      .get('/users/profile')
      .set(token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting user's profile.");
        } else if (res.body.success) {
          const {profile} = res.body.response;
          conf.remoteRegistrationData.userId = profile.user.id;
          t.ok(profile.user.id && Array.isArray(profile.answers) && Array.isArray(profile.connections));
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('POST /connections/add add a new connection for existing user', function(t) {
    // So we can use the connection ids to delete connections later
    conf.connectionIds = [];
    request('http://' + conf.api_url)
      .post('/connections/add')
      .set(token)
      .send({connection_email: conf.inbox.address})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        console.log({err, res})
        if (err) {
          t.fail(err, "Error adding a connection.");
        } else if (res.body.success) {
          const {response} = res.body;
          let connId = null;
          if(response.results && !isNaN(response.results[0])) connId = response.results[0];
          t.ok(connId);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('POST /connections/add add a new connection for a brand new user', function(t) {
    console.log({connection_email: conf.newConnection.address})
    request('http://' + conf.api_url)
      .post('/connections/add')
      .set(token)
      .send({connection_email: conf.newConnection.address})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error adding a connection.");
        } else if (res.body.success) {
          const {response} = res.body;
          let connId = null;
          if(response.results && !isNaN(response.results[0])) connId = response.results[0];
          t.ok(connId);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('Check for brand new user\'s confirmation email', async function(t) {
    const latest = await mailDump.waitForLatest(conf.newConnection.boxId).catch(e => t.fail(e) && t.end());
    const {text} = latest;
    const code = text.split('/users/confirmregistration/').pop().split(']').shift();
    // Since we already test confirmation, there's no reason to re-test it here; just needed to check delivery
    await mailDump.deleteLatest(conf.newConnection.boxId);
    t.ok(true);
    t.end();
  });

  test('Special test to ensure the remote and connection users both have a sponsor ID and that it matches', async function(t) {
    const usersModel = new UsersModel;
    const remoteUser = await usersModel.findUserByEmail(conf.remoteRegistrationData.email).catch(e => t.fail(e) && t.end());
    const connUser = await usersModel.findUserByEmail(conf.newConnection.address).catch(e => t.fail(e) && t.end());
    console.log({remoteUser, connUser})
    if (remoteUser && connUser) {
      t.ok(remoteUser.sponsor_id === connUser.sponsor_id);
    } else {
      t.fail("Missing at least one user");
    }
    t.end();
  });

  test('GET /connections get connections', function(t) {
    request('http://' + conf.api_url)
      .get('/connections')
      .set(token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting connections.");
        } else if (res.body.success) {
          
          const {response} = res.body;
          if (response.connections) {
            conf.connectionsArray = response.connections;
            conf.connectionIds = conf.connectionsArray.map(c => c.user_id)
              // Filter out the user id we want to use for profile view testing
              .filter(c => c.user_id !== conf.inbox.userId);
          }
          t.ok(response);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('DEL /connections/delete delete a connection', function(t) {
    request('http://' + conf.api_url)
      .del('/connections/delete')
      .set(token)
      .send({connection_ids: [conf.connectionIds[0]]})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error deleting a connection.");
        } else if (res.body.success) {
          
          const {response} = res.body;
          console.log({response})
          t.ok(response);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('POST /connections/add add the main user back as a connection for other tests', function(t) {
    request('http://' + conf.api_url)
      .post('/connections/add')
      .set(token)
      .send({connection_email: conf.inbox.address})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        console.log({err, res})
        if (err) {
          t.fail(err, "Error adding a connection.");
        } else if (res.body.success) {
          const {response} = res.body;
          let connId = null;
          if(response.results && !isNaN(response.results[0])) connId = response.results[0];
          t.ok(connId);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('GET /questions/new get a new question', function(t) {
    request('http://' + conf.api_url)
      .get('/questions/new')
      .set(token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting a new question.");
        } else if (res.body.success) {
          console.log({body: res.body, token})
          const {response} = res.body;
          if (response.question) {
            conf.pass.questionObj = response.question;
          }
          t.ok(response.question);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('POST /answers/save save the answer to our question', function(t) {
    // TODO: Finish up the boolean type
    const questionObj = Object.assign({}, conf.pass.questionObj);
    const answerObj = {};
    if (questionObj.type === "text") {
      answerObj.answer = "An answer to a text question",
      answerObj.questionId = questionObj.id;
      answerObj.possible_answer_id = null;
    } else {
      const answer = questionObj.possible_answers[Math.floor(Math.random() * questionObj.possible_answers.length)];
      answerObj.answer = answer.id.toString(),
      answerObj.questionId = questionObj.id;
      answerObj.possible_answer_id = answer.id;
    }
    request('http://' + conf.api_url)
      .post('/answers/save')
      .set(token)
      .send(answerObj)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        console.log({"MUH BODY": res.body})
        if (err) {
          t.fail(err, "Error saving the answer to our question.");
        } else if (res.body.success) {
          const {response} = res.body;
          t.ok(response.message && response.message === "Saved");
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('PUT /answers/update update the answer to our question', function(t) {
    // TODO: Finish up the boolean type
    const questionObj = Object.assign({}, conf.pass.questionObj);
    const answerObj = {};
    if (questionObj.type === "text") {
      answerObj.answer = "An answer to a text question",
      answerObj.questionId = questionObj.id;
      answerObj.possible_answer_id = null;
    } else if(questionObj.type === "multiple_choice") {
      const answer = questionObj.possible_answers[Math.floor(Math.random() * questionObj.possible_answers.length)];
      answerObj.answer = answer.id.toString(),
      answerObj.questionId = questionObj.id;
      answerObj.possible_answer_id = answer.id;
    } else {
      console.log({questionObj})
    }
    request('http://' + conf.api_url)
      .put('/answers/update')
      .set(token)
      .send(answerObj)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error saving the answer to our question.");
        } else if (res.body.success) {
          const {response} = res.body;
          t.ok(response === "Saved");
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('PUT /users/profile update the user profile', function(t) {
    request('https://picsum.photos').get('/200').end((err, res1) => {

    
      request('https://i.picsum.photos').get(res1.headers.location.split('https://i.picsum.photos')[1]).end((err, res) => {
        const profileObj = {
          first_name: "John",
          last_name: "Smith",
          zip_code: "96002",
          profile_image: "data:" + res.headers['content-type'] + ";base64," + res.body.toString('base64')
        };

        request('http://' + conf.api_url)
          .put('/users/profile')
          .set(token)
          .send(profileObj)
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(err, res) {
            if (err) {
              t.fail(err, "Error updating the user profile.");
            } else if (res.body.success) {
              request('http://' + conf.api_url)
                .get('/users/profile')
                .set(token)
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function(err, res) {
                  if (err) {
                    t.fail(err, "Error getting user's profile.");
                  } else if (res.body.success) {
                    const {profile} = res.body.response;
                    console.log({profile})
                    t.ok(profile.user.first_name === "John" && profile.user.profile_image.length);
                  } else {
                    t.fail('Unsuccessful');
                  }
                  t.end();
              });
            } else {
              t.fail('Unsuccessful');
              t.end();
            }
          });
      });
    });
  });

}

module.exports = tests;