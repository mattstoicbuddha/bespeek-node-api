const analytics = require('./analytics');
const embedcode = require('./embedcode');
const userRegisterFlow = require('./user-register-flow');
const passwordReset = require('./password-reset');
const remoteUserRegisterFlow = require('./remote-register-flow');
const dashboardFlowUser = require('./dashboard-flow-user');
const dashboardFlowSponsor = require('./dashboard-flow-sponsor');
const sponsorRegisterFlow = require('./sponsor-register-flow');
const profileView = require('./profile-view');

const tests = (conf, test, request, mailDump) => {
  // We pass this down between tests to have the same user,
  // so we set it up here
  conf.pass = {
  	registrationData:{}
  };

  analytics(conf, test, request, mailDump);
  embedcode(conf, test, request, mailDump);
  sponsorRegisterFlow(conf, test, request, mailDump);
  dashboardFlowSponsor(conf, test, request, mailDump);
  userRegisterFlow(conf, test, request, mailDump);
  passwordReset(conf, test, request, mailDump);
  remoteUserRegisterFlow(conf, test, request, mailDump);
  dashboardFlowUser(conf, test, request, mailDump);
  profileView(conf, test, request, mailDump);
}

module.exports = {
  tests
}
