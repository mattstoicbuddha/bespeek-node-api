const rootDir = process.cwd(),
    fs = require('fs');

const tests = (conf, test, request, mailDump) => {

  let session = "";

  test('GET /analytics/start get an analytics session', function(t) {
    request('http://' + conf.api_url)
      .get('/analytics/start')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid session.");
        } else if (res.body.success) {
          if (res.body.response && res.body.response.session) session = res.body.response.session;
          t.ok(session.length);
        }
        t.end();
      });
  });

  test('POST /analytics/save save an analytics object', function(t) {
    const analyticsObj = {
      "event": "analytics-test",
      "url": "blah.com",
      "ip_address": "127.0.0.1",
      "category": "testing-analytics",
      "browser": "DELETE_THIS.com",
      session
    }
    request('http://' + conf.api_url)
      .post('/analytics/save')
      .send(analyticsObj)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error saving analytics object.");
        } else if (res.body.success) {
          t.ok(true);
        }
        t.end();
      });
  });
}

module.exports = tests;