const UsersModel = require(process.cwd() + '/models/mariadb/users');

const tests = (conf, test, request, mailDump) => {

  test('GET /connections/profile/' + conf.remoteRegistrationData.userId + ' get our new connection\'s profile from the user side of things', function(t) {
    request('http://' + conf.api_url)
      .get('/connections/profile/' + conf.remoteRegistrationData.userId)
      // Use the original user token to access the profile of the user whom added the connection
      .set(conf.inbox.token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting connection profile.");
        } else if (res.body.success) {
          const {response} = res.body;
          t.ok(response.user && response.user.id);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

  test('GET /connections/profile/' + conf.remoteRegistrationData.userId + ' get our new sponsees\'s profile', function(t) {
    request('http://' + conf.api_url)
      .get('/connections/profile/' + conf.remoteRegistrationData.userId)
      // Use the original sponsor user token to access the profile of the sponsee
      .set(conf.sponsorInbox.token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting connection profile.");
        } else if (res.body.success) {
          const {response} = res.body;
          t.ok(response.user && response.user.id);
        } else {
          t.fail('Unsuccessful');
        }
        t.end();
      });
  });

};

module.exports = tests;