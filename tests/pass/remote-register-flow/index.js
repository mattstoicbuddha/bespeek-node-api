const UsersModel = require(process.cwd() + '/models/mariadb/users');
const SponsorsModel = require(process.cwd() + '/models/mariadb/sponsors');

const tests = (conf, test, request, mailDump) => {

  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  conf.remoteRegistrationData = {
    "email": conf.remoteInbox.address,
    "answers": [
      {
        questionId: 54,
        answer: "33",
        type: "multiple_choice",
        possible_answer_id: 33
      },
      {
        questionId: 55,
        answer: "If I Ever Leave This World Alive by Flogging Molly",
        type: "text"
      }
    ]
  }

  test('POST /remote/signup create a user from the remote signup route', async function(t) {
    const usersModel = new UsersModel;
    const sponsorsModel = new SponsorsModel;
    const sponsorUser = await usersModel.findUserByEmail(conf.sponsorInbox.address).catch(e => t.fail(e) && t.end());
    const sponsor = await sponsorsModel.findSponsorByUserId(sponsorUser.id).catch(e => t.fail(e) && t.end());
    conf.remoteRegistrationData.sponsorId = sponsor.id;
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(conf.remoteRegistrationData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('Check for user\'s password creation email', async function(t) {
    const latest = await mailDump.waitForLatest(conf.remoteInbox.boxId).catch(e => t.fail(e) && t.end());
    const {text} = latest;
    const code = text.split('/users/createpassword/').pop().split(']').shift();
    conf.remoteRegistrationData.create_password_code = code;
    await mailDump.deleteLatest(conf.remoteInbox.boxId);
    t.ok(true);
    t.end();
  });

  test('PUT /users/passwordreset reset a user\'s pass w/ code', function(t) {
    const pass = Math.random().toString(36).substring(7) + "8790790&&&" + Math.random().toString(36).substring(7);
    const passObj = {
      reset_code: conf.remoteRegistrationData.create_password_code,
      password_first: pass,
      password_second: pass
    }
    // So we can re-auth
    conf.remoteRegistrationData.password = pass;
    request('http://' + conf.api_url)
      .put('/users/passwordreset')
      .send(passObj)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error resetting user's passsword with code.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate sign the new user in with the new password', function(t) {
    const loginData = {
      email: conf.remoteRegistrationData.email,
      password: conf.remoteRegistrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          token = {
            'x-access-token': res.body.response.token
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;