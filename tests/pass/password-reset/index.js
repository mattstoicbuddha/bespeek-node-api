const tests = (conf, test, request, mailDump) => {

	const {registrationData} = conf.pass;

  test('GET /users/passwordreset create pw reset code and send to user', function(t) {
    request('http://' + conf.api_url)
      .get('/users/passwordreset?email=' + registrationData.email)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error creating user password reset code.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('Check for user\'s password reset email', async function(t) {
    const latest = await mailDump.waitForLatest(conf.inbox.boxId).catch(e => t.fail(e) && t.end());
    const {text} = latest;
    const code = text.split('/users/resetpassword/').pop().split(']').shift();
    registrationData.password_reset = code;
    await mailDump.deleteLatest(conf.inbox.boxId);
    t.ok(true);
    t.end();
  });

  test('PUT /users/passwordreset reset a user\'s pass w/ code', function(t) {
    const pass = Math.random().toString(36).substring(7) + "8790790&&&" + Math.random().toString(36).substring(7);
    const passObj = {
      reset_code: registrationData.password_reset,
      password_first: pass,
      password_second: pass
    }
    // So we can re-auth
    registrationData.password = pass;
    request('http://' + conf.api_url)
      .put('/users/passwordreset')
      .send(passObj)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error resetting user's passsword with code.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate sign the new user in with the new password', function(t) {
    const loginData = {
      email: registrationData.email,
      password: registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          token = {
            'x-access-token': res.body.response.token
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;