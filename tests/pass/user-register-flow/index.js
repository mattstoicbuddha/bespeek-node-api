const tests = (conf, test, request, mailDump) => {
	let newInbox = null;

  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  conf.pass = {
    registrationData: {
      "email": conf.inbox.address,
      "password": "testingitall",
      "first_name": "Matt",
      "last_name": "Armstrong"
    }
  }

  test('POST /users/register create a user', function(t) {
    request('http://' + conf.api_url)
      .post('/users/register')
      .send(conf.pass.registrationData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          const {response} = res.body;
          conf.inbox.userId = response.id;
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('Check for user\'s confirmation email', async function(t) {
    const latest = await mailDump.waitForLatest(conf.inbox.boxId).catch(e => t.fail(e) && t.end());
    const {text} = latest;
    const code = text.split('/users/confirmregistration/').pop().split(']').shift();
    conf.pass.registrationData.confirmation_code = code;
    await mailDump.deleteLatest(conf.inbox.boxId);
    t.ok(true);
    t.end();
  });

  test('POST /users/register/confirm confirm our user', async function(t) {
    const {confirmation_code, email} = conf.pass.registrationData;
    request('http://' + conf.api_url)
      .post('/users/register/confirm')
      .send({confirmation_code})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate sign the new user in', function(t) {
    const loginData = {
      email: conf.pass.registrationData.email,
      password: conf.pass.registrationData.password
    }
    console.log({loginData})
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          conf.inbox.token = {"Authorization": "Bearer " + res.body.response.token};
          t.ok(res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;