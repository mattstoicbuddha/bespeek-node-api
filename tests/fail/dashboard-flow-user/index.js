const tests = (conf, test, request, mailDump) => {
  let newInbox = null;

  let token = {};

  test('GET /users/profile BAD TOKEN VALUE', function(t) {
    request('http://' + conf.api_url)
      .get('/users/profile')
      .set(token)
      .expect(401)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting user's profile.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed as expected');
        }
        t.end();
      });
  });

  test('POST /users/authenticate sign our user in', function(t) {
    // We need to get a token to attempt/fail at the other calls
    const loginData = {
      email: conf.pass.registrationData.email,
      password: conf.pass.registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          token = {
            'Authorization': 'Bearer ' +  res.body.response.token
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('POST /connections/add add a new connection INVALID EMAIL', function(t) {
    // So we can use the connection ids to delete connections later
    conf.connectionIds = [];
    request('http://' + conf.api_url)
      .post('/connections/add')
      .set(token)
      .send({connection_email: 'thisisabademail'})
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error adding a connection.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed properly');
        }
        t.end();
      });
  });

  test('DEL /connections/delete delete a connection BAD ID', function(t) {
    request('http://' + conf.api_url)
      .del('/connections/delete')
      .set(token)
      .send({connection_ids: ['a']})
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not deleting a connection.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed properly');
        }
        t.end();
      });
  });

  test('DEL /connections/delete delete a connection DOES NOT EXIST', function(t) {
    request('http://' + conf.api_url)
      .del('/connections/delete')
      .set(token)
      .send({connection_ids: [-1]})
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not deleting a connection.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed properly');
        }
        t.end();
      });
  });

  test('POST /answers/save EMPTY TEXT ANSWER', function(t) {
    const answerObj = {
      answer: "",
      questionId: 5,
      possible_answer_id: null
    }
    request('http://' + conf.api_url)
      .post('/answers/save')
      .set(token)
      .send(answerObj)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not saving the answer to our question.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok("Failed properly");
        }
        t.end();
      });
  });

  // test('POST /answers/save EMPTY BOOLEAN ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "",
  //     questionId: 42,
  //     possible_answer_id: null
  //   }
  //   request('http://' + conf.api_url)
  //     .post('/answers/save')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('POST /answers/save BAD BOOLEAN ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "neither",
  //     questionId: 42,
  //     possible_answer_id: null
  //   }
  //   request('http://' + conf.api_url)
  //     .post('/answers/save')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('POST /answers/save EMPTY MULTIPLE CHOICE ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "",
  //     questionId: 31,
  //     possible_answer_id: null
  //   }
  //   request('http://' + conf.api_url)
  //     .post('/answers/save')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('POST /answers/save BAD MULTIPLE CHOICE ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "-1",
  //     questionId: 31,
  //     possible_answer_id: -1
  //   }
  //   request('http://' + conf.api_url)
  //     .post('/answers/save')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('PUT /answers/update BAD BOOLEAN ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "neither",
  //     questionId: 42,
  //     possible_answer_id: null
  //   }
  //   request('http://' + conf.api_url)
  //     .put('/answers/update')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('PUT /answers/update EMPTY MULTIPLE CHOICE ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "",
  //     questionId: 31,
  //     possible_answer_id: null
  //   }
  //   request('http://' + conf.api_url)
  //     .put('/answers/update')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  // test('PUT /answers/update BAD MULTIPLE CHOICE ANSWER', function(t) {
  //   const answerObj = {
  //     answer: "-1",
  //     questionId: 31,
  //     possible_answer_id: -1
  //   }
  //   request('http://' + conf.api_url)
  //     .put('/answers/update')
  //     .set(token)
  //     .send(answerObj)
  //     .expect(400)
  //     .expect('Content-Type', /json/)
  //     .end(function(err, res) {
  //       if (err) {
  //         t.fail(err, "Error not saving the answer to our question.");
  //       } else if (res.body.success) {
  //         t.fail('Unsuccessful');
  //       } else {
  //         t.ok("Failed properly");
  //       }
  //       t.end();
  //     });
  // });

  test('PUT /users/profile update the user profile BAD ZIP', function(t) {
    const profileObj = {
      zip_code: "abcdefg"
    };

    request('http://' + conf.api_url)
      .put('/users/profile')
      .set(token)
      .send(profileObj)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not updating the user profile.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed Properly');
        }
        t.end();
      });
  });

  test('PUT /users/profile update the user profile BAD IMAGE', function(t) {
    request('https://picsum.photos').get('/200').end((err, res1) => {

    
      request('https://i.picsum.photos').get(res1.headers.location.split('https://i.picsum.photos')[1]).end((err, res) => {

        const profileObj = {
          profile_image: "data:" + res.body.type + ";base64,daigp0jb7640w" + res.body.toString('base64')
        };

        request('http://' + conf.api_url)
          .put('/users/profile')
          .set(token)
          .send(profileObj)
          .expect(400)
          .expect('Content-Type', /json/)
          .end(function(err, res) {
            if (err) {
              t.fail(err, "Error not updating the user profile.");
            } else if (res.body.success) {
              t.fail('Unsuccessful');
            } else {
              t.ok('Failed Properly');
            }
            t.end();
          });
      });
    });
  });

}
module.exports = tests