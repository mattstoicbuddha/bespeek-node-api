const tests = (conf, test, request, mailDump) => {
	let newInbox = null;

  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  conf.fail = {
    registrationData:  {
      "email": "failure@case.com",
      "password": "testingitall",
      "first_name": "Matt",
      "last_name": "Armstrong"
    }
  }

  test('POST /users/register create a user NO EMAIL ADDRESS', function(t) {
    const cloneData = Object.assign({}, conf.fail.registrationData);
    delete cloneData.email;
    request('http://' + conf.api_url)
      .post('/users/register')
      .send(cloneData)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error failing to register user w/ no email.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/register create a user NO PASSWORD', function(t) {
    const cloneData = Object.assign({}, conf.fail.registrationData);
    delete cloneData.password;
    request('http://' + conf.api_url)
      .post('/users/register')
      .send(cloneData)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error failing to register user w/ no email.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/register create a user BAD EMAIL ADDRESS', function(t) {
    const cloneData = Object.assign({}, conf.fail.registrationData);
    cloneData.email = "adfjapoijadfadfhaoidf";
    request('http://' + conf.api_url)
      .post('/users/register')
      .send(cloneData)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error failing to register user w/ no email.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate USER DOESN\'T EXIST', function(t) {
    const loginData = {
      email: 'thereisnouserwiththisemailaddress' + conf.fail.registrationData.email,
      password: conf.fail.registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(404)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error logging in bad user...somehow.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/authenticate USER EXISTS, BAD PASS', function(t) {
    const loginData = {
      email: conf.inbox.address,
      password: conf.fail.registrationData.password + 'thereisnouserwiththispassword'
    }
    console.log({loginData, fail: conf.fail})
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(401)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        console.log({err, res})
        if (err) {
          t.fail(err, "Error logging in bad user...somehow.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /users/register/confirm INVALID CONFIRMATION CODE', async function(t) {
    const confirmation_code = 4564648615645; // This will never appear as an actual conf code
    request('http://' + conf.api_url)
      .post('/users/register/confirm')
      .send({confirmation_code})
      .expect(404)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

}

module.exports = tests