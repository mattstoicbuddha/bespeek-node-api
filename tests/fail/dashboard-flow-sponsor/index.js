const tests = (conf, test, request, mailDump) => {
	let newInbox = null;

  let token = {},
  badToken = {},
  oldProfile = {};

  test('POST /users/authenticate sign the new user in to get bad token', function(t) {
    const loginData = {
      email: conf.passSponsor.registrationData.email,
      password: conf.passSponsor.registrationData.password
    }
    request('http://' + conf.api_url)
      .post('/users/authenticate')
      .send(loginData)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid token.");
        } else {
          // We need this token to be bad
          token = {
            'Authorization': 'Bearer ' +  res.body.response.token
          };
          badToken = {
            'Authorization': 'Bearer ' +  res.body.response.token + "adfadfadf"
          };
          t.ok(res.body.success);
        }
        t.end();
      });
  });

  test('GET /sponsors/profile get our new sponsor\'s profile', function(t) {
    request('http://' + conf.api_url)
      .get('/sponsors/profile')
      .set(badToken)
      .expect(401)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting sponsors's profile.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok("Failed right");
        }
        t.end();
      });
  });

  test('PUT /sponsors/embedcode/settings update sponsor\'s embed code settings BAD BG COLOR', function(t) {
    const newSettings = {
      backgroundColor: "#AABB",
      quadrantClass: "top-left"
    }
    request('http://' + conf.api_url)
      .put('/sponsors/embedcode/settings')
      .set(token)
      .send(newSettings)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not updating sponsors's embed code settings.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed properly');
        }
        t.end();
      });
  });

  test('PUT /sponsors/embedcode/settings update sponsor\'s embed code settings BAD QUADRANT', function(t) {
    const newSettings = {
      backgroundColor: "#000",
      quadrantClass: "bad-quadrant"
    }
    request('http://' + conf.api_url)
      .put('/sponsors/embedcode/settings')
      .set(token)
      .send(newSettings)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not updating sponsors's embed code settings.");
        } else if (res.body.success) {
          t.fail('Unsuccessful');
        } else {
          t.ok('Failed properly');
        }
        t.end();
      });
  });

}

module.exports = tests;