const tests = (conf, test, request, mailDump) => {

  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  conf.remoteRegistrationData = {
    "email": conf.remoteInbox.address,
    "sponsorId": 1, // Easier to hardcode one than make one at this time. TODO: create sponsor on the fly
    "answers": [
      {
        questionId: 54,
        answer: "33",
        type: "multiple_choice",
        possible_answer_id: 33
      },
      {
        questionId: 55,
        answer: "If I Ever Leave This World Alive by Flogging Molly",
        type: "text"
      }
    ]
  }

  test('POST /remote/signup create a user from the remote signup route BAD EMAIL', function(t) {
    const cloneReg = Object.assign({}, conf.remoteRegistrationData);
    cloneReg.email = "adflkhaldfad";
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(cloneReg)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not signing up user");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /remote/signup create a user from the remote signup route BAD SPONSOR ID -1', function(t) {
    const cloneReg = Object.assign({}, conf.remoteRegistrationData);
    cloneReg.sponsorId = -1;
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(cloneReg)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not signing up user");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /remote/signup create a user from the remote signup route BAD SPONSOR ID LETTER STRING', function(t) {
    const cloneReg = Object.assign({}, conf.remoteRegistrationData);
    cloneReg.sponsorId = "adfafad";
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(cloneReg)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not signing up user");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /remote/signup create a user from the remote signup route NO ANSWERS ARRAY', function(t) {
    const cloneReg = Object.assign({}, conf.remoteRegistrationData);
    delete cloneReg.answers;
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(cloneReg)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not signing up user");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('POST /remote/signup create a user from the remote signup route EMPTY ANSWERS ARRAY', function(t) {
    const cloneReg = Object.assign({}, conf.remoteRegistrationData);
    cloneReg.answers = [];
    request('http://' + conf.api_url)
      .post('/remote/signup')
      .send(cloneReg)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not signing up user");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;