const tests = (conf, test, request, mailDump) => {

	const {registrationData} = conf.fail;

  test('GET /users/passwordreset BAD EMAIL ADDRESS', function(t) {
    request('http://' + conf.api_url)
      .get('/users/passwordreset?email=notagoodemail')
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not creating user password reset code.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('GET /users/passwordreset GOOD EMAIL ADDRESS, NO USER', function(t) {
    request('http://' + conf.api_url)
      .get('/users/passwordreset?email=notagoodemail@doesnotexist.com')
      .expect(404)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not creating user password reset code.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('PUT /users/passwordreset reset a user\'s pass w/ code NON-EXISTENT CODE', function(t) {
    const pass = Math.random().toString(36).substring(7) + "8790790&&&" + Math.random().toString(36).substring(7);
    const passObj = {
      reset_code: "12345",
      password_first: pass,
      password_second: pass
    }
    request('http://' + conf.api_url)
      .put('/users/passwordreset')
      .send(passObj)
      .expect(404)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not resetting user's passsword with code.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });

  test('PUT /users/passwordreset reset a user\'s pass w/ code BAD CODE', function(t) {
    const pass = Math.random().toString(36).substring(7) + "8790790&&&" + Math.random().toString(36).substring(7);
    const passObj = {
      reset_code: 12345,
      password_first: pass,
      password_second: pass
    }
    request('http://' + conf.api_url)
      .put('/users/passwordreset')
      .send(passObj)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error not resetting user's password with code.");
        } else {
          t.ok(!res.body.success);
        }
        t.end();
      });
  });
}

module.exports = tests;