const analytics = require('./analytics');
const userRegisterFlow = require('./user-register-flow');
const passwordReset = require('./password-reset');
const remoteRegisterFlow = require('./remote-register-flow');
const dashboardFlowUser = require('./dashboard-flow-user');
const dashboardFlowSponsor = require('./dashboard-flow-sponsor');
const sponsorRegisterFlow = require('./sponsor-register-flow');

const tests = (conf, test, request, mailDump) => {
  // We pass this down between tests to have the same user,
  // so we set it up here
  conf.fail = {
  	registrationData: {}
  };

  analytics(conf, test, request, mailDump);
  sponsorRegisterFlow(conf, test, request, mailDump);
  dashboardFlowSponsor(conf, test, request, mailDump);
  userRegisterFlow(conf, test, request, mailDump);
  passwordReset(conf, test, request, mailDump);
  dashboardFlowUser(conf, test, request, mailDump);
}

module.exports = {
  tests
}
