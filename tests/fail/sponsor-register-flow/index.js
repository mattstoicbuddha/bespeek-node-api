const {randomName} = require('../../helpers');

const tests = (conf, test, request, mailDump) => {
  let token = {};

  // Generates a random integer for use in automating the randomization of tests
  const randomInt = function(length) {
    const len = Math.pow(10, length - 1);
    return Math.floor(Math.random() * (9 * len)) + len;
  }

  const randomKey = (object) => {
    const keys = Object.keys(object);
    return keys[Math.floor(Math.random() * keys.length)];
  }

  const registrationData = {
    "email": "nubad_" + conf.sponsorInbox.address,
    "password": "testingitall",
    "contact_name": randomName(),
    "contact_number": randomInt(10).toString(),
    "company_name": randomName() + " Funeral Services",
    "address": randomInt(5) + " " + randomName() + " Drive",
    "city": "Palm Springs",
    "zip_code": randomInt(5).toString()
  }

  conf.failSponsor = {registrationData};

  const regKeys = Object.keys(registrationData);
  // Run tests for each field and ensure we error if it's missing
  for(let i = 0; i < regKeys.length; i++) {
    const key = regKeys[i];
    const cloneReg = Object.assign({}, registrationData);
    delete cloneReg[key];
    test('POST /sponsors/register create a sponsor MISSING FIELD: ' + key.toUpperCase(), function(t) {
      request('http://' + conf.api_url)
        .post('/sponsors/register')
        .send(cloneReg)
        .expect(400)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          console.log({cloneReg})
          if (err) {
            t.fail(err, "Error not registering with missing field: " + key);
          } else {
            t.ok(!res.body.success);
          }
          t.end();
        });
    });
  }

}

module.exports = tests;