const tests = (conf, test, request, mailDump) => {

  let session = "";

  const analyticsObj = {
      "event": "analytics-test",
      "url": "blah.com",
      "ip_address": "127.0.0.1",
      "category": "testing-analytics",
      "browser": "DELETE_THIS.com"
  }

  test('GET /analytics/start get an analytics session FOR FAILURE TESTS', function(t) {
    request('http://' + conf.api_url)
      .get('/analytics/start')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) {
          t.fail(err, "Error getting valid session.");
        } else if (res.body.success) {
          if (res.body.response && res.body.response.session) session = res.body.response.session;
          analyticsObj.session = session;
          t.ok(session.length);
        }
        t.end();
      });
  });

  const deletable = ['event', 'category', 'session'];

  for(let i = 0; i < deletable.length; i ++) {
    const key = deletable[i];
    test('POST /analytics/save save an analytics object MISSING ' + key.toUpperCase(), function(t) {
      const analyticsClone = Object.assign({}, analyticsObj);
      delete analyticsClone[key];
      request('http://' + conf.api_url)
        .post('/analytics/save')
        .send(analyticsClone)
        .expect(400)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            t.fail(err, "Error failing at saving an analytics obj.");
          } else if (res.body.success) {
            t.fail("Succeeded somehow");
          } else {
            t.ok("Failed properly");
          }
          t.end();
        });
    });
  }
}

module.exports = tests;