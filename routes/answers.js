const h = require("../helpers"), // Helpers for formatting and such
			Answers = require("../controllers/answers");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get user answers
	server.get('/answers/get', (req, res) => {
		Answers.getAnswers(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/

	// Save an answer
	server.post('/answers/save', (req, res) => {
		Answers.saveAnswer(req, (err, resp) => {
			console.log({err})
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** PUT METHODS ***/

	// Save an answer
	server.put('/answers/update', (req, res) => {
		Answers.updateAnswer(req, (err, resp) => {
			// Return the proper error if we have one
			console.log({err})
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}