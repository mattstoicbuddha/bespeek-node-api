const h = require("../helpers"), // Helpers for formatting and such
			Users = require("../controllers/users"),
			Auth = require("../controllers/auth");

module.exports = (server) => {

	/*** GET METHODS ***/


	// Get a user's profile
	server.get('/users/profile', (req, res) => {
		Users.getProfile(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Create a password reset code for a user and email them
	server.get('/users/passwordreset', (req, res) => {
		Users.sendPasswordResetCode(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Check a user's password reset code to determine
	// if it is valid or not
	server.get('/users/passwordreset/check', (req, res) => {
		Users.validatePasswordResetCode(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});


	// Get a user's connections
	server.get('/users/connections', (req, res) => {
		Users.getConnections(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get a user's Story (death) page
	server.get('/users/story', (req, res) => {
		Users.getStory(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/


	// Register a user
	server.post('/users/register', (req, res) => {
		Users.registerUser(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Log a user in
	server.post('/users/authenticate', (req, res) => {
		Auth.requestToken(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Set a user's password reset code and send them an email
	server.post('/users/passwordreset', (req, res) => {
		Users.sendPasswordResetCode(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Resend a user's registration welcome email
	server.post('/users/register/resend', (req, res) => {
		const {email} = req.params;
		Users.sendConfirmation(email)
			.then(resp => {
				return res.send(200, h.formatSuccess("Success"));
			}).catch(err => {
				return res.send(err.code, h.formatErrors(err));
			});
		
	});

	// Confirm a user's registration
	server.post('/users/register/confirm', (req, res) => {
		Users.confirmRegistration(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});


	/*** PUT METHODS ***/


	// Update a user's profile
	server.put('/users/profile', (req, res) => {
		Users.updateProfile(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Reset a user's password
	server.put('/users/passwordreset', (req, res) => {
		Users.resetPassword(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** DELETE METHODS ***/

	// Delete a connection for a user
	server.del('/users/connections', (req, res) => {
		// Users.deleteConnections(req, (err, resp) => {
		// 	// Return the proper error if we have one
		// 	if (err) return res.send(err.code, h.formatErrors(err));
		// 	return res.send(200, h.formatSuccess(resp));
		// });
	});
}