const h = require("../helpers"), // Helpers for formatting and such
			Questions = require("../controllers/questions");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get a new question
	server.get('/questions/new', (req, res) => {
		Questions.getNewQuestion(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get question by its id
	server.get('/questions/byid/:qid', (req, res) => {
		Questions.getQuestionById(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get question by its id
	server.get('/questions/byids', (req, res) => {
		Questions.getQuestionsByIds(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}