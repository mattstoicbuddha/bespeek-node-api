const h = require("../helpers"), // Helpers for formatting and such
			Analytics = require("../controllers/analytics");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get analytics (need to figure out what is coming back first)
	server.get('/analytics/start', (req, res) => {
		Analytics.startAnalyticsSession(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/

	// Save an analytics object
	server.post('/analytics/save', (req, res) => {
		Analytics.saveAnalytics(req, (err, resp) => {
			console.log({err})
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}