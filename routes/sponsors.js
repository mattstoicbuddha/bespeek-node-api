const h = require("../helpers"), // Helpers for formatting and such
			Sponsors = require("../controllers/sponsors");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get a sponsor's info
	server.get('/sponsors/:sponsor_id', (req, res) => {
		Sponsors.getProfile(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get the current sponsor's info
	server.get('/sponsors/profile', (req, res) => {
		Sponsors.getSponsorProfile(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get verification info for a sponsor based on their registration code
	server.get('/sponsors/verification', (req, res) => {
		Sponsors.getVerificationByCode(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get the embed code settings for a sponsor
	server.get('/sponsors/embedcode/settings', (req, res) => {
		Sponsors.getEmbedCodeSettings(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/

	// Create a sponsor verification
	server.post('/sponsors/verification', (req, res) => {
		Sponsors.createVerification(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Register a sponsor after verification
	server.post('/sponsors/register', (req, res) => {
		Sponsors.register(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** PUT METHODS ***/

	// Add a registration code for a new sponsor to create acct with
	server.put('/sponsors/verification', (req, res) => {
		Sponsors.finishVerification(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Update sponsor info
	server.put('/sponsors/update', (req, res) => {
		Sponsors.update(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});	

	// Update a sponsor's embed code settings
	server.put('/sponsors/embedcode/settings', (req, res) => {
		Sponsors.updateEmbedCodeSettings(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}