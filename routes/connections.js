const h = require("../helpers"), // Helpers for formatting and such
			Connections = require("../controllers/connections");

module.exports = (server) => {

	// Add connections for a user
	server.get('/connections', (req, res) => {
		Connections.getConnections(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get a connection's profile
	server.get('/connections/profile/:userId', (req, res) => {
		Connections.getProfile(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Add connections for a user
	server.post('/connections/add', (req, res) => {
		Connections.addConnections(req, (err, resp) => {
			console.log({err})
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		}).catch(err => {
			console.log({err})
			return res.send(err.code, h.formatErrors(err));
		});
	});

	// Remove connections from a user
	server.del('/connections/delete', (req, res) => {
		Connections.deleteConnections(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}