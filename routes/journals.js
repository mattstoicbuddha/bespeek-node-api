const h = require("../helpers"), // Helpers for formatting and such
			Journals = require("../controllers/journals");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get a list of a user's journals
	server.get('/journals', (req, res) => {
		Journals.getJournals(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Get a specific journal
	server.get('/journal/:id', (req, res) => {
		Journals.getJournal(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/

	// Create a journal
	server.post('/journals/create', (req, res) => {
		Journals.createJournal(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** PUT METHODS ***/

	// Get a list of a user's journals
	server.put('/journals/update', (req, res) => {
		Journals.updateJournal(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}