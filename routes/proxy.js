const h = require("../helpers"), // Helpers for formatting and such
		bspkProxy = require("../controllers/proxy");

module.exports = (server) => {

	/*** POST METHODS ***/

	// Proxy route to get song results from the Itunes API
	server.get('/proxy/songs', (req, res) => {
		bspkProxy.iTunes(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}