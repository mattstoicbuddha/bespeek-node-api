const questions = require("./questions"),
			questionsCategories = require("./questions_categories"),
			users = require("./users");

module.exports = (server) => {
	questions(server);
	questionsCategories(server);
	users(server);
}