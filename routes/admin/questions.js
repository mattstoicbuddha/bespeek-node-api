const rootDir = process.cwd(),
			config = require(rootDir + "/config"),
			prefix = config.admin_prefix,
			h = require(rootDir + "/helpers"), // Helpers for formatting and such
			Questions = require(rootDir + "/controllers/admin/questions");

module.exports = (server) => {
	/*** GET METHODS ***/

	// Get a list of questions
	server.get(prefix + '/questions/list', (req, res) => {
		Questions.listQuestions(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
	/*** POST METHODS ***/

	// Create a question
	server.post(prefix + '/questions/create', (req, res) => {
		Questions.createQuestion(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** PUT METHODS ***/

	// Update question
	server.put(prefix + '/questions/update', (req, res) => {
		Questions.updateQuestion(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** DELETE METHODS ***/

	// Delete question
	server.del(prefix + '/questions/delete', (req, res) => {
		Questions.deleteQuestion(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
};