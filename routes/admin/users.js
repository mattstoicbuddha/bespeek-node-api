const rootDir = process.cwd(),
			config = require(rootDir + "/config"),
			prefix = config.admin_prefix,
			h = require(rootDir + "/helpers"), // Helpers for formatting and such
			Users = require(rootDir + "/controllers/users"),
			UserAdmin = require(rootDir + "/controllers/admin/users");

module.exports = (server) => {
	/*** GET METHODS ***/

	// Get a list of users
	server.get(prefix + '/users/list', (req, res) => {
		UserAdmin.listUsers(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
	// /*** POST METHODS ***/

	// // Create a question
	// server.post(prefix + '/questions/create', (req, res) => {
	// 	Questions.createQuestion(req, (err, resp) => {
	// 		// Return the proper error if we have one
	// 		if (err) return res.send(err.code, h.formatErrors(err));
	// 		return res.send(200, h.formatSuccess(resp));
	// 	});
	// });

	// /*** PUT METHODS ***/

	// Deactivate a user
	server.put(prefix + '/users/deactivate', (req, res) => {
		UserAdmin.deactivateUser(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// Reactivate a user
	server.put(prefix + '/users/reactivate', (req, res) => {
		UserAdmin.reactivateUser(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	// /*** DELETE METHODS ***/

	// // Delete question
	// server.del(prefix + '/questions/delete', (req, res) => {
	// 	Questions.deleteQuestion(req, (err, resp) => {
	// 		// Return the proper error if we have one
	// 		if (err) return res.send(err.code, h.formatErrors(err));
	// 		return res.send(200, h.formatSuccess(resp));
	// 	});
	// });
};