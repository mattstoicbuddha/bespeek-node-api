const rootDir = process.cwd(),
			config = require(rootDir + "/config"),
			prefix = config.admin_prefix,
			h = require(rootDir + "/helpers"), // Helpers for formatting and such
			QuestionsCategories = require(rootDir + "/controllers/admin/questions_categories");

module.exports = (server) => {
	/*** GET METHODS ***/

	// Get a list of questions
	server.get(prefix + '/categories/list', (req, res) => {
		QuestionsCategories.listCategories(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** POST METHODS ***/

	// Create a category
	server.post(prefix + '/categories/create', (req, res) => {
		QuestionsCategories.createCategory(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** PUT METHODS ***/

	// Update question
	server.put(prefix + '/categories/update', (req, res) => {
		QuestionsCategories.updateCategory(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});

	/*** DELETE METHODS ***/

	// Delete Category
	server.del(prefix + '/categories/delete', (req, res) => {
		QuestionsCategories.deleteCategory(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
};