const h = require("../helpers"), // Helpers for formatting and such
			Users = require("../controllers/users"),
			Auth = require("../controllers/auth");

module.exports = (server) => {

	/*** POST METHODS ***/

	// Get a user's profile
	server.post('/remote/signup', (req, res) => {
		Users.remoteSignup(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
	});
}