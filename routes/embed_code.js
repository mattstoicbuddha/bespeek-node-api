const h = require("../helpers"), // Helpers for formatting and such
			Embed = require("../controllers/embed_code");

module.exports = (server) => {

	/*** GET METHODS ***/

	// Get the embed code for a sponsor
	server.get('/embedcode/js/:sponsorId', (req, res) => {
		Embed.getCode(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			res.writeHead(200, {
			  'Content-Length': Buffer.byteLength(resp),
			  'Content-Type': 'application/javascript',
			  'Cache-Control': 'no-cache' // Require revalidation just in case
			});
			res.write(resp);
			return res.end();
		});
	});

	// Get embed stylesheet for a sponsor
	server.get('/embedcode/css/:sponsorId', (req, res) => {
		Embed.getStyle(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			res.writeHead(200, {
			  'Content-Length': Buffer.byteLength(resp),
			  'Content-Type': 'text/css',
			  'Cache-Control': 'no-cache' // Require revalidation just in case
			});
			res.write(resp);
			return res.end();
		});
	});

	// Get embed code test page
	server.get('/embedcode/test/:sponsorId', (req, res) => {
		Embed.getPage(req, (err, resp) => {
			// Return the proper error if we have one
			if (err) return res.send(err.code, h.formatErrors(err));
			res.writeHead(200, {
			  'Content-Length': Buffer.byteLength(resp),
			  'Content-Type': 'text/html',
			  'Cache-Control': 'no-cache' // Require revalidation just in case
			});
			res.write(resp);
			return res.end();
		});
	});
}