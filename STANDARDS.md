# Bespeek Node API

The Bespeek API allows the Bespeek front-end to operate

## Setup


```bash
npm install
```

## Usage
To run `dev` locally: `npm start dev`

## Config
All config values in `config.js` should not need to be edited or updated; however, if options need to be added, this is allowed

## MVC

### Routes
Route files MUST be named based on what the route is for (`questions`, `answers`, etc) and put into the `routes` folder. The only exception to this are the admin routes; for these, you need to do the same thing as other routes, but you have to export the route file functionality from the `routes/admin` folder via the `index.js` file in that folder.

Routes CANNOT do any processing beyond calling the necessary controller function/functions. Anything beyond that is the responsibility of the controller.

Routes MUST maintain a similar prefix based on the type of route. For instance, user routes should always be `/users/some/path`.

Example of a typical route, that passes the request object to the controller (how it should be normally):

```
server.post('/answers/save', (req, res) => {
		Answers.saveAnswer(req, (err, resp) => {
		if (err) return res.send(err.code, h.formatErrors(err));
		return res.send(200, h.formatSuccess(resp));
	});
});
```
This is wrong:

```
server.post('/answers/save', (req, res) => {
		if (!req.id || !req.answer_text) return res.send(400, h.formatErrors("Bad!");
		const validId = someFunctionToCheckIds(req.id);
		if (!validId) return res.send(400, h.formatErrors("Bad!"));
		if (typeof req.answer_text !== "string") return res.send(400, h.formatErrors("Bad!"));
		answerTable
		.saveSomeAnswer((req.id, req.answer_text) => {
			if (err) return res.send(err.code, h.formatErrors(err));
			return res.send(200, h.formatSuccess(resp));
		});
});
```
A route SHOULD call one controller function, although there are cases where more than one could be necessary.

### Controllers
Controller files MUST have the following file structure:
- /controllers
	- /somecontroller
		- /index.js
		- /helpers (optional)

`index.js` will have all of the functionality of that particular controller.
The optional `/helpers` folder can be created to hold functionality that is necessary but shouldn't be exported as part of the controller. For instance, functionality to read file creation time might be useful to two or three controller functions but doesn't need to be in the controller `index.js` file, as it isn't being exported as part of the controller.

Controllers MUST export only functions that are needed in a route or another controller.

Controllers CANNOT directly query any database; they must use model functions for queries.

Errors in controllers MUST be returned via the `createError` helper function, with the appropriate [HTTP Status Code](https://www.restapitutorial.com/httpstatuscodes.html) and error message.

### Models
Models *must* be structured as follows:

- /models
	- /dbtype
		- /index.js
		- /middleware (optional)

Middleware must be under `database_name` > `middleware`. There *must* be an `index.js` file that exports all middleware functionality. The middleware functionality can live in separate files within the `middleware` folder, as long as the functionality is imported/exported via `index.js`.

Returns from models *must* be cached if the following is true:
- The value will not change/will not change often (possible answers to a question, for example)
- The value may be accessed more than once during a user session (such as possible answers)

Redis keys *must* have a prefix that denotes what model the cache value is related to, like `getPossibleAnswers:Boolean:`. The rest of the key can then be written out (`question_id:someid`). This will avoid possible conflicts with other keys and ensure that we can clear a particular key or set of keys easily.

The `getPossibleAnswers` function in `models` > `mariadb` > `possible_answers_boolean` > `index.js` has a good example of how to check for a cached value, query if we don't have one, and save the value. Please use it as an example.

Models *must* be classes.

## Tests
Tests *must* be written for every route that is created. We need to be able to verify that the API does not have issues during deploys.  This API uses [supertest](https://github.com/visionmedia/supertest) and [tape-catch](https://github.com/michaelrhodes/tape-catch) to test API routes. At this time, not all routes have tests, so these will need to be written.

## License
This product is under copyright and is considered closed-source; you may not distribute, copy, use, change, or update this software without the express written permission of Bespeek.