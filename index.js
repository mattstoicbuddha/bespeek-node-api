const restify = require('restify'),
	  plugins = require('restify').plugins,
	  env = process.env.NODE_ENV,
	  testsRunning = process.env.TESTS_RUNNING,
	  initialConfig = require('./config'),
	  jwt = require('restify-jwt-community'),
	  Redis = require("./models/redis"),
	  corsMiddleware = require('restify-cors-middleware');

// We modify config, so we need to leave it mutable
let config = initialConfig;

if (testsRunning) {
	config = Object.assign(initialConfig, initialConfig.tests);
}

var server = restify.createServer();

const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['Authorization', 'Accept-Encoding', 'Accept-Language'],
  exposeHeaders: ["Authorization", 'Access-Control-Allow-Origin']
});

server.pre(cors.preflight)
server.use(cors.actual)

server.pre(restify.pre.userAgentConnection());
server.pre(restify.pre.sanitizePath());

// cors-related necessities
server.pre(function(req, res, next) {
	req.headers.accept = 'application/json';
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
  return next();
});

// Stop brute force login attempts
server.pre(function(req, res, next) {
  const path = req.url,
  			ip = req.connection.remoteAddress,
  			bf = config.brute_force;
  // If we aren't logging in, skip this
  if (path !== bf.user_login_path) return next();
  // Check Redis for the key based on IP
  const redisModel = new Redis;
 	const redisKey = bf.redis_key_base + ip;
 	redisModel.get(redisKey)
		.then(intruder => {
			const maxBeforeNow = Date.now() - parseInt(bf.lock_out_max_mins * 60 * 1000);
			if (intruder.lockOutTime && intruder.lockOutTime < maxBeforeNow) {
				return res.send(429, "Too many failed login attempts. Try again later.");
			}
			if (intruder.attempts < 5) {
				intruder.attempts++;
			} else {
				intruder.lockOutTime = Date.now();
				intruder.attempts = 5;
			}
			setTimeout(() => {
				redisModel.save(redisKey, intruder, 1800)
					.then(result => {
						if (intruder.attempts >= 5) return res.send(429, "Too many failed login attempts. Try again later.");
						return next();
					})
					.catch(err => {
						console.log(err);
						// Even if saving to Redis throws an error, we still want to
						// resolve the promise because the error was with Redis and
						// not the user
						if (intruder.attempts >= 5) return res.send(429, "Too many failed login attempts. Try again later.");
						return next();
					});
				}, intruder.attempts * 1000);
		})
		.catch(err => {
			setTimeout(() => {
				redisModel.save(redisKey, {attempts: 1}, 1800)
					.then(result => {
						return next();
					})
					.catch(err => {
						console.log(err);
						// Even if saving to Redis throws an error, we still want to
						// resolve the promise because the error was with Redis and
						// not the user
						return next();
					});
				}, 1000);
		});
});

server.use(restify.plugins.acceptParser(server.acceptable));

server.use(restify.plugins.queryParser({
	mapParams: true
}));
server.use(restify.plugins.bodyParser({
	mapParams: true
}));
server.use(restify.plugins.jsonp());

const skipPaths = ['/', '/users/register', '/users/authenticate', '/users/passwordreset', '/users/passwordreset/check', '/users/register/confirm', '/sponsors/verification', '/sponsors/register', '/users/register/resend', /embedcode\/(js|css|test)/, /remote/, /proxy/, /analytics\/(save|start)/];

server.use(jwt({ secret: config.jwt.secret}).unless({path: skipPaths}));

// Get a user's profile
server.get('/', (req, res) => {
	return res.send(200, h.formatSuccess("Hello, world!"));
});

server.use(function(req, res, next) {
	// Admin role check middleware
	if (req.url.indexOf('/admin/') > -1) {
		if (req.user.role && req.user.role === 1) return next();
		const err = h.createError(403, "Access denied");
		return res.send(err.code, h.formatErrors(err));
	}
	return next();
});

server.use(function(req, res, next) {
	const skippable = skipPaths.some(sp => {
		if (typeof sp === "string") {
			return sp === req.url;
		}
		return sp.test(req.url);
	});
	// Sponsor check middleware
	if (req.url.indexOf('/sponsors/') > -1 && !skippable) {
		const isSponsor = req.user.type === 'sponsor';
		if (req.user.role && req.user.role === 1 || isSponsor) return next();
		const err = h.createError(403, "Invalid user type.");
		return res.send(err.code, h.formatErrors(err));
	}
	return next();
});

// Bring in our routes
require('./routes/analytics')(server);
require('./routes/answers')(server);
require('./routes/embed_code')(server);
require('./routes/connections')(server);
require('./routes/questions')(server);
require('./routes/admin')(server);
require('./routes/journals')(server);
require('./routes/sponsors')(server);
require('./routes/users')(server);
require('./routes/remote')(server);
require('./routes/proxy')(server);

server.listen(config.port, function() {
  console.log('%s listening at %s', server.name, server.url);
});

module.exports = server;
