const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
    	Users = require("../users"),
			Redis = require(rootDir + "/models/redis"),
    	h = require(rootDir + '/helpers');

class Possible_Answers_Boolean {
	constructor() {
		this.db = knex('possible_answers_boolean');
	}
	/**
	 * Get answer info for a particular question
	 * @since 1.0.0
	 *
	 * @param {integer} questionId - The id of the question we are getting answers for
	 * @returns {object}
	 */
	getPossibleAnswers(questionId) {
		const redisModel = new Redis;
		const redisKey = "getPossibleAnswers:Boolean:question_id:" + questionId;
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey)
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
				// Grab the answers
				this.clean().db
				.where({
					question_id: questionId
				})
				.then(answers => {
					redisModel.save(redisKey, answers, 3600 * 3)
						.then(result => {
							return resolve(answers);
						})
						.catch(err => {
							return reject(err);
						});
				})
				.catch(err => {
					return reject(err);
				});
			});
		});
	}

	/**
	 * Save an answer an admin submits using a transaction
	 * @since 1.0.0
	 *
	 * @param {integer} trueCat - The id of the category that is unlocked if a user selects "true"
	 * @param {integer} falseCat - The id of the category that is unlocked if a user selects "false"
	 * @param {integer} questionId - The id of the question we are saving an answer for
	 * @param {object} trx - The transaction we are saving an answer for
	 * @returns {object}
	 */
	saveAnswerTrx(trueCat, falseCat, questionId, trx) {
		// Force the type for the questionId, as we receive it
		// as a string; do the same for cats as well
		trueCat = parseInt(trueCat);
		falseCat = parseInt(falseCat);
		questionId = parseInt(questionId);

		const redisModel = new Redis;
		const redisKey = "getPossibleAnswers:Boolean:question_id:" + questionId;
		// We don't need to prevent the answer from saving if we can't delete from Redis
		// for whatever reason; the cache will eventually clear on its own, so we can
		// keep the rest of the function outside of the bounds of the promise resolution
		redisModel.del(redisKey)
			.then(result => {
				console.log(result);
			})
			.catch(err => {
				console.log("Error deleting Redis key for answer:", err);
			});
		const answer = {
			question_id: questionId
		}
		if (trueCat) {
			answer.true_cat_unlock = trueCat;
		}
		if (falseCat) {
			answer.false_cat_unlock = falseCat;
		}
		// Continue with our transaction
		return this
		.db
		.where({question_id: questionId})
		.transacting(trx)
			.then(possAnswer => {
				if (possAnswer.length) {
					const newAnswer = Object.assign(possAnswer[0], answer);
					// We can't pass a question_id to the `update` query, since it
					// will detect the question_id as a dupe and throw an error
					delete newAnswer.question_id;
					// Delete the ID, since we don't override it anyway
					delete newAnswer.id;
					return this.clean().db.update(newAnswer)
						.where({question_id: questionId})
						.then(result => {
							return trx;
						});
				} else {
          return trx.insert(answer).into('possible_answers_boolean');
				}
			})
      .then(() => {
      	// Instead of only processing the commit here, we return it so we
      	// can return a different value once everything is said and done
      	return trx.commit
      })
      .then(() => {
      	return {updated_id: questionId}
      })
      .catch(trx.rollback);
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db.clearSelect().clearWhere().clearOrder().clearCounters();
    	return this;
    }

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }
}
module.exports = Possible_Answers_Boolean;