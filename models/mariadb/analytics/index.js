const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
    	Users = require("../users"),
			Redis = require(rootDir + "/models/redis"),
    	h = require(rootDir + '/helpers');

class Analytics {
	constructor() {
		this.db = knex('analytics');
	}

	/**
	 * Save an analytics object
	 * @since 1.0.0
	 *
	 * @param {object} analyticsObj - The analytics object to save
	 * @returns {object}
	 */
	save(analyticsObj) {
		return this.clean().db.insert(analyticsObj);
	}
	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('analytics');
    	return this;
    }

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }
}
module.exports = Analytics;