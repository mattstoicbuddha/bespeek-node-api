const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
			h = require(rootDir + '/helpers');

class Users_Meta {
	constructor() {
		this.db = knex('users_meta');
	}

	/**
	 * Get a user's meta
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @returns {object}
	 */
	getMetaByUserId(userId) {
		this.clean().db
			.where({user_id: userId});
		return this;
	}

	/**
	 * Get a user's meta
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @returns {object}
	 */
	getMetaByKeyAndUserId(userId, metaKey) {
		this.clean().db
			.where({
				user_id: userId,
				meta_key: metaKey
			});
		return this;
	}

	/**
	 * Get multiple users' meta
	 * @since 1.0.0
	 *
	 * @param {array} userIds - The user ids
	 * @returns {object}
	 */
	getMetaByUserIds(userIds) {
		this.clean().db
			.select("meta_key", "meta_value", "user_id")
			.whereIn('user_id', userIds);
		return this;
	}

	/**
	 * Add a meta key/value to a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @param {string} metaKey - The key for the user meta
	 * @param {mixed} metaValue - The value for the user meta
	 * @returns {object}
	 */
	addMetaByUserId(userId, metaKey, metaValue) {
		this.clean().db
			.insert({
				meta_key: metaKey,
				meta_value: metaValue,
				user_id: userId
			});
		return this;
	}

	/**
	 * Add a meta key/value to a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @param {string} metaKey - The key for the user meta
	 * @param {mixed} metaValue - The value for the user meta
	 * @returns {object}
	 */
	updateMetaByUserId(userId, metaKey, metaValue) {
		this.clean().db
		.where({
			user_id: userId,
			meta_key: metaKey
		})
		.update({
			meta_value: metaValue
		});
		return this;
	}

	/**
	 * Add a meta key/value to a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @param {string} metaKey - The key for the user meta
	 * @param {mixed} metaValue - The value for the user meta
	 * @returns {object}
	 */
	upsertMetaByUserId(userId, metaKey, metaValue) {
		// Check to see if the meta exists first
		return this
		.getMetaByKeyAndUserId(userId, metaKey)
		.db.first()
		.then(meta => {
			// If we have the meta, do the update
			if (!h.isEmpty(meta)) {
				return this
					.updateMetaByUserId(userId, metaKey, metaValue);
			} else {
				return this
					.addMetaByUserId(userId, metaKey, metaValue);
			}
		});
	}

	/**
	 * Delete a meta key/value from a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The user id
	 * @returns {object}
	 */
	deleteMetaByUserId(userId, metaKey) {
		this.clean().db
			.where({
				user_id: userId,
				meta_key: metaKey
			})
			.del();
		return this;
	}

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db.clearSelect().clearWhere().clearOrder().clearCounters();
    	return this;
    }
}

module.exports = Users_Meta;