const rootDir = process.cwd(),
		config = require(rootDir + '/config'),
		knex = require(rootDir + '/helpers/knex-connector'),
		h = require(rootDir + '/helpers'),
    	Redis = require(process.cwd() + "/models/redis");

class Sponsor_Verifications {
	constructor() {
		this.db = knex('sponsor_verifications');
	}

	/**
	 * Create the verification object for a new sponsor
	 * @since 1.0.0
	 *
	 * @param {object} vParams - The values we need to create the verification task
	 * @returns {object}
	 */
	startVerification(vParams) {
		this.clean().db
			.insert(vParams);
		return this;
	}

	/**
	 * Get a verification object by id
	 * @since 1.0.0
	 *
	 * @param {string} registrationCode - The registration code for the sponsor
	 * @returns {object}
	 */
	getVerification(id) {
		this.clean().db
			.where({id})
			.first();
		return this;
	}

	/**
	 * Get a verification object by registration code
	 * @since 1.0.0
	 *
	 * @param {string} registrationCode - The registration code for the sponsor
	 * @returns {object}
	 */
	getVerificationByCode(registrationCode) {
		this.clean().db
			.where({registration_code: registrationCode})
			.first();
		return this;
	}

	/**
	 * Get a verification object by registration code
	 * @since 1.0.0
	 *
	 * @param {string} registrationCode - The registration code for the sponsor
	 * @returns {object}
	 */
	deleteVerification(id) {
		this.clean().db
			.where({id})
			.del();
		return this;
	}

	/**
	 * Finish the verification task
	 * @since 1.0.0
	 *
	 * @param {integer} id - The id of the verification to allow registration for
	 * @param {string} registrationCode - The code to add to the sponsor for creating a new account
	 * @returns {object}
	 */
	finishVerification(id, registrationCode) {
		this.clean().db
			.update({registration_code: registrationCode})
			.where({id});
		return this;
	}

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(res => {
    		return callback(res);
    	});
    }

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('sponsor_verifications');
    	return this;
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }
}

module.exports = Sponsor_Verifications;