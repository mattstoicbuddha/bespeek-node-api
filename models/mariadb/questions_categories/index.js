const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
			h = require(rootDir + '/helpers');

class Questions_Categories {
	constructor() {
		this.db = knex('questions_categories');
	}

	/**
	 * Get all question categories
	 * @since 1.0.0
	 *
	 * @returns {object}
	 */
	getAllCategories() {
		this.clean().db
			.where({deleted: 0});
		return this;
	}

	/**
	 * Get a question category
	 * @since 1.0.0
	 *
	 * @param {integer} categoryId - The id of the category we need info for
	 * @returns {object}
	 */
	getCategory(categoryId) {
		this.clean().db
			.where({id: categoryId, deleted: 0});
		return this;
	}

	/**
	 * Save a new category or update an existing one
	 * @since 1.0.0
	 *
	 * @param {object} categoryName - The parameters passed for the category
	 * @returns {object}
	 */
	saveCategory(body) {
		const data = {
			category_name: body.category_name,
			category_description: body.category_description,
			category_icon: body.category_icon
		}
		if (body.id) {
			this.clean().db.update(data).where({id: body.id});
		} else {
			this.clean().db.insert(data);
		}
		return this;
	}

	/**
	 * Delete a category
	 * @since 1.0.0
	 *
	 * @param {integer} categoryId - The id of the category we are deleting
	 * @returns {object}
	 */
	deleteCategory(categoryId) {
		this.clean().db
			.update({
				deleted: 1
			})
			.where({id: categoryId});
		return this;
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db.clearSelect().clearWhere().clearOrder().clearCounters();
    	return this;
    }


	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }
}

module.exports = Questions_Categories;