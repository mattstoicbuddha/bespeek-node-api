const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
			h = require(rootDir + '/helpers'),
			middleware = require(rootDir + '/models/middleware'),
			Answers = require("../answers"),
			Connections = require("../connections"),
			Meta = require("../users_meta"),
			Sponsors = require("../sponsors"),
			Redis = require(process.cwd() + "/models/redis");

class Users {
	constructor() {
		this.db = knex('users');
		this.sponsorsDb = knex('sponsors');
	}

	/**
	 * Find a user in our DB by their user id
	 * @since 1.0.0
	 *
	 * @param {integer} id - The user id
	 * @returns {object}
	 */
	findUserById(id) {
		this.clean().db
			.where({id})
			.select('id', 'email', 'password', 'registration_confirmed', 'registration_code', 'role', 'sponsor_id')
			.first();
		return this;
	}

	/**
	 * Find a user in our DB by their email address
	 * @since 1.0.0
	 *
	 * @param {string} email - The email we are looking up the user by
	 * @returns {object}
	 */
	findUserByEmail(email) {
		this.clean().db
			.where({email})
			.select('id', 'email', 'password', 'registration_confirmed', 'registration_code', 'role', 'sponsor_id')
			.first();
		return this;
	}

	/**
	 * Find users in our DB by their email addresses
	 * @since 1.0.0
	 *
	 * @param {string} email - The email we are looking up the user by
	 * @returns {object}
	 */
	findUsersByEmail(emails) {
		this.clean().db
			.whereIn('email', emails)
			.select('id', 'email', 'registration_confirmed', 'registration_code', 'role');
		return this;
	}

	/**
	 * Register a user
	 * @since 1.0.0
	 *
	 * @param {object} params - The parameter values for updating our user
	 * @returns {object}
	 */
	registerUser(params) {
		const saveable = {};
		Object.keys(params).forEach((param, i) => {
			if (["email", "password", "first_name", "last_name", "zip_code", "profile_image", "sponsor_id", "registration_code", "registration_code_expires", "registration_confirmed", "role"].indexOf(param) > -1) saveable[param] = params[param];
		});
		saveable.created_at = Date.now();
		this.clean().db
			.insert(saveable);
		return this;
	}

	/**
	 * Update a user
	 * @since 1.0.0
	 *
	 * @param {object} params - The parameter values for registering our new user
	 * @returns {object}
	 */
	updateUser(params) {
		if (!params.id) throw "No id passed to update a user";
		const updateable = {};
		Object.keys(params).forEach((param, i) => {
			if (["first_name", "last_name", "zip_code", "profile_image", "registration_code", "registration_code_expires", "registration_confirmed"].indexOf(param) > -1) updateable[param] = params[param];
		});
		this.clean().db
			.update(updateable)
			.where({id: params.id});
		return this;
	}

	/**
	 * Get a user's profile information
	 * @since 1.0.0
	 *
	 * @param {integer} id - The user id
	 * @param {boolean} profileView - Whether or not this is a connection/sponsor viewing a profile
	 *
	 * @returns {object}
	 */
	getUserProfileById(id, profileView = false) {
		// Grab our more generic user info
		return new Promise((resolve, reject) => {
			this.clean().db
			.where({id})
			.select('id', 'email', 'first_name', 'last_name', 'profile_image', 'sponsor_id', 'created_at')
			.first()
			.then(async user => {
				if (!user) {
		      		return reject("Invalid user");
				}
				// Get a proper url for our profile image
				middleware.profileImageFormatter(user);
				// Start grabbing our answers now that we have a user
				const answersModel = new Answers;
				const answers = await answersModel.getAnswersByUserId(id, profileView).catch(err => { throw new Error(err) });
				// Start grabbing our connections now that we have a user
				const connectionsModel = new Connections;
				const connections = await connectionsModel.getConnectionsByUserId(id).catch(err => { throw new Error(err) });
				const returnObj = {
					user,
					answers,
					connections
				};
				if (!user.sponsor_id) {
					return resolve(returnObj);
				}
				const {sponsor_id} = user;
				const sponsorModel = new Sponsors;
				const sponsor = await sponsorModel.findSponsorById(sponsor_id).catch(err => { throw new Error(err) });
				if (sponsor) {
					// Get a proper url for our sponsor logo
					middleware.logoFormatter(sponsor);
					returnObj.sponsor = sponsor;
				}
				return resolve(returnObj);
			})
			.catch(err => {
				return reject(err);
			});
      })
	}

	/**
	 * Get a user's Story (death) page by id
	 * @since 1.0.0
	 *
	 * @param {integer} id - The user id
	 * @returns {object}
	 */
	getUserStoryById(id) {
		// Grab our more generic user info
		return new Promise((resolve, reject) => {
			this.clean().db
			.where({id})
			.select('id', 'email', 'first_name', 'last_name', 'profile_image', 'sponsor_id')
			.first()
			.then(user => {
				if (!user) {
		      return reject("Invalid user");
				}
				// Run our profile image middleware here since we won't encounter it otherwise
				middleware.profileImageFormatter(user);
				// Start grabbing our answers now that we have a user
				const answersModel = new Answers;
				answersModel.
					getAnswersByUserId(id)
					.then(answers => {
						const special_categories = config.special_category_ids;
						const answerCats = [];
						const answerCatIds = [];
						const orphans = [];
						// Get our category objects set up so we can return their info with the appropriate questions later
						answers.forEach(answer => {
							if (answer.category_id && answerCatIds.indexOf(answer.category_id) < 0) {
								const catObj = {
									category_id: answer.category_id,
									category_name: answer.category_name,
									category_description: answer.category_description,
									questions: []
								}
								Object.keys(special_categories).forEach(cat => {
									if (special_categories[cat] === answer.category_id) {
										catObj.special_category = cat;
									}
								})
								answerCats.push(catObj);
								answerCatIds.push(answer.category_id);
							}
						});
						// Get each answer into the appropriate category
						answers.forEach(answer => {
							const catIndex = answerCatIds.findIndex((catId) => {
								return answer.category_id === catId;
							});
							// We should always have a category index, but in case we don't,
							// we don't want to send back orphaned data inside of any categories
							if (catIndex < 0) {
								orphans.push({
									question: answer.question,
									answer: answer.answer
								});
								return;
							}
							answerCats[catIndex].questions.push({
								question: answer.question,
								answer: answer.answer
							});
						});
						const sponsorModel = new Sponsors;
						sponsorModel.findSponsorById(user.sponsor_id)
							.then(sponsor => {
								const resolution = {
									user,
									categories: answerCats
								}
								if (sponsor) resolution.sponsor = sponsor;
								if (orphans.length) resolution.orphans = orphans;
								return resolve(resolution);
							})
							.catch(err => {
								return reject(err);
							})
					})
		      .catch(err => {
		      	return reject(err);
		      });
			})
			.catch(err => {
				return reject(err);
			});
		});
	}

	/**
	 * Set a password reset code for a user by email
	 * @since 1.0.0
	 *
	 * @param {string} email - The user's email
	 * @returns {object}
	 */
	setPasswordResetCode(email) {
		this.clean().db
			.where({email})
			.update({
				password_reset_code: h.generatePasswordReset(),
				password_reset_code_expiration: Date.now() + 3600 // Code expires in 15 minutes
			});
		return this;
	}

	/**
	 * Get a user by their password reset code
	 * @since 1.0.0
	 *
	 * @param {string} resetCode - The user's password reset code
	 * @returns {object}
	 */
	getUserPasswordResetCode(email) {
		this.clean().db
			.select('password_reset_code', 'password_reset_code_expiration')
			.where({email})
			.first();
		return this;
	}

	/**
	 * Get a user by their password reset code
	 * @since 1.0.0
	 *
	 * @param {string} resetCode - The user's password reset code
	 * @returns {object}
	 */
	getUserByPasswordResetCode(resetCode) {
		this.clean().db
			.where({password_reset_code: resetCode})
			.andWhere('password_reset_code_expiration', '<', Date.now())
			.first();
		return this;
	}

	/**
	 * Get a user by their confirmation code
	 * @since 1.0.0
	 *
	 * @param {string} confirmationCode - The user's confirmation code
	 * @returns {object}
	 */
	getUserByConfirmationCode(confirmationCode) {
		this.clean().db
			.where({registration_code: confirmationCode})
			// .andWhere('registration_code_expires', '<', Date.now())
			.first();
		return this;
	}

	/**
	 * Confirms a user in the Bespeek db
	 * @since 1.0.0
	 *
	 * @param {string} id - The user's id
	 * @returns {object}
	 */
	confirmUser(id) {
		this.clean().db
			.update({
				registration_code: null,
				registration_code_expires: null,
				registration_confirmed: true
			})
			.where({id});
		return this;
	}

	/**
	 * Get a user by their password reset code
	 * @since 1.0.0
	 *
	 * @param {string} email - The user's email
	 * @returns {object}
	 */
	updateUserPassword(resetCode, password) {
		this.clean().db
			.update({password: password, password_reset_code: null, password_reset_code_expiration: 0})
			.where({password_reset_code: resetCode});
		return this;
	}

	/**
	 * Get a list of all users; this route does not return passwords or other sensitive info
	 * @since 1.0.0
	 *
	 * @param {integer} limit - The maximum number of users to grab (for pagination)
	 * @param {integer} skip - The number of users to skip before we start grabbing info (for pagination)
	 * @returns {object}
	 */
	getUserList(limit, skip) {
		const userQuery = this.clean().db
			// .where({deleted: 0})
			.select(
				'users.id as id',
				'users.email as email',
				'users.first_name as first_name',
				'users.last_name as last_name',
				'users.deleted as deleted',
				'users.deactivated as deactivated',
				'users.sponsor_id as user_sponsor',
				'sponsor_info.id as sponsor_id',
				'sponsor_info.user_id as sponsor_user',
				'sponsor_info.company_name as sponsor_company'
			).leftJoin(this.sponsorsDb.select('id', 'user_id', 'company_name').as('sponsor_info'), {
				'users.id': 'sponsor_info.user_id'
			})
			if (skip) {
				userQuery.offset(parseInt(skip));
			}
			if (limit) {
				userQuery.limit(parseInt(limit));
			}
		return this;
	}

	/**
	 * Deactivate a user's account
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user to deactivate
	 * @returns {object}
	 */
	deactivateUserById(userId) {
		this.clean().db
			.update({
				deactivated: 1		
			})
			.where({id: userId})
		return this;
	}

	/**
	 * Reactivate a user's account
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user to deactivate
	 * @returns {object}
	 */
	reactivateUserById(userId) {
		this.clean().db
			.update({
				deactivated: 0
			})
			.where({id: userId})
		return this;
	}

	/**
	 * Delete test users (allows delete for any user but is only used as part of tests)
	 * @since 1.0.0
	 *
	 * @param {array.string} emails - The test user emails
	 * @returns {object}
	 */
	deleteTestUsers(emails) {
		const obj = this;
		return obj.clean().db
			.whereIn('email', emails)
			.then((users) => {
				const userIds = users.map(u => u.id);
				return knex('sponsors')
					.whereIn('user_id', userIds)
					.del()
					.then(() => {
						return obj.clean().db
							.whereIn('id', userIds)
							.del().then(() => obj);
					})
			});
	}

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(res => {
    		console.log({res})
    		middleware.profileImageFormatter(res);
    		return callback(res);
    	});
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('users');
    	return this;
    }
}

module.exports = Users;