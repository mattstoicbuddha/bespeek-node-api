const rootDir = process.cwd(),
		config = require(rootDir + '/config'),
		knex = require(rootDir + '/helpers/knex-connector'),
    	h = require(rootDir + '/helpers');

class Connections {
	constructor() {
		this.db = knex('connections');
		this.usersDb = knex('users');
	}

	/**
	 * Get the connections a user has, by user id
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the journal, the meta of which we are updating
	 * @returns {object}
	 */
	async getConnectionsByUserId(userId) {

		const connectionList = await this.clean().db
			.select("user_id", "connection_id")
			.where({"user_id": userId})
			.orWhere({"connection_id": userId})
			.catch(e => e);
		if (connectionList.stack) {
			// We have an error
			throw connectionList;
		}
		const connIds = connectionList.map(c => {
			if (c.user_id === userId) {
				return c.connection_id;
			} else {
				return c.user_id;
			}
		});
		return this.clean().usersDb
			.select("id", "email", "first_name", "last_name")
			.whereIn("id", connIds)
			.then(users => {
				const idArray = [];
				const connectionsArray = [];
				connectionList.forEach(cl => {
					let remote = false;
					let user = {};
					if (cl.user_id === userId) {
						// This is a connection the user added from their profile
						user = users.find(u => u.id === cl.connection_id);
					} else {
						// This is a "remote" connection from another user
						user = users.find(u => u.id === cl.user_id);
						remote = true;
					}
					// Don't need multiple instances of this user
					if (idArray.indexOf(user.id) > -1) {
						const ci = connectionsArray.findIndex(ca => ca.user_id === user.id);
						if (connectionsArray[ci].added_by_user) {
							// This user has added this connection already,
							// so we verify the connection is "remote"
							 if (remote) {
							 	// If the connection is remote, the users are connected
							 	connectionsArray[ci].users_connected = true;
							 } else {
							 	// if the connection is not remote, this is a duplicate from the
							 	// query and can be ignored
							 	return;
							 }
						} else {
							// This connection was NOT added by the user, so it's a keyholder conn;
							// If the connection is NOT remote, the users are connected
							connectionsArray[ci].users_connected = true;
						}
						return;
					} else {
						idArray.push(user.id);
					}
					const clObj = {
						user_id: user.id,
						email: user.email,
						first_name: user.first_name,
						last_name: user.last_name,
						added_by_user: !remote
					};
					connectionsArray.push(clObj);
				});
				return connectionsArray.filter(cl => cl);
			});
	}

	/**
	 * Add connections for a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are adding connections for
	 * @param {array} connectionUserIds - The ids of the users to connect to this one
	 * @returns {object}
	 */
	addConnectionsByUserIds(userId, connectionUserIds) {
		const conns = connectionUserIds.map(cui => {
			return {user_id: userId, connection_id: cui};
		});
		this.clean().db
			.insert(conns);

	    // Return the class object instead of the database object in case we are chaining custom class functions;
	    // these do not exist on the db instantiation and it will throw an error.
		return this;
	}

	/**
	 * Delete connections for a user
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are removing connections for
	 * @param {array} connectionUserIds - The ids of the users to disconnect
	 * @returns {object}
	 */
	deleteConnectionsByUserIds(userId, connectionUserIds) {
		
		console.log({userId, connectionUserIds})
		this.clean().db
			.whereIn('connection_id', connectionUserIds)
			.andWhere({user_id: userId})
			.del();

	    // Return the class object instead of the database object in case we are chaining custom class functions;
	    // these do not exist on the db instantiation and it will throw an error.
		return this;
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('connections');
		this.usersDb = knex('users');
    	return this;
    }

	/**
	 * Expose the then function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }
}
module.exports = Connections;