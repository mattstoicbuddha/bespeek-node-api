const rootDir = process.cwd(),
		config = require(rootDir + '/config'),
		knex = require(rootDir + '/helpers/knex-connector'),
		h = require(rootDir + '/helpers'),
		middleware = require(rootDir + '/models/middleware'),
    	Answers = require("../answers"),
    	Meta = require("../users_meta"),
    	Redis = require(process.cwd() + "/models/redis");

class Sponsors {
	constructor() {
		this.db = knex('sponsors');
	}

	/**
	 * Find a sponsor in our DB by the sponsor id
	 * @since 1.0.0
	 *
	 * @param {integer} id - The user id
	 * @returns {object}
	 */
	findSponsorById(id) {
		this.clean().db
			.where({id})
			.select('id', 'company_name', 'logo', 'embed_code_background_color', 'embed_code_quadrant_class', 'contact_number', 'contact_name')
			.first();
		return this;
	}

	/**
	 * Find a sponsor in our DB by their user id
	 * @since 1.0.0
	 *
	 * @param {integer} id - The user id
	 * @returns {object}
	 */
	findSponsorByUserId(id) {
		this.clean().db
			.where({user_id: id})
			.select('id', 'company_name', 'logo', 'city', 'address', 'zip_code', 'contact_name', 'contact_number', 'embed_code_background_color', 'embed_code_quadrant_class')
			.first();
		return this;
	}

	/**
	 * Create a new sponsor
	 * @since 1.0.0
	 *
	 * @param {object} params - The data to make the new sponsor with
	 * @returns {object}
	 */
	createSponsor(params) {
		this.clean().db
			.insert(params);
		return this;
	}

	/**
	 * Update a sponsor
	 * @since 1.0.0
	 *
	 * @param {object} params - The parameter values for updating our sponsor
	 * @returns {object}
	 */
	updateSponsor(params) {
		if (!params.user_id) throw "No id passed to update a user";
		const updateable = {};
		Object.keys(params).forEach((param, i) => {
			if (["company_name", "contact_name", "contact_number", "logo", "address", "city", "zip_code", "embed_code_quadrant_class", "embed_code_background_color"].indexOf(param) > -1) updateable[param] = params[param];
		});
		this.clean().db
			.update(updateable)
			.where({user_id: params.user_id});
		return this;
	}

	/**
	 * Get sponsees for a particular sponsor
	 * @since 1.0.0
	 *
	 * @param {object} params - The data to make the new sponsor with
	 * @returns {object}
	 */
	getSponsees(sponsorId) {
		const usersDb = knex('users');
		usersDb
			.select('id')
			.where({sponsor_id: sponsorId});
		return usersDb;
	}

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(res => {
    		// Get full image paths for sponsor logos
    		middleware.logoFormatter(res);
    		return callback(res);
    	});
    }

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('sponsors');
    	return this;
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }
}

module.exports = Sponsors;