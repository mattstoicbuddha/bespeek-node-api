const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
    	Users = require("../users"),
			Redis = require(rootDir + "/models/redis"),
    	h = require(rootDir + '/helpers');

class Answers {
	constructor() {
		this.db = knex('answers');
	}

	/**
	 * Get answer info for submitted answers; note that this does not return question info
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are getting answers for
	 * @returns {object}
	 */
	getAnsweredQuestionsByUserId(userId) {
		const redisModel = new Redis;
		const redisKey = "getAnsweredQuestionsByUserId:user_id:" + userId;
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey)
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
				// Grab the answers a user has already submitted
				this.clean().db
				.where({
					user_id: userId
				})
				.then(answers => {
		      		redisModel.save(redisKey, answers, 3600 * 3)
						.then(result => {
							return resolve(answers);
						})
						.catch(err => {
							console.log(err);
							// Even if saving to Redis throws an error, we still want to
							// resolve the promise because the error was with Redis and
							// not the data
							return resolve(answers);
						});
		      })
		      .catch(err => {
		      	return reject(err);
		      });
				});
			});
	}

	/**
	 * Grab the answers a user has already submitted, as well as the questions
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are getting answers for
	 * @param {boolean} profileView - Whether or not this is a connection/sponsor viewing a profile
	 * @param {boolean} useCache - Whether or not to use Redis to get our answers; currently only relevant for checks within this model
	 * @returns {object}
	 */
	getAnswersByUserId(userId, profileView = false, useCache = false) {
		const redisModel = new Redis;
		const redisKey = "getAnswersByUserId:user_id:" + userId;
		return new Promise(async (resolve, reject) => {
			if (useCache) {
				const cached = await redisModel.get(redisKey).catch(err => console.error(err));
				if (cached) return resolve(cached);
			}
		    // Set up our subquery for our join to get the questions
		    const questionQuery = knex('questions')
		          .select("id", "text", "category")
		          .as('questions');
		    const qCatWhere = {deleted: 0};
		    if (profileView) {
		    	qCatWhere.is_core = 1;
		    }
		    // Set up our subquery for our join to get the question categories
		    const questionCategoryQuery = knex('questions_categories')
		          .select("id", "category_name", "category_description", "category_icon", "is_core")
		          .where(qCatWhere)
		          .as('questions_categories');
		    // Set up our subquery for our join to get the question answers
		    const questionPossibleAnswersQuery = knex('possible_answers_multiple_choice')
		          .select("id", "answer_text", "question_id")
		          .as('possible_answers_mc');
				// Grab the answers a user has already submitted
				this.clean().db
					.select("questions.id as qid",
							"questions.text as question",
							"questions.category as question_category_id",
							"questions_categories.id as questions_categories_id",
							"questions_categories.category_name as questions_categories_name",
							"questions_categories.category_icon as questions_categories_icon",
							"questions_categories.category_description as questions_categories_description",
							"questions_categories.is_core as questions_categories_is_core",
							"possible_answers_mc.id as answers_mc_id",
							"possible_answers_mc.answer_text as answers_mc_text",
							"possible_answers_mc.question_id as answers_mc_qid",
							"answers.id as aid",
							"answers.text as answer",
							"answers.possible_answer_id as possible_answer_id")
					.where({
						user_id: userId
					})
					.leftJoin(questionQuery, {
		          "questions.id": "answers.question_id"
		      })
					.leftJoin(questionCategoryQuery, {
		          "questions.category": "questions_categories.id"
		      })
					.leftJoin(questionPossibleAnswersQuery, {
		          "questions.id": "possible_answers_mc.question_id",
		          "answers.possible_answer_id": "possible_answers_mc.id"
		      })
		      .orderBy("aid", "DESC")
		      .then(answers => {
		      	const formattedAnswers = answers.map(answer => {
		      		const answerClone = Object.assign({}, answer);
		      		if (profileView && !answerClone.questions_categories_is_core) {
		      			// If we are viewing the profile, we only get core questions atm
		      			return null;
		      		}
		      		answerClone.category_id = answerClone.questions_categories_id;
		      		answerClone.category_name = answerClone.questions_categories_name;
		      		answerClone.category_description = answerClone.questions_categories_description;
		      		answerClone.category_icon = answerClone.questions_categories_icon;
		      		if (answerClone.answers_mc_id) {
		      			// We have a multiple choice answer,
		      			// need to get the answer text
		      			answerClone.answer = answerClone.answers_mc_text;
		      			delete answerClone.answers_mc_text;
		      			// We already have a qid
		      			delete answerClone.answers_mc_qid;
		      			// We don't need the answer id, we already have it w/ possible_answer_id
		      			delete answerClone.answers_mc_id;
		      		}
		      		delete answerClone.question_category_id;
		      		delete answerClone.questions_categories_id;
		      		delete answerClone.questions_categories_name;
		      		delete answerClone.questions_categories_description;
		      		delete answerClone.questions_categories_icon;
		      		return answerClone;
		      	}).filter(a => a);
		      	redisModel.save(redisKey, formattedAnswers, 3600 * 3)
							.then(result => {
								return resolve(formattedAnswers);
							})
							.catch(err => {
								console.log(err);
								// Even if saving to Redis throws an error, we still want to
								// resolve the promise because the error was with Redis and
								// not the data
								return resolve(formattedAnswers);
							});
		      })
		      .catch(err => {
		      	return reject(err);
		      });
			});
	}

	/**
	 * Save an answer a user submits
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are saving the answer for
	 * @param {integer} questionId - The id of the question the user is answering
	 * @param {string} answer - The text of the answer
	 * @returns {object}
	 */
	saveAnswer(userId, questionId, answer, possible_answer_id) {
		// Force the type for the questionId, as we receive it
		// as a string
		questionId = parseInt(questionId);
		// Grab the answers a user has already submitted
		return this.getAnswersByUserId(userId, false)
		.then(answers => {
			console.log({answers, userId, questionId, answer, possible_answer_id})
			const answered = answers.filter((answer) => {
				return answer.qid === questionId;
			});
			// If we have anything in the filtered array, this
			// question has already been answered
			if (answered.length) {
		        // Return a rejected promise so knex returns this as a normal error;
		        // attempts at throwing an error stop script execution, normally
		        return new Promise(function(resolve, reject) {
		            reject("You have already answered this question.");
		        });
			}
			this.clearUserAnswersFromRedis(userId);
			// If we don't have an answer, insert a new one!
			this.clean().db.insert({
				user_id: userId,
				question_id: questionId,
				text: answer,
				possible_answer_id
			});
			console.log("should've done a thign I think")
		    // Return the class object instead of the database object in case we are chaining custom class functions;
		    // these do not exist on the db instantiation and it will throw an error.
			return this;
		});
	}

	/**
	 * Update an answer for a question a user has been asked
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user we are saving the answer for
	 * @param {integer} questionId - The id of the question the user is answering
	 * @param {string} answer - The text of the answer
	 * @returns {object}
	 */
	updateAnswer(userId, questionId, answer, possible_answer_id) {
		// Force the type for the questionId, as we receive it
		// as a string
		questionId = parseInt(questionId);
		// Grab the answers a user has already submitted
		return this.getAnswersByUserId(userId, false)
		.then(answers => {
			const answered = answers.filter((answer) => {
				return answer.qid === questionId;
			});
			console.log({userId, questionId, answers, answer, possible_answer_id, answered})
			// If we don't have anything in the filtered array, this
			// question has not already been answered
			if (!answered.length) {
		        // Return a rejected promise so knex returns this as a normal error;
		        // attempts at throwing an error stop script execution, normally
		        return new Promise(function(resolve, reject) {
		            reject(h.createError(400, "This question has not been answered yet."));
		        });
			}
			this.clearUserAnswersFromRedis(userId);
			// Update our answer
			this.clean().db.update({
				"answers.text": answer,
				possible_answer_id
			})
			.where({
				user_id: userId,
				question_id: questionId
			});
	    // Return the class object instead of the database object in case we are chaining custom class functions;
	    // these do not exist on the db instantiation and it will throw an error.
			return this;
		});
	}

	/**
	 * Get answer info for submitted answers; note that this does not return question info
	 * @since 1.0.0
	 *
	 * @param {array} userIds - The ids of the users we are getting answer counts for
	 * @returns {object}
	 */
	getAnswerCountByUserIds(userIds) {
		const redisModel = new Redis;
		const redisKey = "getAnswerCountByUserIds:user_id:" + userIds.sort((a, b) => a - b).join(",");
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey)
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
				// Grab the answers a user has already submitted
				this.clean().db
				.whereIn('user_id', userIds)
				.then(count => {
		      	redisModel.save(redisKey, count, 3600 * 3)
							.then(result => {
								return resolve(count);
							})
							.catch(err => {
								console.log(err);
								// Even if saving to Redis throws an error, we still want to
								// resolve the promise because the error was with Redis and
								// not the data
								return resolve(count);
							});
		      })
		      .catch(err => {
		      	return reject(err);
		      });
				});
			});
	}

	/**
	 * Clear answers in Redis for a particular user
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
	clearUserAnswersFromRedis(userId) {
		return new Promise((resolve, reject) => {
			const redisModel = new Redis;
			const redisKeys = [
				"getAnsweredQuestionsByUserId:user_id:" + userId,
				"getAnswerCountByUserIds:user_id:" + userId,
				"getAnswerCountByUserIds:user_id:*," + userId + ",*",
				"getAnswerCountByUserIds:user_id:" + userId + ",*",
				"getAnswerCountByUserIds:user_id:*," + userId,
				"getAnswersByUserId:user_id:" + userId
			];
			// We don't need to prevent the answer from saving if we can't delete from Redis
			// for whatever reason; the cache will eventually clear on its own, so we can
			// keep the rest of the function outside of the bounds of the promise resolution
			redisModel.delMany(redisKeys)
					.then(result => {
						console.log({del: result});
					})
					.catch(err => {
						console.log("Error deleting Redis key for answer:", err);
					});
			});
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('answers');
    	return this;
    }

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }

	/**
	 * Expose the catch function for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    catch(callback) {
    	return this.db.catch(callback);
    }
}
module.exports = Answers;