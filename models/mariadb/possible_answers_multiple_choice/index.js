const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
    	Users = require("../users"),
			Redis = require(rootDir + "/models/redis"),
    	h = require(rootDir + '/helpers');

class Possible_Answers_Multiple_Choice {
	constructor() {
		this.db = knex('possible_answers_multiple_choice');
	}
	/**
	 * Get answer info for a particular question
	 * @since 1.0.0
	 *
	 * @param {integer} questionId - The id of the question we are getting answers for
	 * @returns {object}
	 */
	getPossibleAnswers(questionId) {
		const redisModel = new Redis;
		const redisKey = "getPossibleAnswers:MultipleChoice:question_id:" + questionId;
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey)
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
				// Grab the answers
				this.clean().db
				.where({
					question_id: questionId
				})
				.then(answers => {
					redisModel.save(redisKey, answers, 3600 * 3)
						.then(result => {
							return resolve(answers);
						})
						.catch(err => {
							return reject(err);
						});
				})
				.catch(err => {
					return reject(err);
				});
			});
		});
	}

	/**
	 * Save an answer an admin submits using a transaction
	 * @since 1.0.0
	 *
	 * @param {array} possibleAnswers - An array of possible answers to store in the DB
	 * @param {integer} questionId - The id of the question we are saving an answer for
	 * @param {object} trx - The transaction we are saving an answer for
	 * @returns {object}
	 */
	saveAnswersTrx(possibleAnswers, questionId, trx) {
		const redisModel = new Redis;
		const redisKey = "getPossibleAnswers:MultipleChoice:question_id:" + questionId;
		// We don't need to prevent the answer from saving if we can't delete from Redis
		// for whatever reason; the cache will eventually clear on its own, so we can
		// keep the rest of the function outside of the bounds of the promise resolution
		redisModel.del(redisKey)
			.then(result => {
				console.log(result);
			})
			.catch(err => {
				console.log("Error deleting Redis key for answer:", err);
			});

		// Force the type for the questionId, as we receive it
		// as a string
		questionId = parseInt(questionId);
		const answersArray = possibleAnswers.map(answer => {
			return {
				question_id: questionId,
				answer_text: answer.answer_text,
				cat_unlock: parseInt(answer.cat_unlock),
				answer_weight: parseInt(answer.answer_weight)
			}
		});
		// Continue with our transaction
		return this
		.db
		.where({question_id: questionId})
		.del()
		.transacting(trx)
        .then((result) => {
          return trx.insert(answersArray).into('possible_answers_multiple_choice');
        })
        .then(() => {
        	// Instead of only processing the commit here, we return it so we
        	// can return a different value once everything is said and done
        	return trx.commit
        })
        .then(() => {
        	return {question_id: questionId}
        })
        .catch(trx.rollback);
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db.clearSelect().clearWhere().clearOrder().clearCounters();
    	return this;
    }

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }
}
module.exports = Possible_Answers_Multiple_Choice;