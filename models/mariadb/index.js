const users = require("./users"),
	users_meta = require("./users_meta"),
	connections = require("./connections"),
	questions = require("./questions"),
	questions_categories = require("./questions_categories"),
	answers = require("./answers"),
	possible_answers_boolean = require("./possible_answers_boolean"),
	possible_answers_multiple_choice = require("./possible_answers_multiple_choice"),
	analytics = require("./analytics"),
	sponsors = require("./sponsors"),
	sponsor_verifications = require("./sponsor_verifications");

module.exports = {
	users,
	users_meta,
	connections,
	questions,
	questions_categories,
	answers,
	analytics,
	sponsors,
	sponsor_verifications
}