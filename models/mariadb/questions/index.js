const rootDir = process.cwd(),
			config = require(rootDir + '/config'),
			knex = require(rootDir + '/helpers/knex-connector'),
    	Users = require("../users"),
    	UsersMeta = require("../users_meta"),
    	Answers = require("../answers"),
    	possibleAnswersBoolean = require("../possible_answers_boolean"),
    	possibleAnswersMultipleChoice = require("../possible_answers_multiple_choice"),
			Redis = require(rootDir + "/models/redis"),
    	h = require(rootDir + '/helpers');

class Questions {
	constructor() {
		this.db = knex('questions');
	}

	/**
	 * Get a random question for the user to answer that they haven't answered yet
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user, so we can update their user meta to note the last question that was asked
	 * @returns {object}
	 */
	async getNewQuestion(userId, userInfo) {
		const { unlocked_cats } = userInfo;
		// Figure out what questions a user has already answered by grabbing their answers first
		const answersModel = new Answers;
		const answers = await answersModel.getAnsweredQuestionsByUserId(userId).catch(e => h.createError(500, e));
		// Set up a container for question IDs the user has already answered
		const questionIds = [];
		if (answers.length) {
			answers.forEach((answer) => {
				questionIds.push(answer.question_id);
			});
		}
		const coreCategories = await knex('questions_categories').select('id').where('is_core', 1).catch(e => h.createError(500, e));
		if (h.isError(coreCategories)) {
			throw coreCategories;
		}
		const coreCategoryIds = coreCategories.map(c => c.id);
		// Now that we have all of our question IDs, we can get a new question
		// from the database; note that we don't call the DB directly through the
		// defined `this.db`; this is to avoid collision with prior queries
		const catsToCheck = unlocked_cats.concat(coreCategoryIds);
		const query = knex('questions').select("id", "text", "type")
			.whereIn("category", catsToCheck)
			.andWhere({
				deleted: 0
			});
		if (questionIds.length) {
			// If we have questionIds, make sure we ignore those questions
			query.andWhere(function() {
				this.whereNotIn("id", questionIds);
			});
		}
		// Randomize the request, and limit it to 1
		return query
			.orderByRaw("RAND() ASC")
			.limit(1)
			.then(rows => {
				if (!rows || !rows.length) {
					return new Promise(function(resolve, reject) {
			      resolve({"message": "There are no new questions available", "question_loaded": false});
			    });
				}
				const question = rows[0];
				// Boolean and text questions do not need any extra info to modify answers,
				// so we can send them straight back
				if (["boolean", "text"].indexOf(question.type) > -1) {
					return {question, "question_loaded": true};
				} else if (question.type === "multiple_choice") {
					// Now we have to get the answers from the multiple choice table
	          		const possAnswers = new possibleAnswersMultipleChoice;
	          		return possAnswers.getPossibleAnswers(question.id)
	          			.then(answers => {
	          				question.possible_answers = answers;
							return {question, "question_loaded": true};
	          			});
				}
			});
	}

	/**
	 * Get a list of all available questions
	 * @since 1.0.0
	 *
	 * @param {integer} userId - The id of the user, so we can update their user meta to note the last question that was asked
	 * @returns {object}
	 */
	listQuestions() {
	    // Set up our subquery for categories
	    const categoryQuery = knex('questions_categories')
          .select("id", "category_name", "category_description")
          .as("categories");
	    // Set up our possible answers for boolean questions
	    const booleanAnswersQuery = knex('possible_answers_boolean')
          .select("question_id", "true_cat_unlock", "false_cat_unlock")
          .as("possible_answers_boolean");
	    // Set up our possible answers for boolean questions
	    const multipleChoiceAnswersQuery = knex('possible_answers_multiple_choice')
          .select("question_id", "cat_unlock", "answer_text", "answer_weight")
          .as("possible_answers_multiple_choice");

		this.db.select(
				"questions.id as id",
				"questions.text as text",
				"questions.type as type",
				"questions.category as question_category_id",
				"categories.id as category_id",
				"categories.category_name as category_name",
				"categories.category_description as category_description",
				"possible_answers_boolean.question_id as pab_question_id",
				"possible_answers_boolean.true_cat_unlock as pab_true_cat_unlock",
				"possible_answers_boolean.false_cat_unlock as pab_false_cat_unlock",
				"possible_answers_multiple_choice.question_id as pam_question_id",
				"possible_answers_multiple_choice.cat_unlock as pam_cat_unlock",
				"possible_answers_multiple_choice.answer_text as pam_answer_text",
				"possible_answers_multiple_choice.answer_weight as pam_answer_weight"
			)
			.where({
				deleted: 0
			})
			.leftJoin(categoryQuery, {
        "questions.category": "categories.id"
    	})
			.leftJoin(booleanAnswersQuery, {
        "questions.id": "possible_answers_boolean.question_id"
    	})
			.leftJoin(multipleChoiceAnswersQuery, {
        "questions.id": "possible_answers_multiple_choice.question_id"
    	});

	    // Return the class object instead of the database object in case we are chaining custom class functions;
	    // these do not exist on the db instantiation and it will throw an error.
		return this;
	}

	/**
	 * Get a question by its id
	 * @since 1.0.0
	 *
	 * @param {integer} qid - The id of the question
	 * @returns {object}
	 */
	getQuestionById(qid) {
		const redisModel = new Redis;
		const redisKey = "getQuestionById:question_id:" + qid;
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey + "1")
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
				return this.db.select("id", "text", "type", "category")
					.where({
						"deleted": 0,
						"id": qid
					})
					.first()
		    		.then(question => {
		    			if (!question) {
		    				console.log({qid})
		    				return reject(h.createError(404, "No question found with that id."))
		    			}
		    			const questionPromise = new Promise((qResolve, qReject) => {
					    	if (question.type === "text") {
									return qResolve(question);
								} else if (question.type === "boolean") {
									const possAnswers = new possibleAnswersBoolean;
									return possAnswers.getPossibleAnswers(question.id)
					    			.then(answers => {
				    					const answer = answers[0];
					    				question.true_cat_unlock = answer.true_cat_unlock;
					    				question.false_cat_unlock = answer.false_cat_unlock;
											return qResolve(question);
					    			})
					    			.catch(err => {
							    		console.log(err);
											return qReject(h.createError(500, err));
					    			});
								} else if (question.type === "multiple_choice") {
									// Now we have to get the answers from the multiple choice table
					    		const possAnswers = new possibleAnswersMultipleChoice;
					    		return possAnswers.getPossibleAnswers(question.id)
					    			.then(answers => {
					    				question.possible_answers = answers;
											return qResolve(question);
					    			})
					    			.catch(err => {
							    		console.log(err);
											return qReject(h.createError(500, err));
					    			});
								}
							});
							questionPromise
								.then(question => {
									redisModel.save(redisKey, question, 3600 * 3)
									.then(result => {
										return resolve(question);
									})
									.catch(err => {
										console.log(err);
										// Even if saving to Redis throws an error, we still want to
										// resolve the promise because the error was with Redis and
										// not the data
										return resolve(questions);
									});
								})
		    	})
		    	.catch(err => {
		    		console.log(err);
						return Promise.reject(h.createError(500, err));
		    	});
			});
		});
	}

	/**
	 * Get questions by their ids
	 * @since 1.0.0
	 *
	 * @param {array} qids - An array of question ids
	 * @returns {object}
	 */
	getQuestionsByIds(qids) {
		const redisModel = new Redis;
		const redisKey = "getQuestionsByIds:question_ids:" + qids.sort((a, b) => a - b).join(",");
		return new Promise((resolve, reject) => {
			redisModel.get(redisKey + "1")
			.then(result => {
				return resolve(result);
			})
			.catch(err => {
		    // Set up our subquery for categories
		    const categoryQuery = knex('questions_categories')
	          .select("id", "category_name", "category_description")
	          .as("categories");
		    // Set up our possible answers for boolean questions
		    const booleanAnswersQuery = knex('possible_answers_boolean')
	          .select("question_id", "true_cat_unlock", "false_cat_unlock")
	          .as("possible_answers_boolean");
		    // Set up our possible answers for boolean questions
		    const multipleChoiceAnswersQuery = knex('possible_answers_multiple_choice')
	          .select("question_id", "cat_unlock", "answer_text", "answer_weight")
	          .as("possible_answers_multiple_choice");

			return this.db.select(
					"questions.id as id",
					"questions.text as text",
					"questions.type as type",
					"questions.category as question_category_id",
					"categories.id as category_id",
					"categories.category_name as category_name",
					"categories.category_description as category_description",
					"possible_answers_boolean.question_id as pab_question_id",
					"possible_answers_boolean.true_cat_unlock as pab_true_cat_unlock",
					"possible_answers_boolean.false_cat_unlock as pab_false_cat_unlock",
					"possible_answers_multiple_choice.question_id as pam_question_id",
					"possible_answers_multiple_choice.cat_unlock as pam_cat_unlock",
					"possible_answers_multiple_choice.answer_text as pam_answer_text",
					"possible_answers_multiple_choice.answer_weight as pam_answer_weight"
				)
				.whereIn("questions.id", qids)
				.andWhere({
					"deleted": 0
				})
				.leftJoin(categoryQuery, {
	        "questions.category": "categories.id"
	    	})
				.leftJoin(booleanAnswersQuery, {
	        "questions.id": "possible_answers_boolean.question_id"
	    	})
				.leftJoin(multipleChoiceAnswersQuery, {
	        "questions.id": "possible_answers_multiple_choice.question_id"
	    	})
	    	.then(questions => {
	    		questions = h.formatQuestionListRows(questions);
	    		const qPromises = questions.map(question => {
	    			return new Promise((qResolve, qReject) => {
				    	if (question.type === "text") {
								return qResolve(question);
							} else if (question.type === "boolean") {
								const possAnswers = new possibleAnswersBoolean;
								return possAnswers.getPossibleAnswers(question.id)
				    			.then(answers => {
				    				const answer = answers[0];
				    				question.true_cat_unlock = answer.true_cat_unlock;
				    				question.false_cat_unlock = answer.false_cat_unlock;
										return qResolve(question);
				    			})
				    			.catch(err => {
						    		console.log(err);
										return qReject({err});
				    			});
							} else if (question.type === "multiple_choice") {
								// Now we have to get the answers from the multiple choice table
				    		const possAnswers = new possibleAnswersMultipleChoice;
				    		possAnswers.getPossibleAnswers(question.id)
				    			.then(answers => {
				    				question.possible_answers = answers;
										return qResolve(question);
				    			})
				    			.catch(err => {
										return qReject({err});
				    			});
							} else {
								return qReject({err: "Invalid question type"});
							}
						});
					});
					Promise.all(qPromises)
						.then(questions => {
							console.log({questions})
							redisModel.save(redisKey, questions, 3600 * 3)
								.then(result => {
									return resolve(questions);
								})
								.catch(err => {
									console.log(err);
									// Even if saving to Redis throws an error, we still want to
									// resolve the promise because the error was with Redis and
									// not the data
									return resolve(questions);
								});
						})
						.catch(err => {
							return reject(err);
						});
	    	})
	    	.catch(err => {
					return reject(err);
	    	});
	    });
	  });
	}

	/**
	 * Save a question, either as a new question or update a current one, depending on if we have an id
	 * @since 1.0.0
	 *
	 * @param {object} body - The parameters for creating/updating a question
	 * @returns {object}
	 */
	saveQuestion(body) {
		const deleted = typeof body.deleted !== "undefined" && parseInt(body.deleted) === 1;
		let qid = body.id || 0;

		if (qid) {
		const redisModel = new Redis;
		const redisKeyIndividual = "getQuestionById:question_id:" + qid;
		const redisKeyMany = "getQuestionsByIds:question_ids:*" + qid + "*";
		// We don't need to prevent the answer from saving if we can't delete from Redis
		// for whatever reason; the cache will eventually clear on its own, so we can
		// keep the rest of the function outside of the bounds of the promise resolution
		redisModel.delMany([redisKeyIndividual, redisKeyMany])
				.then(result => {
					console.log(result);
				})
				.catch(err => {
					console.log("Error deleting Redis key for answer:", err);
				});
		}

		// Check to see if we already have this question with this text, which we don't allow
		return knex('questions')
			.where({text: body.text})
			.first()
			.then(questionResult => {
				// If we have a qid, we expect this question to have the
				// same text as another, so we don't need to send back
				// an error
				if (questionResult && !qid) {
			    return new Promise(function(resolve, reject) {
			        reject("This question already exists.");
			    });
				}
				const trans = knex.transaction((trx) => {
				if (qid) {
					// We are updating a question since we have an id
					this.db.update({
			        text: body.text,
			        category: body.category_id,
			        deleted
			      })
			      .where({
			        id: qid
			      });
					} else {
						this.db.insert({
			        text: body.text,
			        type: body.type,
			        category: body.category_id,
			        deleted: 0
			      });
					}
					// Set up our transaction for saving possible answers simultaneously
		      return this
		      .db
		      .transacting(trx)
		      .then((result) => {
		          // If the result is an array, we received a row ID from the insert;
		          // if it isn't, this is an update and we treat it accordingly
		          const questionId = Array.isArray(result) ? result[0] : body.id;
		          qid = questionId;
		          // Get our question again so we have our question info and don't have to depend
		          // on the body values, since we don't pass things like type on update
		          return this
		          	.clean()
		          	.db
		          	.where({id: qid})
		          	.first()
		          	.then(question => {
		          		// If we don't have a question from this query, then this is a new question;
		          		// we don't get the question because we are in the middle of a transaction
		          		// that hasn't been committed yet.  So we need to use the body parameters
		          		// instead
			          	const questionObj = question || body;
			          	// If we have a boolean or multiple choice question, special processing is required
									if (questionObj.type === "boolean") {
										const possAnswers = new possibleAnswersBoolean;
										return possAnswers.saveAnswerTrx(body.true_cat_id, body.false_cat_id, questionId, trx);
									} else if (questionObj.type === "multiple_choice") {
										const possAnswers = new possibleAnswersMultipleChoice;
										if (body.possible_answers && Array.isArray(body.possible_answers)) {
											return possAnswers.saveAnswersTrx(body.possible_answers, questionId, trx);
										} else {
										    return new Promise(function(resolve, reject) {
											    reject("Invalid answer array passed");
											});
										}
									} else if (questionObj.type === "text") {
										return trx;
									} else {
								    return new Promise(function(resolve, reject) {
								        reject("Invalid question type passed");
								    });
									}
							});
			    })
		      .then(trx.commit)
		      .catch(trx.rollback);
				});
				return trans;
			});
	}

	/**
	 * Delete a question
	 * @since 1.0.0
	 *
	 * @param {integer} questionId - The id of the question we are deleting
	 * @returns {object}
	 */
	deleteQuestion(questionId) {
		this.clearQuestionFromRedis();
		this.db
			.update({
				deleted: 1
			})
			.where({id: questionId});
		return this;
	}

	clearQuestionFromRedis(questionId) {
		const redisModel = new Redis;
		const redisKeys = [
			"getQuestionById:question_id:" + questionId,
			"getQuestionsByIds:question_ids:" + questionId,
			"getQuestionsByIds:question_ids:*," + questionId + ",*",
			"getQuestionsByIds:question_ids:" + questionId + ",*",
			"getQuestionsByIds:question_ids:*," + questionId
		];
		// We don't need to prevent the answer from saving if we can't delete from Redis
		// for whatever reason; the cache will eventually clear on its own, so we can
		// keep the rest of the function outside of the bounds of the promise resolution
		return redisModel.delMany(redisKeys)
				.then(result => {
					return result;
				})
				.catch(err => {
					return err;
				});
	}

	/**
	 * Clean the query to remove unnecessary clauses for reuse
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    clean() {
    	this.db = knex('questions');
    	return this;
    }

	/**
	 * Expose the promise object for the db class connector
	 * @since 1.0.0
	 *
	 * @param {function} callback - The callback function to send the result to the router
	 * @returns {object}
	 */
    then(callback) {
    	return this.db.then(callback);
    }
}
module.exports = Questions;