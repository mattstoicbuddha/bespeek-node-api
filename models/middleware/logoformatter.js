const rootDir = process.cwd(),
			config = require(rootDir + '/config');

// Format our logo(s) for return with our sponsor results
const logoFormatter = (res) => {
	// Ensure that something ws passed to the logo formatter
	if (!res) return res;

	if (Array.isArray(res)) {
		res.forEach(r => {
			if (r.logo) {
				// Add the image CDN to the logo property
				// so the end user doesn't need to figure that
				// out separately
				r.logo = config.aws.assets_cdn + "/" + config.aws.sponsor_logos_dir + "/" + r.logo;
			}
		});
	} else {
		if (res.logo) {
			// Add the image CDN to the logo property
			// so the end user doesn't need to figure that
			// out separately
			res.logo = config.aws.assets_cdn + "/" + config.aws.sponsor_logos_dir + "/" + res.logo;
		}
	}
}

module.exports = logoFormatter;