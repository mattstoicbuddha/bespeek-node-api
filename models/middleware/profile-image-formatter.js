const rootDir = process.cwd(),
		config = require(rootDir + '/config');

// Format our profile_image(s) for return with our sponsor results
const profileImageFormatter = (res) => {
	if (Array.isArray(res)) {
		res.forEach(r => {
			if (r.profile_image) {
				// Add the image CDN to the profile_image property
				// so the end user doesn't need to figure that
				// out separately
				r.profile_image = config.aws.assets_cdn + "/" + config.aws.profile_images_dir + "/" + r.profile_image;
			}
		});
	} else if (res) {
		if (res.profile_image) {
			// Add the image CDN to the profile_image property
			// so the end user doesn't need to figure that
			// out separately
			res.profile_image = config.aws.assets_cdn + "/" + config.aws.profile_images_dir + "/" + res.profile_image;
		}
	}
}

module.exports = profileImageFormatter;