const mariadb = require("./mariadb"),
			redis = require("./redis");

module.exports = {
	mariadb,
	redis
}