const rootDir = process.cwd(),
		redis = require('ioredis'),
		config = require(rootDir + "/config"),
		client = redis.createClient({
			host: config.redis.host,
			port: config.redis.port,
			password: config.redis.password
		}),
		h = require(rootDir + '/helpers');

class Redis {
	constructor(opts) {
		this.db = client;
		this.prefix = opts && opts.prefix ? opts.prefix + ":" : "";
	}

	/**
	 * Save a key/value pair in Redis
	 * @since 1.0.0
	 *
	 * @param {string} key - The key we need to save a value for
	 * @param {mixed} value - The value we need to save
	 * @param {integer} expiresIn - How long before the Redis cache expires this entry (optional)
	 * @returns {object}
	 */
	save(key, value, expiresIn) {
		// Add our prefix to the passed Redis key
		key = key + this.prefix;
		return new Promise((resolve, reject) => {
			this.db.set(key, JSON.stringify(value), (err, result) => {
				if (err) return reject(err);
				this.db.expire(key, expiresIn || 3600);
				return resolve(result);
			});
		});
	}

	/**
	 * Get a key/value pair from Redis
	 * @since 1.0.0
	 *
	 * @param {string} key - The key we need to get a value for
	 * @returns {object}
	 */
	get(key) {
		// Add our prefix to the passed Redis key
		key = key + this.prefix;
		return new Promise((resolve, reject) => {
			this.db.get(key, (err, result) => {
				if (err) return reject(err);
				if (!result) return reject("No result in Redis");
				try {
					const parsed = JSON.parse(result);
					if (parsed === null) throw "Invalid or missing Redis result.";
					return resolve(parsed);
				} catch(e) {
					// If there is an error with parsing the result,
					// it is invalid json and we can't move forward
					// with sending the result back
					return reject("Invalid JSON");
				}
			});
		});
	}

	/**
	 * Delete key/value pairs (based on pattern matching) from Redis
	 * @since 1.0.0
	 *
	 * @param {string} pattern - The pattern we need to delete
	 * @returns {object}
	 */
	 del(pattern) {
		// Add our prefix to the passed Redis key
		pattern = pattern + this.prefix;
		return new Promise((resolve, reject) => {
			let keyTotal = 0;
			const stream = this.db.scanStream({
			  match: pattern
			});
			stream.on('data', (keys) => {
		      const pipeline = this.db.pipeline();
			  // `keys` is an array of strings representing key names
			  if (keys.length) {
			  	keyTotal += keys.length;
			  	keys.map(function(key, ind) {
			    	pipeline.del(key);
			  	});
			  }
			  pipeline.exec();
			});
			stream.on('end', function () {
				return resolve(keyTotal);
			});
			stream.on('error', function (e) {
				return reject(e.message);
			});
		});
	}

	/**
	 * Delete several key/value pairs from Redis using different patterns
	 * @since 1.0.0
	 *
	 * @param {array} patterns - The patterns we need to delete
	 * @returns {object}
	 */
	 delMany(patterns) {
		return new Promise((resolve, reject) => {
		 	const delManyPromises = patterns.map(pattern => {
				// Add our prefix to the passed Redis key
				pattern = pattern + this.prefix;
				return new Promise((dResolve, dReject) => {
					let keyTotal = 0;
					const stream = this.db.scanStream({
					  match: pattern
					});
					stream.on('data', (keys) => {
				      const pipeline = this.db.pipeline();
					  // `keys` is an array of strings representing key names
					  if (keys.length) {
					  	keyTotal += keys.length;
					  	keys.map(function(key, ind) {
					    	pipeline.del(key);
					  	});
					  }
					  pipeline.exec();
					});
					stream.on('end', function () {
						return dResolve({pattern, keyTotal});
					});
					stream.on('error', function (e) {
						return dReject(e.message);
					});
				});
			});
			Promise.all(delManyPromises)
				.then(results => {
					return resolve(results);
				})
				.catch(err => {
					return reject(err);
				})
		});
	}

}

module.exports = Redis;